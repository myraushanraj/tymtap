<?php 
require_once 'db-connection.php';
require_once 'company-class.php';
require_once 'application-class.php';
function show_all_disputed_jobs(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
 $cmd="select thread_id from job_process where thread_status='disputed'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
				                                   
				                                            return $arr1;
}
function show_all_jobs(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="select thread_id from job_process";
				     $result=$db->query($cmd);
				       // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
				                                   
				                                            return $arr1;
}
function all_requests($id){
$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT request_id from job_application where thread_id IN (SELECT thread_id from job_process where owner_id='$id') ORDER BY  timings DESC";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
                    return $arr1;


}
function companies_thread($id){
$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_process where owner_id='$id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
                    return $arr1;

}
function all_thread(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT thread_id from job_process";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
                    return $arr1;
}


class thread{

function __construct($thread_id){
$this->id=$thread_id;
$this->all_data();
$this->application_data();
//$this->thread_status();
}
			function all_data(){
					
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_process where thread_id='$this->id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
				                       $this->courier='';	            
					                   $this->title=$arr1[0]['thread_title'];
					                   $this->description=$arr1[0]['thread_description'];
					                   $this->company=$arr1[0]['owner_id'];
					                    $this->get_notified=$arr1[0]['admin_not'];
					                   $c3=new company($this->company);
					                   $this->company_name=$c3->title;
					                   $this->company_logo=$c3->pic;

					                   $this->job_seeker=$arr1[0]['job_id'];
					                   $this->posted_ago=return_timespan($arr1[0]['post_timings']);
					                   $this->fare_rate=$arr1[0]['fare_rate'];
					                   $this->job_duration=$arr1[0]['job_duration'];
					                   $this->courier=$arr1[0]['courier_id'];
					                   $this->checked_in_time=$arr1[0]['checkin_time'];
					                   $this->checked_in_time_ago=$this->return_timespan($this->checked_in_time);

					                   $this->checked_out_time=$arr1[0]['checkout_time'];
					                    $this->checked_out_time_ago=$this->return_timespan($this->checked_out_time);
					                   $this->courier_process=$arr1[0]['courier_checkout'];
					                   $this->last_seen='';
					                   $this->job_time_ago=return_timespan($arr1[0]['job_timings']);
					                   $this->job_start=$arr1[0]['start_date'];
					                   $this->job_end=$arr1[0]['end_date'];
					                   $this->job_status=$arr1[0]['thread_status'];
					           
					                   $this->location=$arr1[0]['location'];
					                   if(empty($arr1[0]['amount_generated']))
					                   {
					                        $this->billed=$this->job_duration*$this->fare_rate;
					                        $this->billed_formatted='&#8377;'.$this->billed;
					                    }
					                            $this->courier_alloted=$arr1[0]['courier_id'];
					                   if(!empty($this->courier_alloted))
					                   {
					                        $courier=new courier($this->courier_alloted);
					                        $this->courier_alloted_service=$courier->service;	
					                        $this->courier_alloted_phone=$courier->phone;
					                   }
			}


			function application_data(){

			         $d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT COUNT(`request_id`) AS accepted from job_application where thread_id='$this->id' and job_status='accepted'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	      $this->accept_count=$row['accepted'];
				                                                }
				$cmd="SELECT COUNT(`request_id`) AS rejected from job_application where thread_id='$this->id' and job_status='rejected'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	      $this->reject_count=$row['rejected'];
				                                               }

					                $this->apply_count=$this->accept_count+$this->reject_count;

			}

	function return_thread_status_string(){

							if($this->job_status=='accepted')
								return 'Accepted';
							else if($this->job_status=='checked_in')
							return 'Checked In';
						else if($this->job_status=='checked_out')
							return 'Checked Out';
						else if($this->job_status=='paid')
							return 'Paid';
						else if($this->job_status=='disputed')
							return 'Disputed';
						else if($this->job_status=='payment_done')
							return 'Payment Done';

			}

			function thread_status(){

				if($this->courier_alloted)
					$this->thread_status='started';
				elseif (!empty($this->checked_in_time)) {
					$this->thread_status='checked_in';
				}elseif (!empty($this->checked_out_time)) {
					$this->thread_status='checked_out';
				}
				elseif (!empty($this->billed)) {
					$this->thread_status='billed';
				}else
				{
							$this->thread_status='not_started';
				}


			}



			function apply_job(){
$request_id = rand(1000000,9999999);
$hangout_id = rand(1000000,9999999);
date_default_timezone_get('Asia/calcutta');
$timings=date('Y-m-d H:i:s');

$db=new dbconn();
$con=$db->connect();
$cmd="INSERT INTO job_application(request_id,thread_id,hangout_id,timings,job_id) values ('$request_id','$this->id','$hangout_id','$timings','$this->job_id')";
$query=$con->prepare($cmd);
$query->execute();

			}

public function new_status($new_status){
$db=new dbconn();
$con=$db->connect();
$cmd="UPDATE job_process SET thread_status='$new_status' where thread_id='$this->id'";
$query=$con->query($cmd);


}


function my_applied_jobs(){
$d1=new dbconn();
	               	 $db=$d1->connect();$arr1=array();
        			 $cmd="SELECT thread_id from job_application where job_id='$this->job_id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
                     return $arr1;
}




function thread_box(){
// shows frontend for one full thread 
?>
<div class="container" style="box-shadow: 0px 0px 10px 2px grey;">
<div class="row gap-top">
<div class="col-md-6">
<?php for ($i=0; $i < 5; $i++) { 
  # code...?>
<img src="https://upload.wikimedia.org/wikipedia/commons/3/38/Red_star_unboxed-2000px.png" width="26">
<?php } ?>
<br>
<ul type="none" class="job-details">
<li style=" color: purple"></li><?php echo $this->title?></li>
<li><?php echo $this->company_name?></li>
<li><img src="icon/<?php echo $this->company_logo?>" width="160"></li>
</ul>
</div>
<div class="col-md-6">
Payment Verified <img src="icon/verified.png" width="30">
<br>
<?php if(!($this->job_applied()))
{?>
<a href='javascript:void(0)'  class='btn theme-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?apply=<?php echo $this->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to apply for this job. Doing this will make visible for the company and they can accept your request ?'"  data-target='#newstatus'>Apply Now</a>
<?php }else{
	echo "<strong class='applied-text'>Applied</strong>";
	} ?>
</div>
</div>
    
<div class="row">
<ul type="none" class="theme1">
  <li class="same-line">
<img src="icon/calendar.png" width="30"><br>
 <?php echo $this->job_time_ago?></li>
  <li class="same-line">
<img src="icon/indian-rupee.png" width="30"><br>
  &#8377 <?php echo $this->fare_rate?> / hour</li>
  <li class="same-line">
<img src="icon/placeholder.png" width="30"><br>
 <?php echo $this->location?> </li>
</ul>
</div>
</div><!-- == container === -->

<?php
}



function interview_questions(){
        	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_interview where thread_id='$this->id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                           }

				                           $this->question_count=3;
				                 			$this->query1=$arr1[0]['query1'];
				                 			$this->query2=$arr1[0]['query2'];
				                 			$this->query3=$arr1[0]['query3'];
				                     	  	         

}
function job_applied(){
$d1=new dbconn();
	               	 $db=$d1->connect();$arr1=array();
        			 $cmd="SELECT thread_id from job_application where job_id='$this->job_id' and thread_id='$this->id'";
				     $result=$db->query($cmd);
				     if($result->rowCount()>0)
				     return 1;
				     else
				     return 0;
}


function company_thread_box(){
?>
<div class="container thread-box">
      <div class="row">
<h3 class="theme1 capital"><?php echo $this->title?> </h3>
<p class="job-desc"><?php echo $this->description?> </p>
<ul type="none">
  <li class="same-line">Start Date:<i class="theme1"><?php echo $this->job_start?></i> </li>
  <li class="same-line">End Date: <i class="theme1"><?php echo $this->job_end?></i></li>
  <li class="same-line">Timings: <i class="theme1">11am to 3 pm</i> </li>
</ul>
<?php if(!empty($this->job_status))
echo '<a href="#" class="pull-right btn btn-success capital">'.$this->job_status.'</a>';
?>
      </div>
 </div>
<?php 
}

                   function return_timespan($from_time){
date_default_timezone_set("Asia/calcutta");
$from_time = strtotime(date($from_time));
$to_time =strtotime(date('Y-m-d H:i:s'));
//$from_time = strtotime(date("2017-3-20 09:19:00"));
$minutes=round(abs($to_time - $from_time) / 60,2);
$hours=$minutes/60;
$days=$hours/24;
$years=$days/365;
//echo date("H:i:s");
//===================                        
$final_minutes=((($minutes%(60))%24)%365);
$final_hours=(($hours)%24);
$final_days=($days%24);
//============
$return_string='';
if((round($years)>0))
{
     $return_string.=round($years).' years ';
}
if($final_days>0)
$return_string.=$final_days.' days ';
if($final_hours>0 && ($final_days<=1))
$return_string.=$final_hours.' hours ';
if($final_minutes>=0  && ($final_hours<=1))
$return_string.=$final_minutes.' mins ';
$return_string=ltrim($return_string);
$return_string=rtrim($return_string);
if($return_string=='ago')
{
return 'just now ';
}else
{
return $return_string.' ago';
}	
}

}


?>