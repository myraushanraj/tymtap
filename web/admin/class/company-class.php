<?php 
require_once 'db-connection.php';
//$c1=new company('3923851');
//print_r($c1);
function show_all_companies(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT job_id from company";
				     $result=$db->query($cmd);
				       // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
				                                   
				                                            return $arr1;
}
class company{

function __construct($id){
$this->id=$id;
$this->all_data();
$this->task_data();
$this->supervisor_details();
}
function companies_ongoing_thread(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT request_id from job_application where job_status='accepted' AND thread_id IN (select thread_id from job_process where owner_id='$this->id' and thread_status='checked_in')";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                          }
                    return $arr1;
}
	function ongoing_jobs(){ 
				$ongoing_arr=$this->companies_ongoing_thread();
				for ($i=0; $i < (sizeof($ongoing_arr)); $i++) {
					$application=new application($ongoing_arr[$i]['request_id']);
					$application->ongoing_thread_box();
				        }
			}

// payment tab optioned thread 


			function companies_payment_thread(){
	               	$d1=new dbconn();
	               	$db=$d1->connect();$arr1=array();
  	 $cmd="SELECT request_id from job_application where job_status='accepted' AND thread_id IN (select thread_id from job_process where owner_id='$this->id' and (thread_status='checked_out' OR thread_status='payment_done'))";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
                    return $arr1;
}
	function payment_jobs(){ 
				$pay_arr=$this->companies_payment_thread();
				for ($i=0; $i < sizeof($pay_arr); $i++) {
					$application=new application($pay_arr[$i]['request_id']);
					$application->payment_thread_box();
				        }
			}



			function all_data(){					
	               	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="	SELECT * from  company where job_id='$this->id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                             while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                          }
				     
					                   $this->title=$arr1[0]['title'];
					                   $this->about=$arr1[0]['about'];
					                   $this->in_service=$arr1[0]['in_service'];
					                   $this->category=$arr1[0]['category'];
					                   $this->address=$arr1[0]['address'];
					                   $this->phone=$arr1[0]['address'];
					                   $this->mobile=$arr1[0]['mobile'];
					                   if(!empty($arr1[0]['logo']))
					                   $this->pic='../img-company/'.$arr1[0]['logo'];
					                    else
					                   $this->pic='../img-company/unknown.png';
					              	   $this->is_online=$arr1[0]['is_online'];
					                   
					              
			}
			function supervisor_details(){
       	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
	 $cmd="	SELECT * from  company_login where job_id='$this->id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                             while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                          }
				                 $this->email=$arr1[0]['email'];
				                 $this->password=$arr1[0]['password'];                         


			}

			function user_custom(){

				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="select * from organisation_team where member_id='$this->id'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
					                   $this->theme_color=$arr1[0]['theme'];
					                   $this->email_allowed=$arr1[0]['email_allowed'];
					                   $this->msg_allowed=$arr1[0]['msg_allowed'];

					              
			}
					function all_category_jobs($type){
			if($type=='pending')
				$type_form='accepted';
			elseif ($type=='pay_due') 
				$type_form='checked_out';
			elseif ($type=='ongoing') 
				$type_form='checked_in';
			elseif ($type=='paid') 
				$type_form='payment_done';
			    else
				$type_form='all';
		 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
	               	if($type_form=='all'){
	            $cmd="SELECT thread_id from job_process where owner_id='$this->id'";
	               	}else{
                $cmd="SELECT thread_id from job_process where owner_id='$this->id' and thread_status='$type_form'";
        			}
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
				                                               return $arr1;
		}		
function task_data()
			{
				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_process where owner_id='$this->id' and thread_status='accepted'";
				     $result=$db->query($cmd);
				       $this->pending_count=$result->rowCount();
				      $cmd="SELECT * from job_process where owner_id='$this->id' and thread_status='checked_in'";
				     $result=$db->query($cmd);
				      $this->ongoing_count=$result->rowCount();
				      $cmd="SELECT * from job_process where owner_id='$this->id' and thread_status='payment_done'";
				     $result=$db->query($cmd);
				      $this->paid_count=$result->rowCount();
				      	$cmd="SELECT * from job_process where owner_id='$this->id' and thread_status='checked_out'";
				        $result=$db->query($cmd);
				      $this->unpaid_count=$result->rowCount();
				
				      $this->total_count=$this->paid_count+$this->ongoing_count+$this->unpaid_count+$this->pending_count;
				  }

	function job_data(){
				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_id_data where job_id='$this->id'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
					                   $this->rating=$arr1[0]['rating'];
					                   $this->job_active=$arr1[0]['job_active'];
					                   $this->job_count=$arr1[0]['job_count'];
					                    $this->last_review=$arr1[0]['last_review'];

					               



			}

			function view_all_messages(){

			   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT mid from message_history where member_id='$id' and reply_text IS  NULL";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }

// 2 nds case where user is a sender


        			 $cmd="SELECT mid from message_history where triggered_from='$id' and reply_seen='0' and reply_text IS Not  NULL";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }
				                   	                    	   return $arr1;

}

}


?>