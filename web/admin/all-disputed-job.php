<?php
include_once('inc/header.php');
require_once('class/thread-class.php');
$all_arr=show_all_disputed_jobs();
?>
<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<div class="col-md-12">
	<h3>Companies (1234) <button class="btn btn-primary">Add New</button>
		<button class="btn btn-success">Export Data</button></h3>
</div>
<div class="agile-grids">	
				<!-- tables -->
				
				<div class="agile-tables">
					<div class="w3l-table-info">
					  
					    <table id="dataTables-example">
						<thead>
						  <tr>
							<th>#</th>
							<th>Title</th>
							<th>Timings</th>
							<th>Members</th>
						     <th>Fare Rate / Hours</th>
						    <th>Action</th>

						  </tr>
						</thead>
						<tbody>
		<?php for ($i=0; $i < sizeof($all_arr); $i++) { 
			# code...
$thread=new thread($all_arr[$i]['thread_id']);
			?>
						  <tr>
							<td><?php echo $i; ?></td>

							<td>

							<?php echo $thread->title?><Br>
								<span class="highlighted"><?php echo $thread->job_status?></span>
<?php if($thread->job_status=='payment_done')
echo "<a href='' class='btn btn-success'>Download Invoice </a>";
?>
							</td>
	<td>
	<ul>
		<li>Created on : <?php echo $thread->posted_ago?></li>
		<li>Checked in : <?php echo $thread->checked_in_time_ago?></li>
	    <li>Checked out : <?php echo $thread->checked_out_time_ago?></li>
	</ul>
   </td>
<td>
	<ul>
		<li>Company : <?php echo $thread->company_name?></li>
	    <li>Job Seeker : <?php echo $thread->job_seeker?></li>
	</ul>
   </td>
   <td>
	<ul>
		<li>Fare Rate: <?php echo '&#8377;'.$thread->fare_rate?></li>
	    <li>Fare / Hour : <?php echo $thread->job_duration?></li>
	</ul>
   </td>
								<td>
								<a href="settle-disputes.php" class="disable-btn"> Action</a>
								</td>			
						  </tr>
		<?php } ?>	
						
						
						 
						</tbody>
					  </table>
					</div>

<!--//four-grids here-->
<script>
	function detail_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_thread_bank_detail.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}
function job_detail(id,type){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_thread_job_detail.php?id='+id+"&type="+type,true);
xmlhttp.send();
}
</script>
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->

			<?php
		    include_once('inc/footer.php');	
			include_once('inc/side_nav.php');
			?>
			<!--inner block start here-->
