<?php
include_once('inc/header.php');
require_once('class/company-class.php');
if(isset($_GET['block_company']))
{
$d1=new dbconn();
 $db=$d1->connect();
            $company_id=$_GET['block_company'];
            $sql="UPDATE company set in_service='0' where job_id='$company_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}
if(isset($_GET['enable_company']))
{
$d1=new dbconn();
 $db=$d1->connect();
            $company_id=$_GET['enable_company'];
            $sql="UPDATE company set in_service='1' where job_id='$company_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}

if(isset($_POST['notify_user']))
{
$d1=new dbconn();
 $db=$d1->connect();
 date_default_timezone_set("Asia/calcutta");
$timings=(date("Y-m-d H:i:s"));
            $company_id=$_POST['company1'];
            $notification=$_POST['message1'];
            $sql="INSERT INTO notification (member_id,notification,timings,triggeredby) VALUES ('$company_id',:notification,'$timings','admin')";
            $qry = $db->prepare($sql);
            $qry -> execute(array(":notification"=>$notification));
}
if(isset($_POST['update_profile']))
{
$company_id=$_POST['company_id1'];
$email=$_POST['email1'];
$name=$_POST['name1'];
$phone=$_POST['phone1'];
$about=$_POST['about1'];
$d1=new dbconn();
 $db=$d1->connect();
            $sql="UPDATE organisation_team set name='$name',phone='$phone',email='$email',about_me='$about' where company_id='$company_id'";echo $sql;
            $qry = $db->prepare($sql);
            $qry -> execute();
}
$all_arr=show_all_companies();
?>
<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<div class="col-md-12">
	<h3>Companies (<?php echo sizeof($all_arr)?>) 
		<button class="btn btn-export"  href="javascript:void(0)" data-toggle='modal' data-target='#export_data'
     onclick="document.getElementById('table_name').value='job_process'" class=" btn btn-danger">Export Data</button></h3>
</div>
</div>
<div class="agile-grids">	
				<!-- tables -->
				
				<div class="agile-tables">
					<div class="w3l-table-info">
					  
					    <table id="dataTables-example">
						<thead>
						  <tr>
							<th>#</th>
							<th>Company</th>
							<th>Contact</th>
							<th>Description</th>
						     <th>Job Stats</th>

							<th>Action</th>
						  </tr>
						</thead>
						<tbody>
		<?php for ($i=0; $i < sizeof($all_arr); $i++) { 
			# code...
$company=new company($all_arr[$i]['job_id']);
			?>
						  <tr>
							<td><?php echo $i; ?></td>

							<td>
<img src="<?php echo $company->pic;?>" class='profile-img'>
							<?php echo $company->title?>
								<span class="highlighted"><?php echo $company->id?></span>

							</td>

							<td><?php echo $company->email."<br>".$company->phone?></td>
		
							<td><?php echo $company->about;?></td>
													<td>Total: <a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $company->id?>','total')" ><?php echo $company->total_count;?></a><br>
								Delivered:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $company->id?>','paid')" ><?php echo $company->paid_count;?></a><br>
							Pending:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $company->id?>','pending')" ><?php echo $company->pending_count;?></a>
Ongoing:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $company->id?>','ongoing')" ><?php echo $company->ongoing_count;?></a>
Payment Due :<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $company->id?>','pay_due')" ><?php echo $company->unpaid_count;?></a>
							</td>
							<td>
<?php if($company->in_service)
{ ?>
<button href='javascript:void(0)'  class='disable-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?block_company=<?php echo $company->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to block '+'<?php echo $company->title?>'+' from all time tap service ?'"  data-target='#newstatus'>Block</button>
<?php }else{ ?>
<button href='javascript:void(0)'  class='disable-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?enable_company=<?php echo $company->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to enable '+'<?php echo $company->title?>'+' from all time tap service ?'"  data-target='#newstatus'>Enable</button>
<?php } ?>
							<br>
								<button data-toggle='modal' style="display: none" onclick="notify_pop('<?php echo $company->id ?>')" data-target='#detail_pop' class="notify-btn">Notify</button>	
								<br>
							</td>	
						  </tr>
		<?php } ?>	
						
						
						 
						</tbody>
					  </table>
					</div>

<!--//four-grids here-->
<script>
function edit_profile(id){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/edit-company-profile.php?id='+id,true);
xmlhttp.send();
}

function notify_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/notify-company.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}

	function detail_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_company_bank_detail.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}
function job_detail(id,type){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch-company-job-detail.php?id='+id+"&type="+type,true);
xmlhttp.send();
}
</script>
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->

			<?php
		    include_once('inc/footer.php');	
			include_once('inc/side_nav.php');
			?>
			<!--inner block start here-->
