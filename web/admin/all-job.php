<?php
include_once('inc/header.php');
require_once('class/thread-class.php');
//anmin notoficatin (get notyofied )
if(isset($_GET['get_notified']))
{
$d1=new dbconn();
$db=$d1->connect();
            $thread_id=$_GET['get_notified'];
            $sql="UPDATE job_process set admin_not='1' where thread_id='$thread_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}
$all_arr=show_all_jobs();
?>
<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<div class="col-md-12">
	<h3>All Jobs (<?php echo sizeof($all_arr)?>) 
		<button class="btn btn-export"  href="javascript:void(0)" data-toggle='modal' data-target='#export_data'
     onclick="document.getElementById('table_name').value='job_process'" class=" btn btn-danger">Export Data</button></h3>
</div>
<div class="agile-grids">	
				<!-- tables -->
				
				<div class="agile-tables">
					<div class="w3l-table-info">
					  
					    <table id="dataTables-example">
						<thead>
						  <tr>
							<th>#</th>
							<th>Title</th>
							<th>Timings</th>
							<th>Members</th>
						     <th>Fare Rate / Hours</th>

						  </tr>
						</thead>
						<tbody>
		<?php for ($i=0; $i < sizeof($all_arr); $i++) { 
			# code...
$thread=new thread($all_arr[$i]['thread_id']);
			?>
						  <tr>
							<td><?php echo $i; ?></td>

							<td>

							<?php echo $thread->title?><Br>
								<span class="highlighted"><?php echo $thread->job_status?></span>
<?php if($thread->job_status=='payment_done')
echo "<a href='../company/invoices/IN".$thread->id.".pdf' class='btn btn-success'>Download Invoice </a>";
?>
	</td>
	<td>
	<ul>
		<li>Created on : <?php echo $thread->posted_ago?></li>
		<li>Checked in : <?php echo $thread->checked_in_time_ago?></li>
	    <li>Checked out : <?php echo $thread->checked_out_time_ago?></li>
	</ul>
   </td>
   <td>
	<ul>
		<li>Company : <?php echo $thread->company_name?></li>
	    <li>Job Seeker : <?php echo $thread->job_seeker?></li>
	</ul>
   </td>
   <td>
	<ul>
		<li>Fare Rate: <?php echo '&#8377;'.$thread->fare_rate?></li>
	    <li>Fare / Hour : <?php echo $thread->job_duration?></li>
	      
<?php if(!$thread->get_notified)
{ ?>
	      <li style="display: none">
	      <a href='javascript:void(0)'  class='disable-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?get_notified=<?php echo $thread->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to get notified of this job process as an admin'"  data-target='#newstatus'>Get Notified</a>
	      </li>
	      <?php }else
	      echo "<strong class='green-highlighted'>Notification On</strong>"; ?>
	</ul>
   </td>
										
						  </tr>
		<?php } ?>	
						
						
						 
						</tbody>
					  </table>
					</div>

<!--//four-grids here-->
<script>
	function detail_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_thread_bank_detail.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}
function job_detail(id,type){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_thread_job_detail.php?id='+id+"&type="+type,true);
xmlhttp.send();
}
</script>
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->

			<?php
		    include_once('inc/footer.php');	
			include_once('inc/side_nav.php');
			?>
			<!--inner block start here-->
