<?php
include_once('inc/header.php');
require_once('class/member-class.php');
require_once('class/notification-class.php');

if(isset($_GET['block_member']))
{
$d1=new dbconn();
 $db=$d1->connect();
            $member_id=$_GET['block_member'];
            $sql="UPDATE organisation_team set in_service='0' where member_id='$member_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}
if(isset($_GET['enable_member']))
{
$d1=new dbconn();
 $db=$d1->connect();
            $member_id=$_GET['enable_member'];
            $sql="UPDATE organisation_team set in_service='1' where member_id='$member_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}

if(isset($_POST['notify_user']))
{
$d1=new dbconn();
 $db=$d1->connect();

            // send push notifications ===================
$admin=new notification($_POST['member1']);
$link="";
$from='admin';
$notification=$_POST['message1'];
$admin->push_notification($notification,$from,$link,'0',$_POST['subject1']);
/*------- push notification ========================= */ 

}
if(isset($_POST['update_profile']))
{
$member_id=$_POST['member_id1'];

$name=$_POST['name1'];
$hometown=$_POST['hometown1'];
$designation=$_POST['designation1'];

$about=$_POST['about1'];
$d1=new dbconn();
$db=$d1->connect();
            $sql="UPDATE organisation_team set name='$name',about_me='$about',phone='$phone',email='$email',about_me='$about' where member_id='$member_id'";
            $qry = $db->prepare($sql);
            $qry -> execute();
}
$all_arr=show_all();
?>
<!-- tables -->
<link rel="stylesheet" type="text/css" href="css/table-style.css" />
<link rel="stylesheet" type="text/css" href="css/basictable.css" />
<script type="text/javascript" src="js/jquery.basictable.min.js"></script>
<div class="col-md-12">
	<h3>Registerd Users (<?php echo sizeof($all_arr)?>) 
		<button class="btn btn-export"  href="javascript:void(0)" data-toggle='modal' data-target='#export_data'
     onclick="document.getElementById('table_name').value='organisation_team'" class=" btn btn-danger">Export Data</button></h3>
</div>
</div>
<div class="agile-grids">	
				<!-- tables -->
				
				<div class="agile-tables">
					<div class="w3l-table-info">
					  
					    <table id="dataTables-example">
						<thead>
						  <tr>
							<th>#</th>
							<th>Name</th>
							<th>Contact</th>
							<th>Profile</th>
							<th>Job Starts</th>
							<th>Last active</th>
							<th>Action</th>
						  </tr>
						</thead>
						<tbody>
		<?php for ($i=0; $i < sizeof($all_arr); $i++) { 
			# code...
$member=new member($all_arr[$i]['member_id']);
			?>
						  <tr>
							<td><?php echo $i; ?></td>
							<td>
<img src="<?php echo $member->pic;?>" class='profile-img'>
							<?php echo $member->name?>
								<span class="highlighted"><?php echo $member->id?></span>
							</td>
							<td><?php echo $member->email."<br>".$member->phone?>
								<br>
								<?php if(!((empty($member->account_number) || ($member->account_number)=='not updated')))
								{ ?>
								<a href="javascript:void(0)" style="padding: 1px"data-target="#detail_pop" data-toggle="modal" class="btn-export" onclick="detail_pop('<?php echo $member->id?>','')">Bank Data</a>
<br><Br>
<?php if($member->bank_verified)
echo "<strong>verified</strong><img src='icons/verified.png' width='23' style='margin-left:12px'>";
else
echo "<strong>Not verified</strong>";
						 } ?>
							</td>
							<td><?php echo $member->about?></td>
							<td>Total: <a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $member->id?>','total')" ><?php echo $member->total_count;?></a><br>
								Delivered:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $member->id?>','paid')" ><?php echo $member->paid_count;?></a><br>
							Pending:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $member->id?>','pending')" ><?php echo $member->pending_count;?></a>
Ongoing:<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $member->id?>','ongoing')" ><?php echo $member->ongoing_count;?></a>
Payment Due :<a href="javascript:void(0)" data-target="#detail_pop" data-toggle="modal"  onclick="job_detail('<?php echo $member->id?>','pay_due')" ><?php echo $member->unpaid_count;?></a>
							</td>
							<td>Pragati Maidan<br>
							2 days ago Active</td>
							<td>
<?php if($member->in_service)
{ ?>
<button href='javascript:void(0)'  class='disable-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?block_member=<?php echo $member->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to block '+'<?php echo $member->name?>'+' from all time tap service ?'"  data-target='#newstatus'>Block</button>
<?php }else{ ?>
<button href='javascript:void(0)'  class='disable-btn' data-toggle='modal' data-target='#confirmation_pop' 
  onclick="document.getElementById('changing_link').href='?enable_member=<?php echo $member->id;?>';document.getElementById('changin_text').innerHTML='Are you sure to enable '+'<?php echo $member->name?>'+' from all time tap service ?'"  data-target='#newstatus'>Enable</button>
<?php } ?>
							<br>
								<button data-toggle='modal' onclick="notify_pop('<?php echo $member->id ?>')" data-target='#detail_pop' class="notify-btn">Notify</button>	
								<br>
								<button href='javascript:void(0)'  onclick="edit_profile('<?php echo $member->id ?>')" class='disable-btn' data-toggle='modal' data-target='#detail_pop' 
>Update</button>	</td>	
						  </tr>
		<?php } ?>
						</tbody>
					  </table>
					</div>
<!--//four-grids here-->
<script>
function notify_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/notify-user.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}

function detail_pop(id,extra=''){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_member_bank_detail.php?id='+id+"&extra="+extra,true);
xmlhttp.send();
}
function job_detail(id,type){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {

    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/fetch_member_job_detail.php?id='+id+"&type="+type,true);
xmlhttp.send();
}

function edit_profile(id){

var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('detail_pop_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/edit-user-profile.php?id='+id,true);
xmlhttp.send();

}
</script>
<!-- script-for sticky-nav -->
		<script>
		$(document).ready(function() {
			 var navoffeset=$(".header-main").offset().top;
			 $(window).scroll(function(){
				var scrollpos=$(window).scrollTop(); 
				if(scrollpos >=navoffeset){
					$(".header-main").addClass("fixed");
				}else{
					$(".header-main").removeClass("fixed");
				}
			 });
			 
		});
		</script>
		<!-- /script-for sticky-nav -->

			<?php
		    include_once('inc/footer.php');	
			include_once('inc/side_nav.php');
			?>
			<!--inner block start here-->
