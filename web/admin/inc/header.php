<?php 
// page will be redirected when session has expired =====================
require_once "session_active_or_not.php";
?>
<!--
Author: monkhub
Author URL: http://monkhub.com
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Time Tap Super Admin</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/morris.css" type="text/css"/>
<!-- Graph CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- jQuery -->
<script src="js/jquery-2.1.4.min.js"></script>
<!-- //jQuery -->
<link href='//fonts.googleapis.com/css?family=Roboto:700,500,300,100italic,100,400' rel='stylesheet' type='text/css'/>
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!-- lined-icons -->
<link rel="stylesheet" href="css/icon-font.min.css" type='text/css' />
<!-- //lined-icons -->
</head> 
<body>
<?php include_once "admin_pops.php";?>
   <div class="page-container">
   <!--/content-inner-->
<div class="left-content">
	   <div class="mother-grid-inner">
             <!--header start here-->
				<div class="header-main">
		
					<div class="w3layouts-left">
							
							<!--search-box-->
								<div class="w3-search-box">
									<form action="#" method="post" style="display: none">
										<input type="text" placeholder="Search..." required="">	
										<input type="submit" value="">					
									</form>
								</div><!--//end-search-box-->
							<div class="clearfix"> </div>
						 </div>
						 <div class="w3layouts-right">
							<div class="profile_details_left"><!--notifications of menu start -->
								<ul class="nofitications-dropdown">
									
									<li class="dropdown head-dpdn">
										<a href="#" onclick="load_notifications()" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><img src="icons/bell.png" style="width:34px !important"><span class="badge blue" id="unread_bubble">3</span></a>
										<ul class="dropdown-menu" id="notification_loader">
										</ul>
									</li>	
										
									<div class="clearfix"> </div>
								</ul>
								<div class="clearfix"> </div>
							</div>
							<!--notification menu end -->
							
							<div class="clearfix"> </div>				
						</div>
						<div class="profile_details w3l">		
								<ul>
									<li class="dropdown profile_details_drop">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
											<div class="profile_img">	
												<span class="prfil-img"><img src="icons/user.png" alt="" style="width:34px !important;height:34px  !important;border:none"> </span> 
												<div class="user-name">
													<p>Hi, <?php echo $_SESSION['admin_logged'] ?></p>

												</div>
												<i class="fa fa-angle-down"></i>
												<i class="fa fa-angle-up"></i>
												<div class="clearfix"></div>	
											</div>	
										</a>
										<ul class="dropdown-menu drp-mnu">
											<li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> 
											<li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li> 
											<li> <a href="callscripts/signout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
										</ul>
									</li>
								</ul>
							</div>
							
				     <div class="clearfix"> </div>	
				</div>
<!-- ===  dettails wala popup  ============================== -->
<!-- track now  -->
 <!-- Modal -->
  <div class="modal fade" id="detail_pop" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
 
        <div class="modal-body text-center" id="detail_pop_body">
<!-- === ================= Track wala form ========================= -->

<!-- ===  track wala form ============================================ -->
 
        </div>
      <div class="modal-footer modal-close-div">
<a href='javascript:void(0)'   name='update_booking'>Close window</a>
        </div>
      </div>
    </div>
  </div>
<!-- //track now -->

<!--- ==  details wala poppup ends here ===================== -->


