<?php require_once "class/db-connection.php";

function pending_support_count(){
           	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from supporttoken where cleared='0'";
				     $result=$db->query($cmd);
                                   $count=$result->rowCount();

return $count;

}

function pending_redeem_count(){
      	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from redeem_request where redeem_status='pending'";
				     $result=$db->query($cmd);
                                   $count=$result->rowCount();

return $count;


}
?>
<!--/sidebar-menu-->
				<div class="sidebar-menu">
					<header class="logo1">
						<a href="#" class="sidebar-icon"> <span class="fa fa-bars"></span> 

						</a> 
						<img src="icons/logo2.png" width='120'>
					</header>
						<div style="border-top:1px ridge rgba(255, 255, 255, 0.15)"></div>
                           <div class="menu">
									<ul id="menu" >
										<li><a href="index.php"><i class="fa fa-tachometer"></i> <span>Dashboard</span><div class="clearfix"></div></a></li>
										
										
										 <li id="menu-academico" ><a href="all-users.php"><i class="fa fa-envelope nav_icon"></i><span>Job Seekers</span><div class="clearfix"></div></a></li>
									<li><a href="all-company.php"><i class="fa fa-picture-o" aria-hidden="true"></i><span>Company</span><div class="clearfix"></div></a></li>
									<li id="menu-academico" ><a href="all-job.php"><i class="fa fa-bar-chart"></i><span>Posted Jobs</span><div class="clearfix"></div></a></li>
									
									<li id="menu-academico" ><a href=""><i class="fa fa-exclamation-triangle" aria-hidden="true"></i><span>Settlements
<div class="not-bubble" style="<?php 
if(!pending_redeem_count())
echo 'display: none';?>"><?php echo pending_redeem_count();?></div>



									</span><div class="clearfix"></div></a></li>
									 
									 <li><a href="all-disputed-job.php"><i class="fa fa-table"></i>  <span>Disputes

<div class="not-bubble"><?php echo '1';?></div>
									 </span><div class="clearfix"></div></a></li>
									<li><a href="support-tickets.php"><i class="fa fa-map-marker" aria-hidden="true"></i>  <span>Support Tickets
<div class="not-bubble" style="<?php 
if(!pending_support_count())
echo 'display: none';?>"><?php echo pending_support_count();?></div>
									</span><div class="clearfix"></div></a></li>
							       <li><a href="all-admin.php"><i class="fa fa-map-marker" aria-hidden="true"></i>  <span>Admin Access</span><div class="clearfix"></div></a></li>
							       <li><a href="all-courier.php"><i class="fa fa-map-marker" aria-hidden="true"></i>  <span>Courier Service</span><div class="clearfix"></div></a></li>
							              <li><a href="all-printer.php"><i class="fa fa-map-marker" aria-hidden="true"></i>  <span>Printing Service</span><div class="clearfix"></div></a></li>
								  </ul>
								</div>
							  </div>
							  <div class="clearfix"></div>		
							</div>
							<script>
							var toggle = true;
										
							$(".sidebar-icon").click(function() {                
							  if (toggle)
							  {
								$(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
								$("#menu span").css({"position":"absolute"});
							  }
							  else
							  {
								$(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
								setTimeout(function() {
								  $("#menu span").css({"position":"relative"});
								}, 400);
							  }
											
											toggle = !toggle;
										});
							</script>
<!--js -->
<script src="js/jquery.nicescroll.js"></script>
<script src="js/scripts.js"></script>
<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.min.js"></script>
   <!-- /Bootstrap Core JavaScript -->	   
<!-- morris JavaScript -->	
<script src="js/raphael-min.js"></script>
<script src="js/morris.js"></script>

	<!-- ====  live notificatin part starts ===================== -->
<script>
function load_notifications(){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
 document.getElementById('notification_loader').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/load_notification.php?show=1',true);
xmlhttp.send();
}
function notify_user(){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {

 document.getElementById('unread_bubble').innerHTML=xmlhttp.responseText;
    setTimeout(notify_user,7200);
    }
  }
xmlhttp.open("GET",'callscripts/update-notification-counter.php?show=1',true);
xmlhttp.send();
}

    $(document).ready(function(){
notify_user();
    });

</script>

	<!-- === i9ive notifications ends here ======================== -->
</body>
</html>