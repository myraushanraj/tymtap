<?php 
include_once 'class/thread-class.php';
include_once 'class/misc-class.php';
include_once 'class/company-class.php';
include_once 'class/message-data-class.php';
error_reporting(0);
if(isset($_GET['comp']))
{
     $comp=$_GET['comp'];
     $company=new company($comp);
}
if(isset($_GET['apply']))
{
  $thread=new thread($_GET['apply']);
  $thread->apply_job();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/theme-css.css">
<style type="text/css">
    .chat-header {
      height: 36px;
    text-align: center;
    background: #1a76d2;
    color: white;
    font-weight: bold;
    font-size: 16px;
    padding-top: 8px;
    position: fixed;
    margin-left: -27px;
    text-transform: capitalize;
    text-align: center;
    top: 0px;
    z-index: 3333;
}
.post-btn {
    color: #6f6e6e !important;
    /* padding: 4px 20px; */
    font-size: 14px;
    font-family: helvetica;
    /* font-weight: bold; */
    text-decoration: none !important;
}
  .chat-logo{
    width: 60px;
    height: 60px;
    border-radius: 50%;
    border: 2px solid #9a9fa4;

}

.chat-time{
      color: #1a76d2;
    float: right;
    margin-right: 0%;
}

.caht-ul{    margin-top: 25px
      height: 157px;
    position: relative;
}
.chat-content{
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;border-radius: 9px;
    /* position: absolute; */
    margin-left: 2%;
    background: #e6e6e6;
    padding: 3%;
}
#chat_container{
  padding-left: 7%;
    padding-right: 7%;
    margin-bottom: 62px;

}
.fixed-bottom-chat {
    width:100%;
    margin-left: 5%;
    padding-left: 6%;
    min-width: 272px;
    padding-right: 6%;
    position: fixed;
    margin-top: 3px;
    bottom: 0px;
    padding-top: 15px;
    background: #ffffffb3;
}.online-icon{
    background: #4e9e33;
    width:11px;margin-left: 32px;
    height:11px;display: inline-block;
    border-radius: 50%;
}


.chat-textbar{
    height: 47px;
    width:92%;
    overflow-x: hidden;
    resize: none;
    border: 1px solid #7d7d7d;
}

#new_chat_count{
   border-radius: 50%;
    background: #e85907;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    /* float: right; */
    /* margin-right: 4px; */
    margin-bottom: 8px;
    position: absolute;
    z-index: 2222;
    margin-left: -36px;
    margin-top: -32px;
}
</style>
</head>
<body style="" >
<?php  
  $r1=new message_records($_GET['owner']);
  $message_arr=$r1->from_job_seeker_conversation($_GET['owner'],$_GET['member']);
?>
<input type="hidden" id="chat_count_old" value='<?php echo sizeof($message_arr); ?>'>
<div class="agile-grids" id='chat_container' style="max-width: 700px">
</div>
<!-- ===  chat bar ================================ -->
<div class="row fixed-bottom-chat">
<div id="typing-notify">..typing</div>
<textarea class="chat-textbar" id="text_id" onkeypress="doSomething(this, event)"></textarea>
<div id="new_msg">
<a href="#chat_footer"><div id="new_chat_count">
</div></a>

</div>
</div>
<!--- chat bar ends here =========================== -->
<div id="chat_footer"></div>
<script>
function doSomething(element, e) {
    var charCode;

    if(e && e.which){
        charCode = e.which;
    }else if(window.event){
        e = window.event;
        charCode = e.keyCode;
    }

    if(charCode == 13) {
        // Do your thing here with element
        chat_ajax();
    }
}
function chat_ajax(){
  var owner="<?php echo $_GET['owner']?>";
  var member="<?php echo $_GET['member']?>";

  var chat1=$('#text_id').val();
  if(chat1!='')
  {
var add_chat="<?php echo $_GET['member'];?>";

var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 

document.getElementById("text_id").value='';
  window.scrollBy(0,300);
chatRefresh("<?php echo $_GET['member']?>","<?php echo $_GET['owner']?>");
  window.scrollBy(0,300);
 // Scroll 100px to the right
      
    }
  }
xmlhttp.open("GET","callscripts/send_message.php?owner="+owner+"&member="+member+"&message="+chat1,true);
xmlhttp.send();
  }
}
function chatRefresh(member,owner){
  var chat_count_old=$("#chat_count_old").val();
  var chat_count_new=$("#chat_count_new").val();
  var new_chat_count=(+chat_count_new-chat_count_old);
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('chat_container').innerHTML=xmlhttp.responseText;
  
    if(new_chat_count>0)
    {
      $('#new_msg').show();
      document.getElementById('new_chat_count').innerHTML=new_chat_count;
    }else{
        $('#new_msg').hide();
    }
    }
  }
xmlhttp.open("GET",'callscripts/chat-loader-refresh.php?member='+member+"&owner="+owner,true);
xmlhttp.send();
}
function is_typing(comp){
var typing=$("#text_id").is(':focus');
if(typing)
 typing=1;
else
 typing=0;
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
if(xmlhttp.responseText==1)
document.getElementById("typing-notify").style.display='block';
 else
  document.getElementById("typing-notify").style.display='none';
 // Scroll 100px to the right
    }
  }
xmlhttp.open("GET","callscripts/other-side-typing.php?member="+comp+"&typing="+typing,true);
xmlhttp.send();
  }
    $(document).ready(function(){

      refreshchat();
    });
    function refreshchat(){

chatRefresh("<?php echo $_GET['member']?>","<?php echo $_GET['owner']?>");
is_typing("<?php echo $_GET['member']?>");
   setTimeout(refreshchat,4300);
   }
  $(window).scroll(function() {
   if($(window).scrollTop() + $(window).height() == $(document).height()) {
// logic for user has voewed the ,essage 
  $('#new_msg').hide();
  document.getElementById('chat_count_old').value=$("#chat_count_new").val();
   }
});
</script>
   </body>
   </html>