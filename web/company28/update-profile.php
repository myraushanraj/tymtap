<?php require_once("inc/header.php"); 
require_once("class/company-class.php");
$company=new company($_SESSION['company_id']);
?>
<div class="container bottom_padding bottom_margin">


<div class="col-md-12 register_wrap">
       <div class="tab-content">
    <div id="home" class="tab-pane fade in active">

<form action="callscripts/update-profile.php" method="post">

    <h4>Company title</h4>
        <div class="form-group">
                <input type="text"  value="<?php echo $company->title ?>" class="form-control" rows="5" id="title1" name="title1" required >
        </div>

    <h4>Company about</h4>
        <div class="form-group">
       <textarea class="form-control" rows="5" name="about1" id="about1"  required >
         <?php echo $company->about ?>
       </textarea>
          </div>

    <h4>Registered Address</h4>
        <div class="form-group">
                <textarea class="form-control" rows="5" name="address1" required id="address1">
                  <?php echo $company->address ?>
                </textarea>
          </div>


    <h4>Mobile Number</h4>
        <div class="form-group">
                <input type="number" name="mobile1" id='mobile1' value="<?php echo $company->mobile?>" class="form-control" style="width:200px">
                <a href="javascript:void(0)" onclick="verify_number()" >Verify Now</a>
          </div>


	 <h4>Category <span style="color:red"><?php echo $company->category?></span></h4>	
	 <div class="row center" style="display:none">
	 	<div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('IT')" class="btn_bottom">&nbsp;&nbsp; IT &nbsp; &nbsp; </a></div>
	 	<div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('Design')" class="btn_bottom">Design</a></div>
        <div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('Marketing')" class="btn_bottom">Marketing</a></div>

	 </div>
	  <div class="row center top_margin"  style="display:none">
	 	<div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('Events')" class="btn_bottom">Events</a></div>
	 	<div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('Automonbile')" class="btn_bottom">Automobile</a></div>
        <div class="col-md-3"><a href="javascript:void(0)" onclick="add_cat('Manufacturing')" class="btn_bottom">Manufacturing</a></div>

	 </div>

<!-- ----  company's catwegory hideen input ================ -->
<input type="hidden" name="category" id="cat_id">

<!--- =  category ends here ================================= -->

	  <div class="row center top_margin">
	 	<div class="col-md-3"><a href = "javascript:void(0)" class="btn_bottom">Others</a></div>
	 	<div class="col-md-3"></div>
     <div class="col-md-3"></div>

	 </div>
            <h4>If others please mention <input type="text" class="bottom_border" name="other_category" > </h4>
             <h4>Supervisor Details</h4>
	      <div class="form-group">
                <input type="text" readonly class="form-control" value="<?php echo $company->email?>" placeholder="Supervisor Email" name="supervisor_email">
          </div>
          <div class="form-group">
                <input type="password" readonly class="form-control" value="<?php echo $company->password?>" placeholder="Supervisor password"  name="supervisor_password">
          </div>
	  <input type="submit" style="display:none1" value="Save" class="submit_btn" name="update_profile">  
	  </form>    
    </div>

  </div>
</div>

</div>
<script>
function add_cat(cat){
document.getElementById('cat_id').value=cat;
}

function verify_number(){

var mobile=document.getElementById('mobile1').value;
window.location="otp-verification.php?mobile="+mobile;

}
</script>
<?php include_once "inc/footer.php"; ?>