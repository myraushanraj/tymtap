<?php require_once("inc/header.php"); 
include_once "inc/chat-box.php"; 
require_once("class/member-class.php");
require_once("class/transaction-class.php");
$trans=new transaction('1724');
$trans_arr=$trans->all_transaction();
?>
<style>
.add-money{
      color: #a9a2a2;margin-right: 21px;
}
.icon-bank{
  width:29px;
  margin-right: 12px;
}
.currency{
  font-size: 33px;
    color: #464242;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #555;
    cursor: default;
    background-color: transparent;
    border-bottom: 2px solid #ff0000 !important;}
.nav-tabs{
      text-align: center;
    padding-left: 44%;
    margin-top: 47px;
}
table{
      color: grey;
    width: 90%;
    margin-left: 10%;
    text-align: left;
}
.close2{
      background: transparent;
    color: white;
    font-size: 15px;
    text-transform: unset;
    border: none !important;
    outline: none !important;
}
.input2{
  color: #0d4b8e;
    width: 200px;
    height: 33px;
    text-align: center;
    font-size: 16px;
    background: none;

}
.modal-body {
    background:white !important;
}
.modal-header {
    padding: 15px;
    border-bottom: none;
}
.modal-title{
  color: black;
}
</style>
  <!-- Modal -->
  <div class="modal fade" id="redeem_money" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Redeem Account</h4>
        </div>
        <div class="modal-body text-center" id="redeem_body">
          <p>Choose Your Amount </p>
          <input type="number" id='redeem_amount' value="<?php echo $trans->balance?>" style="color:red">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn close2"  onclick="redeem_money()" >Redeem Money</button>
        </div>
      </div>
      
    </div>
</div>

<!-- ==  add money ============== -->
<!-- ==  add money =============== -->

  <!-- Modal -->
  <div class="modal fade" id="add_money" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Money</h4>
        </div>
        <div class="modal-body text-center" >
          <p>Choose Your Amount </p>
          <input type="number" id='amount' value="999" style="color:red">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn close2" onclick="make_pay()">Add Money</button>
        </div>
      </div>
      
    </div>
</div>

<!-- ==  add money ============== -->
<div class="container text-center" style="font-size:18px">
  <h2>Wallet</h2>
  <p class="red">Your balance</p>
<strong>&#8377; 4500</strong>


<div class="row text-center" style="margin-top: 22px">
<a href="javascript:void(0)" class="add-money" data-toggle="modal" data-target="#add_money"><span><img src="icon/add.png" class="icon-bank"></span>Add money to wallet</a>
<a href="javascript:void(0)" class="add-money" data-toggle="modal" data-target="#redeem_money"><span><img src="icon/house.png" class="icon-bank"></span>Return Money to Bank</a>
</div>

  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">All</a></li>
    <li><a data-toggle="tab" href="#menu1">Deposit</a></li>
    <li><a data-toggle="tab" href="#menu2">Redeem</a></li>
  </ul>

  <div class="tab-content" style="margin-top:12px">
    <div id="home" class="tab-pane fade in active">
      <h3>All</h3>
      <p>
<!-- == table ========= -->        
                  <table>
                      <tr><th>Date</th><th>Action</th><th>Amount</th><th>Balance</th></tr>
                        <?php for ($i=0; $i < sizeof($trans_arr); $i++) { 
                          # code...
                           ?>
                           <tr><td><?php echo $trans_arr[$i]['timings']?></td><td><?php echo $trans_arr[$i]['type1']?></td><td>
                            <?php echo $trans_arr[$i]['amount']?>
                           </td><td><?php echo $trans_arr[$i]['balance']?></td></tr>

                          <?php 
                        } ?>
                      
                  </table>
<!-- == table ends ==-->
      </p>
    </div>
    <div id="menu1" class="tab-pane fade">
    

                  <table>
                      <tr><th>Date</th><th>Action</th><th>Amount</th><th>Balance</th></tr>
                        <?php for ($i=0; $i < sizeof($trans_arr); $i++) { 
                          # code...
                          if($trans_arr[$i]['type1']=='deposit')
                          {
                           ?>
                           <tr><td><?php echo $trans_arr[$i]['timings']?></td><td><?php echo $trans_arr[$i]['type1']?></td><td>
                            <?php echo $trans_arr[$i]['amount']?>
                           </td><td><?php echo $trans_arr[$i]['balance']?></td></tr>

                          <?php 
                        } } ?>
                      
                  </table>




    </div>

  <div id="menu2" class="tab-pane fade">
    

                  <table>
                      <tr><th>Date</th><th>Action</th><th>Amount</th><th>Balance</th></tr>
                        <?php for ($i=0; $i < sizeof($trans_arr); $i++) { 
                          # code...
                          if($trans_arr[$i]['type1']=='withdraw')
                          {
                           ?>
                           <tr><td><?php echo $trans_arr[$i]['timings']?></td><td><?php echo $trans_arr[$i]['type1']?></td><td>
                            <?php echo $trans_arr[$i]['amount']?>
                           </td><td><?php echo $trans_arr[$i]['balance']?></td></tr>

                          <?php 
                        } } ?>
                      
                  </table>




    </div>
  </div>
</div>

<script>
  function make_pay(){

    var amount=document.getElementById('amount').value;
    window.location="pay_now.php?amount="+amount;
  }
  function redeem_money(){
  var redeem_amount=$('#redeem_amount').val();

  var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {

    document.getElementById('redeem_body').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/redeem_money.php?amount='+redeem_amount,true);
xmlhttp.send();
}
</script>


</body>
</html>