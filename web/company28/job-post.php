<?php
include('inc/header.php');
?>
<div class="container">   
      <form action="callscripts/new-job.php" method="post" class="width_register  gap-top-padding bottom_margin">
         <h3 class="theme_color">Hi, <?php echo $_SESSION['company_name']?>! Post Your First Job</h3>
         
         <br>
  <div class="form-group">
    <label for="email">Title of the job</label>
    <input type="text" class="form-control" id="job_title" name="job_title">
  </div>
  <div class="form-group">
    <label for="email">Describe your job</label>
    <textarea class="form-control" rows="5" id="job_description" name="job_description"> </textarea>
  </div>
  <div class="form-group">
    <label for="email">Enter skill needed</label>
    <input type="text" class="form-control" id="skill_needed" name="skill_needed">
  </div>

    <div class="form-group">
    <label for="email">Fare Rate</label>
    <input type="number" class="form-control" name="job_fare_rate" id="job_fare_rate">
  </div>
   <div class="form-group">
    <label for="email">Number of hours of working</label>
    <input type="number" class="form-control" name="working_hours" id="working_hours">
  </div>
  <div class="form-group">
    <label for="email">Number of freelancer</label>
    <input type="number" class="form-control" name="no_of_freelancer" id="no_of_freelancer">
  </div>
   <div class="form-group">
    <label for="email">Duration of the job (in days)</label>
    <input type="number" class="form-control" name="duration_in_days" id="duration_in_days">
  </div>
  <div class="form-group">
    <label for="email">Deadline to apply</label>
    <input type="date" class="form-control" name="deadline_date" id="deadline_date">
  </div>
  <div class="form-group">
    <label for="email">Location of the event/job</label>
    <input type="text" class="form-control" name="job_location" id="job_location">
  </div>
  <div class="form-group">
    <label for="email" style="width: 100%">Start time/End time</label>
    <input type="date" class="form-control from_date" name="start_date" id="start_date" style="">
    <input type="date" class="form-control to_date" name="end_date" id="end_date" style="">

  </div>
  <div class="form-group">
    <label for="email">Please paste the Interview kind of queries as a quick reference for selecting the job seekers</label>
        <textarea class="form-control" rows="3" name="ques1" > </textarea>

  </div>
  <div class="form-group">

        <textarea class="form-control" rows="3" name="ques2" > </textarea>
   
  </div>
  <div class="form-group">

        <textarea class="form-control" rows="3" name="ques3"> </textarea>
   
  </div>
   <div class="form-group" id='ques_id4' style="display:none">

        <textarea class="form-control" rows="3" name="ques4" > </textarea>
   
  </div>
   <div class="form-group" id='ques_id5' style="display:none">

        <textarea class="form-control" rows="3" name="ques5" > </textarea>
   
  </div>
   <div class="form-group" id='ques_id6' style="display:none">

        <textarea class="form-control" rows="3" name="ques6" > </textarea>
   
  </div>
   <div class="form-group" id='ques_id7' style="display:none">

        <textarea class="form-control" rows="3" name="ques7" > </textarea>
   
  </div>


<a href="javascript:void(0)" onclick="add_more_ques()" id="add_btn_id">Add More</a>
    <div class="form-group" >

        <input type="hidden" name="city" id='city' value="delhi"> 
      </div>
  <button type="submit" name="new_job" class="submit_btn">Post a Job</button>
</form>

    </div>
<script>
  function add_more_ques(){
    var p;
    for(p=4;p<8;p++)
    {
      if(p==7)
      {
        document.getElementById('add_btn_id').style.display='none';

      }
      var ques_id_string="ques_id"+p;
    var this_vis=document.getElementById("ques_id"+p).style.display;
    if(this_vis=='none')
    {
      document.getElementById("ques_id"+p).style.display='block';
      break;
    }

    }
  }

</script>
  
</body>
</html>

