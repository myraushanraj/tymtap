<html>

<head>
	
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
	<header>
		<style>
		body{
		bodywidth:100%;
	font-size:75%;
    font-family: Helvetica, Arial, Sans-Serif;
    margin-left: auto;
    margin-right: auto;
}
.avatar {
    border-radius: 50%;
    margin-top: 10px;
}
		
	.red
	{color: red}

	.data-box{
		padding-top: 8px;
	}
	.data-box1{
		padding-top: 8px;
	}
	</style>
	</header>
<div class="container" style="margin-top: 20px;">
	<div class="col-md-12" style="border: 1px solid #e6e6e6;">
		<h3 style="color: #8c8c8c;"><b>My Wallet</b></h3><span style="color: #8c8c8c;">be your own bank.</span>
		<h3 style="float: right; margin-top: -35px; color: #8c8c8c;"><b>0.22684555 BTC</b></h3>
		<br>
		<span style="float: right; margin-top: -20px; font-size: 25px; color: #8c8c8c;"><b>$8.34</b></span>
		<br>
		
		<input type="submit" name="" value="receive">
		<input type="submit" name="" value="send">
	</div>
</div>

<div class="container">
<div class="row" style="margin-top: 15px;">
       <div class="col-md-6">
	        <h6>Most Recent Activity</h6>
       </div>	
       <div class="col-md-1"></div>
        <div class="col-md-5">
             <h6>Balance</h6>
        </div>
</div>	
<!-- data sets ================== -->
<div class="row" style="margin-top: 10px;">

	<div class="col-md-6 data-box">
		<div class="row" style="border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar" >
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4" ><span style="float: right;"><strong class="red">Sent</strong>1.4 BTC</span></div>

        </div>
        <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Deposit</strong>0.05247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.005247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Deposit </strong>0.00547 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>
         <div class="row" style="margin-top: 20px; border: 1px solid #f2f2f2;">
			<div class="col-md-1">
				<img src="menu.png" width="18" class="avatar">
			</div>
              <div class="col-md-3">Transection</div>
              <div class="col-md-3">July28,2018</div>
              <div class="col-md-4"><span style="float: right;"><strong class="red">Sent </strong>0.00586247 BTC</span></div>

        </div>


    </div>
    <div class="col-md-1"></div>

    <div class="col-md-5 data-box1" >
       <div class="row" style="border: 1px solid #f2f2f2;">

            <div class="col-md-6">
		          My Bitcoin Wallet
              </div>
            <div class="col-md-6">
		          <span style="float: right;">0.21965404 BTC</span>
            </div>
         </div>
          <div class="row">

            <div class="col-md-6">
		          Savings
              </div>
            <div class="col-md-6">
		          <span style="float: right;">0.0021965404 BTC</span>
            </div>
         </div>
         <div class="row" style="border: 1px solid #f2f2f2;">

            <div class="col-md-6" style="color: #f2f2f2;">
		          Imported Address
              </div>
            <div class="col-md-6" style="color: #d9d9d9;">
		          <span style="float: right;">0.0022648555 BTC</span>
            </div>
         </div>
         <div class="row" style="color: #8c8c8c;">

            <div class="col-md-6">
		          Total
              </div>
            <div class="col-md-6">
		          <span style="float: right;">0.002264855555 BTC</span>
            </div>
         </div>

      </div>

  </div>	
</div>





</div>	





</body>
</html>