<?php require_once("inc/header.php"); 
include_once "inc/chat-box.php"; 
require_once("class/member-class.php");
$member=new member($_GET['user']);
$member->task_data();

?>
  <style>
.avatar {
    vertical-align: middle;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    margin-top: 10px;
}
.avatar1 {
    vertical-align: middle;
    width: 120px;
    height: 120px;
    border-radius: 50%;
    margin-top: 10px;
    margin-left: 100px;
}
span{
  font-size:15px;
}
.checked {
    color: orange;
}
img{
  margin-right: 8px;
}
h4{
    color: #4a4343;
    font-weight: bold;
    font-family: helvetica !important;
    margin-top: 12px;
}
</style>


<div class="container" style=" margin-top:60px;">
  <div class="row">
    
    <div class="col-md-1">
      

    </div>
        <div class="col-md-4" style="background-color: white; border: 1px solid grey;">
          <img src="image2.jpg" alt="Avatar" class="avatar1">
          <br>
  
          <h4 style="margin-left: 100px;"><?php echo $member->name ?></h4>
          
          <span style="margin-left: 100px;"><?php echo $member->designation?></span>
          <br>

          <span style="margin-left: 60px;">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <b>5.0</b>(<?php echo $member->total_count?> Reviews)
          </span>

          <br>
          <br>
          <input type="submit" class="btn btn-success" value="Contact Me">
          <input type="submit" class="btn" value="Custome Order">
          <br>
          <br>
                <img src="icon/location.png" width="22"><span style="color: grey;">From</span>
          <span style="float: right;"><b><?php echo $member->hometown?></b></span>
          <br>
           <img src="icon/man-user.png" width="22"><span style="color: grey;">Member Since</span>
          <span style="float: right;"><b>
            <?php $datearr=explode(' ',$member->registered_on);
            echo $datearr[0]?>

          </b></span>
          <br>
                <img src="icon/clock.png" width="22"><span style="color: grey;">Avg Responce Time</span>
          <span style="float: right;"><b>1 Hour</b></span>
          <br>
         <img src="icon/right-arrow.png" width="22"><span style="color: grey;">Recent delivery</span>
          <span style="float: right;"><b>9 days</b></span>


      

    </div>
        <div class="col-md-1">
      

    </div>
        <div class="col-md-6" style="background-color: white; border: 1px solid grey;">
      <h4>Description</h4>

      <span><?php echo $member->about?></span>
      <br><br>
      <h4>Job Statistics</h4>
      Ongoing -<span style="color: grey;"> <?php echo $member->ongoing_count?><br></span>
      Delivered <b></b> -<span style="color: grey;"> <?php echo $member->paid_count?><br></span>
    
    Active -<span style="color: grey;"> <?php echo $member->pending_count?><br></span>
      <br>
      <br>
      <h4>Skills</h4>
      <span>Photography, Photoshop, EditingGraphic, DesignPhoto Editing</span>

    </div>
  </div>

  <div class="row" style="margin-top: 50px;">
  <div class="col-md-1">
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey;">
    
<h4>Chandan's Reviews</h4>
<span>
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <b>5.0</b>(14 Reviews)
          </span>

  </div>
</div>

  <div class="row" style="">
  
  <div class="col-md-1">
      

    </div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 80px;">

<div class="row">
  <div class="col-md-1">
  
</div>
<div class="col-md-3" style="margin-top: 20px;">
  Seller Communication
  <br>
  <span style="margin-left: 25px;">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  </span>
  
</div>
  <div class="col-md-1">
  
</div>
<div class="col-md-3" style="margin-top: 20px;">
  Service as Described
  <br>
  <span style="margin-left: 25px;">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  </span>
</div>
<div class="col-md-1">
  
</div>
<div class="col-md-3" style="margin-top: 20px;">
  Would Recommend
  <br>
  <span style="margin-left: 25px;">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
  <img src="icon/star.png" width="20">
</span>
</div>  

</div>
  </div>

  </div>
  <div class="row">
    <div class="col-md-1">
  
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
  <img src="image3.jpg" alt="Avatar" class="avatar">  
Louiemolina <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          
<br>
It was an amazing experience working with Jim. He kept me up to date on everything. He's very talented.
<br>
<span style="font-size: 12px;">9 days ago</span>
  </div>
</div>
    <div class="row">
  <div class="col-md-1">
  
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
    <img src="image2.jpg" alt="Avatar" class="avatar">
Mantangs <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          
<br>

It was really hard to find good footage for my case. However, once I got one, Jim did the great work. The video was really great quality.
<br>
<span style="font-size: 12px;">22 days ago</span>
  </div>

</div>
<div class="row">
  <div class="col-md-1">
  
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
    <img src="image1.jpg" alt="Avatar" class="avatar">
Markusfrench <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          
<br>
Fast and good and has an eye for making videos.
<br>
<span style="font-size: 12px;">23 days ago</span>

  </div>

</div>

<div class="row">
  <div class="col-md-1">
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
    <img src="image3.jpg" alt="Avatar" class="avatar">

Kaveh96 <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          
<br>
Great work, thanks
<br>
<span style="font-size: 12px;">26 days ago</span>
  </div>
</div>
<div class="row">
  <div class="col-md-1">
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 170px;">
    <img src="image2.jpg" alt="Avatar" class="avatar">
Nerdcultcha <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <span class="fa fa-star checked "></span>
          <span class="fa fa-star "></span>
          
<br>
Overall I am delighted with the work produced by Jim, and working with him has been a joy. He responded to messages promptly and has a great approach to dealing with any queries. His passion is obvious and I canâ€™t recommend him enough. If I have any other projects lined up in the future needing this sort of thing, I will be straight back here!
<br>
<span style="font-size: 12px;">27 days ago</span>
  </div>
</div>

<div class="row">
  <div class="col-md-1">
</div>
  <div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 150px;">
<img src="image1.jpg" alt="Avatar" class="avatar">
Perry5r <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <img src="icon/star.png" width="20">
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          
<br>
John is a pleasure to work with and produces a superb result. His standards are extremely high and he clearly put great thought and talent into the project.
<br>
<span style="font-size: 12px;">about 1 month ago</span>
  </div>
</div>



</div>



<script>
function add_cat(cat){
document.getElementById('cat_id').value=cat;
}
function verify_number(){
var mobile=document.getElementById('mobile1').value;
window.location="otp-verification.php?mobile="+mobile;
}
</script>
<?php include_once "inc/footer.php"; ?>