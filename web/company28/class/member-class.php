<?php 
include_once 'db-connection.php';
include_once 'misc-class.php';
class member{

function __construct($mid){
$this->id=$mid;
$this->member_data();
$this->my_rating();
$this->bank_data();
}
				function member_data(){
					
	               	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from organisation_team where member_id='$this->id'";
				     $result=$db->query($cmd);
				     // echo $cmd;
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
				                                   
					                   $this->name=$arr1[0]['name'];
					                   $this->email=$arr1[0]['email'];
					                   $this->link="freelancer-profile.php?user=".$this->id;
					                   $this->about=$arr1[0]['about_me'];
					                   $this->username=$arr1[0]['username'];
					                   $this->designation=$arr1[0]['designation'];
					                   $this->phone=$arr1[0]['phone'];
					                   $this->last_login=$arr1[0]['lastlogin'];
					                   $this->registered_on=$arr1[0]['created_on'];
					                   $this->registered_ago=return_timespan($arr1[0]['created_on']);
					                   if(!empty($arr1[0]['profile_pic']))
					                   $this->pic='../img-profile/'.$arr1[0]['profile_pic'];
					               	    else
					               		$this->pic='../img-profile/unknown.png';
					               		$this->hometown=$arr1[0]['hometown'];
					               		$this->passed_out=$arr1[0]['institute'];
					               	    $this->is_online=$arr1[0]['is_online'];
			}

						function bank_data(){
					
	           	    $d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from bank_process where member_id='$this->id'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
				                                               if(!empty($arr1[0]['bank_account']))
				                                               { 
					                   $this->account_number=$arr1[0]['bank_account'];
					                   $this->holder=$arr1[0]['account_holder_name'];
					                   $this->ifsc=$arr1[0]['IFSC_code'];
					         		   $this->bank_verified=$arr1[0]['verified_paid'];
					                   $this->location=$arr1[0]['bank_location'];
					                   $this->bank=$arr1[0]['bank_name'];
					              								}else{
					              	   $this->account_number='not updated';
					                   $this->holder='not updated';
					                   $this->ifsc='not updated';
					                   $this->location='not updated';
					                   $this->bank='not updated';	
					             								}
			}
		function all_category_jobs($type){
			if($type=='pending')
				$type_form='accepted';
			elseif ($type=='pay_due') 
				$type_form='checked_out';
			elseif ($type=='ongoing') 
				$type_form='checked_in';
			elseif ($type=='paid') 
				$type_form='payment_done';
			    else
				$type_form='all';
		 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
	               	if($type_form=='all'){
	            $cmd="SELECT thread_id from job_process where job_id='$this->id'";
	               	}else{
                $cmd="SELECT thread_id from job_process where job_id='$this->id' and thread_status='$type_form'";
        			}
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
				                                               return $arr1;
		}			              
		
			function task_data()
			{
				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			$cmd="SELECT * from job_application where job_id='$this->id' and job_status='accepted'";
				     $result=$db->query($cmd);
				       $this->pending_count=$result->rowCount();
				      $cmd="SELECT * from job_application where job_id='$this->id' and job_status='checked_in'";
				     $result=$db->query($cmd);echo $cmd;
				     $this->ongoing_count=$result->rowCount();
				      $cmd="SELECT * from job_application where job_id='$this->id' and job_status='payment_done'";
				     $result=$db->query($cmd);echo $cmd;
				      $this->paid_count=$result->rowCount();
				     $cmd="SELECT * from job_application where job_id='$this->id' and job_status='checked_out'";
				     $result=$db->query($cmd);
				        $this->unpaid_count=$result->rowCount();
				     $cmd="SELECT * from job_application where job_id='$this->id' and job_status='disputed'";
				     $result=$db->query($cmd);    
				      $this->disputed_count=$result->rowCount();
				      $this->total_count=$this->paid_count+$this->ongoing_count+$this->unpaid_count+$this->pending_count;   

// last active 
				      $cmd="SELECT timings as time1 from job_application where job_id='$this->id' ORDER BY timings DESC LIMIT 0,1";
				     $result=$db->query($cmd);
				     while($row=$result->fetch()){ 
				            $this->last_active=$row['time1'];
				            $this->last_active_ago=return_timespan($row['time1']);
				                                 }
			}
			function user_custom(){
				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from organisation_team where member_id='$this->id'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
					                   $this->theme_color=$arr1[0]['theme'];
					                   $this->email_allowed=$arr1[0]['email_allowed'];
					                   $this->msg_allowed=$arr1[0]['msg_allowed'];    
			}

function member_job_delivered(){
	 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT thread_id from job_process where thread_id IN (SELECT thread_id from job_application where job_id='$this->id')";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                                }
				                                                return $arr1;

}
	function job_data(){
				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT * from job_id_data where job_id='$this->id'";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }
					                   $this->rating=$arr1[0]['rating'];
					                   $this->job_active=$arr1[0]['job_active'];
					                   $this->job_count=$arr1[0]['job_count'];
					                   $this->last_review=$arr1[0]['last_review'];

					               



			}
			function all_review(){

				 	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			$cmd="SELECT * from reviews where request_id IN (SELECT request_id from job_application where job_id='$this->id')";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($arr1,$row);
				                                               }

				                                          return $arr1;
			}

			function my_rating(){

                	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();$arr1=array();
        			 $cmd="SELECT AVG(rating) as my_rating from reviews where request_id IN (SELECT request_id FROM job_application where job_id='$this->id')";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                 $rating=$row['my_rating'];
				                                               }

				                                               $this->rating=$rating;
				                                               if(!$this->rating)
				                                               	$this->rating=7;				                      
				                                               	 return $rating;                        
			}

			function view_all_messages(){
			   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT mid from message_history where member_id='$id' and reply_text IS  NULL";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }

				           // 2 nds case where user is a sender
        			 $cmd="SELECT mid from message_history where triggered_from='$id' and reply_seen='0' and reply_text IS Not  NULL";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }
				                   	                    	   return $arr1;        	                    	   
}


}



?>