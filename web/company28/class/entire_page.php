<?php
include_once('inc/header.php');include_once 'class/thread-class.php';
include_once "callscripts/job_process_action.php"; 

?>
<style>
    .chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}


#new_chat_count{
      border-radius: 50%;
    background: green;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    float: right;

    margin-right: 2px;
}

#typing-notify{
      background: #fdfdff;
    padding-left: 17%;
    height: 26px;
    width: 100%;
}
.online-icon{
    background: #4e9e33;
    width:11px;
    height:11px;display: inline-block;
    border-radius: 50%;
}

h6.company-title{
      font-size: 15px;
    font-weight: bold;
}


::-webkit-scrollbar {
    width:10px;
}
::-webkit-scrollbar-track {
    background-color: #eaeaea;
    border-left: 1px solid #ccc;
}
::-webkit-scrollbar-thumb {
    background-color: #ccc;
}
::-webkit-scrollbar-thumb:hover {
    background-color:grey;
}
.chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

.chat-time {
    color: #1a76d2;
    float: right;
    margin-right: 5%;
}
.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}
#msg-close-btn{
    margin-right: 20px;
    color: #c2c2c3;
    font-family: cursive;
    margin-top: 9px;
    position: absolute;
    z-index: 2222222222;
    margin-left: 91%;
}
.chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

#new_chat_count {
    border-radius: 50%;
    background: #ffb748;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    float: right;
    margin-right: 2px;

}
.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}
</style>
<div class="menu admin-tabs">
 <div class="container">
  <ul class="nav nav-tabs">
    <li><a href="job-post.php">Create Job</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#"> My Projects <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="#">Submenu 1-1</a></li>
        <li><a href="#">Submenu 1-2</a></li>
        <li><a href="#">Submenu 1-3</a></li>                        
      </ul>
    </li>
    <li><a data-toggle="tab" href="#menu1">Payments</a></li>
    <li class="active"><a data-toggle="tab" href="#jobs_posted">Jobs Posted</a></li>
    <li><a data-toggle="tab" href="#requests" onclick="view_requested_application()">Requests</a></li>
    <li><a data-toggle="tab" href="#on_going_job">On-Going Job</a></li>
     </ul>
 </div>
</div>
<div class="container below-header">
  <div class="tab-content bottom_margin">

    <div id="menu1" class="tab-pane fade">
     <div class="col-md-9" >

     	<div class="wrap_payment top_margin">
     		<h5><img src="user/pp.png" class="img_round"><span>&nbsp; Rahul Mittal</span><span class="paid">Paid</span></h5>
     		<h5 class="theme_color">Freelancer for Amazon Marketing Event</h5>
     		<table>
     			<tr><td>Date of Event </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </td><td class="theme_color"> 01/01/2018</td></tr>
     			<tr><td>Check -In Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 04:01 pm(Delay by 1 minute)</td></tr>
     			<tr><td>Check Out Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 08:01 pm</td></tr>
     			<tr><td>Amount </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Rs 3000</td></tr>
     			<tr><td>Location </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Pragati Maidan</td></tr>
     		</table>
     		<div style="position: relative;">
          <h5 class="generate"><span class="paid">Generate Invoice</span></h5>
          </div>
     		
     	</div>
     </div>
     <div class="col-md-3 top_margin">
     	<br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border">Filter</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;All</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Paid</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Payment</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Date</li>
     	    </div>
     	
     </div>
    </div>



    <div id="jobs_posted" class="tab-pane fade in active">
      
        <div class="wrap_payment top_margin">
     		
  <?php 
        $all_thread=companies_thread($_SESSION['company_id']);
        if(!sizeof($all_thread))
            echo "<strong style='font-size:17px'>You have not posted any jobs at Time tap ! Post a new job today</strong>";
        for($k=0;$k<sizeof($all_thread);$k++)
        {                
                 $thread=new thread($all_thread[$k]['thread_id']);
                 echo $thread->company_thread_box();
        } ?>
     	</div>

    </div>

    <div id="on_going_job" class="tab-pane fade">
  
    <div id="ongoing_jobs">
     <div class="col-md-8" >

     	<div class="wrap_payment top_margin">
     		<h5><img src="user/pp.png" class="img_round"><span>&nbsp; Rahul Mittal<br><span class="location">New Delhi</span></span><span class="paid">Check-In</span></h5>
     		<h5 class="theme_color">Freelancer for Amazon Marketing Event</h5>
     		<table>
     			<tr><td>Date of Event </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </td><td class="theme_color"> 01/01/2018</td></tr>
     			<tr><td>Check -In Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 04:01 pm(Delay by 1 minute)</td></tr>
     			<tr><td>Check Out Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 08:01 pm</td></tr>
     			<tr><td>Amount </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Rs 3000</td></tr>
     			<tr><td>Location </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Pragati Maidan</td></tr>
     		</table>
     		<div style="position: relative;">
          <h5 class="generate"><span class="chat" style="">Chat Now</span></h5>
          </div>
     		
     	</div>
     </div>
     </div>
     <div class="col-md-4 top_margin">
     	<br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border theme_color">Filter</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;All</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Paid</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Payment</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Date</li>
     	    </div>
     	    <br><br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border theme_color
     	            ">Announcements</li>
     	            <li>The meeting will be held after the event at 07:00 p.m. in the main shop</li>
     	            <li>Please collect your badges before starting the job</li>
     	            <br><br><br><br>
     	            <li style="background: #cecece;"><input type="text" style="width: 80%" style="border: none;" name=""><span>&nbsp; Send</span></li>
     	            
     	    </div>
     	
     </div>
    </div>

    <div id="requests" class="tab-pane fade">
    </div>



  </div>
</div>



<div id="messenger-chat" style="display:none">
<a href="javascript:void(0)" onclick="this.style.display='none';close_messenger()" id="msg-close-btn">X</a>
<iframe src="" id="msg_id"></iframe>
</div>
<script>  
function open_messenger(member){

  document.getElementById('msg_id').src="my-chat-small.php?member="+member;
  $("#messenger-chat").show();
    $("#msg-close-btn").show();

}
function close_messenger(){
  $("#messenger-chat").hide();
}

function view_requested_application(){

var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {

    document.getElementById('requests').innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET",'callscripts/load_all_application_requests.php?show_all=1',true);
xmlhttp.send();
}


function load_interview_answers(request_id){

var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {

 document.getElementById("interview_answer_body").innerHTML=xmlhttp.responseText;

    }
  }
xmlhttp.open("GET",'callscripts/view_job_seeker_answer.php?request_id='+request_id,true);
xmlhttp.send();
}

</script>
</body>
</html>

