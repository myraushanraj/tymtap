<?php require_once('pdf/fpdf.php');
include_once"class/thread-class.php";
include_once"class/notification-class.php";
include_once"class/transaction-class.php";
if(isset($_GET['invoiced']))
{
  $request_id=$_GET['invoiced'];
$app=new application($_GET['invoiced']);
$thread=new thread($app->thread_id);
date_default_timezone_set("Asia/calcutta");
$timings =(date('Y-m-d H:i:s'));
// step 1: Create an ordered Invoice ==================================
create_order_invoice($app->job_seeker_name,$app->job_seeker_phone,$app->job_seeker_email,$thread->billed,$app->job_title,$timings,$app->id,'');
$thread->new_status('payment_done');

$db=new dbconn();
$con=$db->connect();
// step 2: Update the payment status for this application ============== 
$cmd="UPDATE job_application set job_status='payment_done' where request_id='$request_id'";echo $cmd;
$query=$con->prepare($cmd);
$query->execute(array());
// step 2: Update the payment status for this application ============== 

$cmd="UPDATE job_payment set status='paid' where request_id='$request_id'";
$query=$con->prepare($cmd);
$query->execute(array());

// step 3; relaese the same amount to the transaction table4 

$t1=new transaction(($app->job_seeker));
$t1->new_transaction(($thread->billed),'deposit',($thread->id));

/*------- push notification ========================= */
$admin=new notification($app->job_seeker);
$link="";
$from=$_SESSION['company_id'];
$notification="Congratulations ! : ".$app->job_seeker_name." Your Invoice is generated for request Id : ".$app->thread_id;
$admin->push_notification($notification,$from,$link);
?>
<script>
	window.location="index.php?show_pay=1";
</script>

<?php 
}


function create_order_invoice($customername,$phone,$email,$bill,$stringitems,$time,$orderid,$plan_id){

	$address="New Delhi";

// calculate the final bill here =================
$domain='www.timetap.com';
$gst=0.12*$bill;
$delivery_charge='120';
$final_bill=$bill+$gst-$delivery_charge;


$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetY(75);
$pdf->SetFont("Arial","",15);
$time=date("y/m/d h:i:s");
// Title
$pdf->SetFont("helvetica","",10,'');
    





// the header par of pdf =======================================================================================================
$pdf->SetTextColor(199,0,0);


$pdf->SetFillColor(0,233,255);

$pdf->Image('logo2.png',10,6,30);
$pdf->Ln(20);
$pdf->SetFont("Arial","",15);
$pdf->SetY(5);
$pdf->SetX(122);
$pdf->Cell(0,2,$setup->company,"0","1",C);
$pdf->Ln(4);
$pdf->SetX(122);
   
$pdf->Ln(2.8);
$pdf->SetY(85);
$pdf->SetX(122);
$pdf->SetFont("Arial","B",8);
$pdf->SetFillColor(255,255,255);
$pdf->SetTextColor(0,0,0);

// left side details ===============================
    $pdf->Ln(4);
        $pdf->Ln(4);$pdf->SetX(-322);
$pdf->SetFont("Arial","",17);


    $pdf->Ln(4);
      $pdf->Cell(0,2,"","",11);
$pdf->SetFont("Arial","B",11);
    $pdf->Cell(0,2,"Your billing details :","",11);  $pdf->Ln(4);  $pdf->Ln(4);


    
  $pdf->SetFont("Arial","",9);
  $pdf->Cell(0,2,"{$customername}","0","1");
  $pdf->Ln(4);
  $pdf->Cell(0,2,"{$address}","0","1");
  $pdf->Ln(4);
  $pdf->Cell(0,2,"{$email}","0","1");
  $pdf->Ln(4);
  $pdf->Cell(0,2,"{$phone}","0","1");
 
// left side detailsends here ================================

  // right side detailos starts here ========================================================
$pdf->SetY(35);
      $pdf->SetX(0);              $pdf->Ln(4);
              $pdf->Cell(0,2,"","0","1",C);
              $pdf->SetFont("Arial","B",14);
        $pdf->Cell(0,2,"Invoice number: IN{$orderid}","0","1",C);
              $pdf->Ln(4);
$pdf->Cell(0,2,"Date :{$time}","0","1",C);
$pdf->Ln(4);

  // right side details will ends here =========================================================================
$pdf->SetFont("Arial","",10);
$pdf->SetY(135);



  //$pdf->SetX(-52);

// customizing the string for detailed products invoicing 
// ====== detailed product invoicing 


$pdf->SetFont("helvetica","B",9);


$pdf->SetFont("helvetica","B",9);
$pdf->Cell(145,10," Project Alloted :  {$stringitems}","0","1");

      $pdf->Ln(19);

      $pdf->Cell(145,10," PAYABLE AMOUNT:   INR {$bill}","0","1");

      $pdf->Ln(1);

      $pdf->Cell(145,10," GST @ 12% {$setup->gst} % : INR {$gst}","0","1");

      $pdf->Ln(1);

      $pdf->Cell(145,10," servic charge (deducted):   INR {$delivery_charge}","0","1");

      $pdf->Ln(4);
            $pdf->Cell(145,10,"______________________________________","0","1");
$pdf->SetTextColor(230,0,0);

      $pdf->Cell(145,10," TOTAL :   INR {$final_bill}","0","1");
$pdf->SetTextColor(0,0,0);
      $pdf->Cell(145,10,"This invoice can be used as a reciept .
       ","0","1");  $pdf->Ln(0.5);
       $pdf->Cell(145,10,"Please refer this Invoice in case any payment settlement issue arrises","0","1");
            $pdf->Cell(145,10,"You can track your payment status on {$domain}/invoice={$orderid}","0","1");
             $pdf->Cell(145,10,"Payment will be transferred to the users bank account after verification","0","1");
// =========================== new pdf examples ================================================= 




//============================= new pdf examples ends hre



//$pdf->output();
$invoicename='invoices/IN'.$orderid.'.pdf';
$content = $pdf->Output($invoicename,'F');




}

?>
