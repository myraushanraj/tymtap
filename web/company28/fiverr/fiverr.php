<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
	<header>
		<style>
.avatar {
    vertical-align: middle;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    margin-top: 10px;
}
.avatar1 {
    vertical-align: middle;
    width: 120px;
    height: 120px;
    border-radius: 50%;
    margin-top: 10px;
    margin-left: 100px;
}
.checked {
    color: orange;
}
</style>
	</header>

<div class="container" style="background-color: #f7f7f7; margin-top: 30px;">
	<div class="row">
		
		<div class="col-md-1">
			

		</div>
				<div class="col-md-4" style="background-color: white; border: 1px solid grey;">
					<img src="image2.jpg" alt="Avatar" class="avatar1">
					<br>
					<br>
					<h4 style="margin-left: 100px;">Chandan Jha</h4>
					
					<span>Business development, e commerce and visual arts are my passion!</span>
					<br>

					<span style="margin-left: 60px;">
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<b>5.0</b>(14 Reviews)
					</span>

					<br>
					<br>
					<input type="submit" class="btn btn-success" value="Contact Me">
					<input type="submit" class="btn" value="Custome Order">
					<br>
					<br>
					<i class="material-icons">location_on</i><span style="color: grey;">From</span>
					<span style="float: right;"><b>India</b></span>
					<br>
					<i class="material-icons">person</i><span style="color: grey;">Member Since</span>
					<span style="float: right;"><b>Feb 2016</b></span>
					<br>
					<i class="material-icons">access_time</i><span style="color: grey;">Avg Responce Time</span>
					<span style="float: right;"><b>1 Hour</b></span>
					<br>
					<i class="material-icons">arrow_forward</i><span style="color: grey;">Recent delivery</span>
					<span style="float: right;"><b>9 days</b></span>


			

		</div>
				<div class="col-md-1">
			

		</div>
				<div class="col-md-6" style="background-color: white; border: 1px solid grey;">
			<h4 style="margin-top: 17px;">Description</h4>

			<span>To briefly introduce myself - I have been working more than 5 years as a chief editor in a fashion and modelling agency. Currently I work as a freelance photographer, editor and consultant in media agencies, advising public relations, content creation, marketing and business development teams. I have been working more than 5 years as a chief editor in a fashion and modelling agency. Currently I work as a freelance photographer, editor and consultant in media agencies, advising public relations, content creation, marketing and business development teams.</span>
			<br><br>
			<h4>Languages</h4>
			English -<span style="color: grey;"> Native/Bilingual<br></span>
			Polish <b>(Polski)</b> -<span style="color: grey;"> Native/Bilingual</span>
			<br>
			<br>
			<h4>Skills</h4>
			<span>Photography, Photoshop, EditingGraphic, DesignPhoto Editing</span>

		</div>
	</div>

	<div class="row" style="margin-top: 50px;">
	<div class="col-md-1">
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey;">
		
<h4>Chandan's Reviews</h4>
<span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<b>5.0</b>(14 Reviews)
					</span>

	</div>
</div>

	<div class="row" style="">
	
	<div class="col-md-1">
			

		</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 80px;">

<div class="row">
	<div class="col-md-1">
	
</div>
<div class="col-md-3" style="margin-top: 20px;">
	Seller Communication
	<br>
	<span style="margin-left: 25px;">
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	</span>
	
</div>
	<div class="col-md-1">
	
</div>
<div class="col-md-3" style="margin-top: 20px;">
	Service as Described
	<br>
	<span style="margin-left: 25px;">
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	</span>
</div>
<div class="col-md-1">
	
</div>
<div class="col-md-3" style="margin-top: 20px;">
	Would Recommend
	<br>
	<span style="margin-left: 25px;">
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
	<span class="fa fa-star checked"></span>
</span>
</div>	

</div>
	</div>

	</div>
	<div class="row">
		<div class="col-md-1">
	
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
	<img src="image3.jpg" alt="Avatar" class="avatar">	
Louiemolina <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					
<br>
It was an amazing experience working with Jim. He kept me up to date on everything. He's very talented.
<br>
<span style="font-size: 12px;">9 days ago</span>
	</div>
</div>
		<div class="row">
	<div class="col-md-1">
	
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
		<img src="image2.jpg" alt="Avatar" class="avatar">
Mantangs <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					
<br>

It was really hard to find good footage for my case. However, once I got one, Jim did the great work. The video was really great quality.
<br>
<span style="font-size: 12px;">22 days ago</span>
	</div>

</div>
<div class="row">
	<div class="col-md-1">
	
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
		<img src="image1.jpg" alt="Avatar" class="avatar">
Markusfrench <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					
<br>
Fast and good and has an eye for making videos.
<br>
<span style="font-size: 12px;">23 days ago</span>

	</div>

</div>

<div class="row">
	<div class="col-md-1">
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 130px;">
		<img src="image3.jpg" alt="Avatar" class="avatar">

Kaveh96 <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					
<br>
Great work, thanks
<br>
<span style="font-size: 12px;">26 days ago</span>
	</div>
</div>
<div class="row">
	<div class="col-md-1">
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 170px;">
		<img src="image2.jpg" alt="Avatar" class="avatar">
Nerdcultcha <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked "></span>
					<span class="fa fa-star "></span>
					
<br>
Overall I am delighted with the work produced by Jim, and working with him has been a joy. He responded to messages promptly and has a great approach to dealing with any queries. His passion is obvious and I can’t recommend him enough. If I have any other projects lined up in the future needing this sort of thing, I will be straight back here!
<br>
<span style="font-size: 12px;">27 days ago</span>
	</div>
</div>

<div class="row">
	<div class="col-md-1">
</div>
	<div class="col-md-11" style="background-color: white; border: 1px solid grey; height: 150px;">
<img src="image1.jpg" alt="Avatar" class="avatar">
Perry5r <span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star checked"></span>
					<span class="fa fa-star"></span>
					<span class="fa fa-star"></span>
					
<br>
John is a pleasure to work with and produces a superb result. His standards are extremely high and he clearly put great thought and talent into the project.
<br>
<span style="font-size: 12px;">about 1 month ago</span>
	</div>
</div>



</div>


</body>

</html>