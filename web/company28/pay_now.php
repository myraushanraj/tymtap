<?php
require_once "class/company-class.php";
$c1=new company($_SESSION['company_id']);
$testing="Wallet Recharge";
$buyer_name=$c1->title;
$phone='8587024270';
$amount=$_GET['amount'];
$email=$c1->email;
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://www.instamojo.com/api/1.1/payment-requests/');
curl_setopt($ch, CURLOPT_HEADER, FALSE);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
curl_setopt($ch, CURLOPT_HTTPHEADER,
            array("X-Api-Key:f13f9c4d8d19eae466afb0bebd85d71d",
                  "X-Auth-Token:31d35d037a5128b1a1ee2ace5f5a6406"));
$payload = Array(
    'purpose' => $testing,
    'amount' => $amount,
    'phone' => $phone,
    'buyer_name' =>$buyer_name,
    'redirect_url' => 'http://www.monkhub.com/success.php',
    'send_email' => true,
    'webhook' => 'http://www.example.com/webhook/',
    'send_sms' => false,
    'email' => 'chandanjha.7@gmail.com',
    'allow_repeated_payments' => false
);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
$response = curl_exec($ch);
curl_close($ch); 

echo $response;
// now redirect the page to payment gateway 

$json = json_decode($response);

$forward_url=$json->payment_request->longurl;
?>
<script>
    window.location="<?php echo $forward_url; ?>";
</script>