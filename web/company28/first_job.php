<?php
include_once('inc/header.php');include_once 'class/thread-class.php'; 
?>
<div class="menu">
 <div class="container">
  <ul class="nav nav-tabs">
    <li class="active"><a href="job-post.php">Create Job</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#"> My Projects <span class="caret"></span></a>
      <ul class="dropdown-menu">
        <li><a href="#">Submenu 1-1</a></li>
        <li><a href="#">Submenu 1-2</a></li>
        <li><a href="#">Submenu 1-3</a></li>                        
      </ul>
    </li>
    <li><a data-toggle="tab" href="#menu1">Payments</a></li>
    <li><a data-toggle="tab" href="#jobs_posted">Jobs Posted</a></li>
    <li><a data-toggle="tab" href="#requests">Requests</a></li>
    <li><a data-toggle="tab" href="#on_going_job">On-Going Job</a></li>
     </ul>
 </div>
</div>
<div class="container">
  <div class="tab-content bottom_margin">

    <div id="menu1" class="tab-pane fade">
     <div class="col-md-9">

     	<div class="wrap_payment top_margin">
     		<h5><img src="user/pp.png" class="img_round"><span>&nbsp; Rahul Mittal</span><span class="paid">Paid</span></h5>
     		<h5 class="theme_color">Freelancer for Amezon Marketing Event</h5>
     		<table>
     			<tr><td>Date of Event </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </td><td class="theme_color"> 01/01/2018</td></tr>
     			<tr><td>Check -In Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 04:01 pm(Delay by 1 minute)</td></tr>
     			<tr><td>Check Out Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 08:01 pm</td></tr>
     			<tr><td>Amount </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Rs 3000</td></tr>
     			<tr><td>Location </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Pragati Maidan</td></tr>
     		</table>
     		<div style="position: relative;">
          <h5 class="generate"><span class="paid">Generate Invoice</span></h5>
          </div>
     		
     	</div>
     </div>
     <div class="col-md-3 top_margin">
     	<br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border">Filter</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;All</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Paid</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Payment</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Date</li>
     	    </div>
     	
     </div>
    </div>



    <div id="jobs_posted" class="tab-pane fade">
      
        <div class="wrap_payment top_margin">
     		
  <?php 
        $all_thread=companies_thread('CMP123');
        for($k=0;$k<sizeof($all_thread);$k++)
        {                
                 $thread=new thread($all_thread[$k]['thread_id']);
                 echo $thread->company_thread_box();
        } ?>
     	</div>

    </div>



    <div id="on_going_job" class="tab-pane fade">
  

     <div class="col-md-8">

     	<div class="wrap_payment top_margin">
     		<h5><img src="user/pp.png" class="img_round"><span>&nbsp; Rahul Mittal<br><span class="location">New Delhi</span></span><span class="paid">Check-In</span></h5>
     		<h5 class="theme_color">Freelancer for Amezon Marketing Event</h5>
     		<table>
     			<tr><td>Date of Event </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp; </td><td class="theme_color"> 01/01/2018</td></tr>
     			<tr><td>Check -In Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 04:01 pm(Delay by 1 minute)</td></tr>
     			<tr><td>Check Out Time </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> 08:01 pm</td></tr>
     			<tr><td>Amount </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Rs 3000</td></tr>
     			<tr><td>Location </td><td>&nbsp;&nbsp;&nbsp; : &nbsp;&nbsp;&nbsp;</td><td class="theme_color"> Pragati Maidan</td></tr>
     		</table>
     		<div style="position: relative;">
          <h5 class="generate"><span class="chat">Chat Now</span></h5>
          </div>
     		
     	</div>
     </div>
     <div class="col-md-4 top_margin">
     	<br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border theme_color">Filter</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;All</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Paid</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Payment</li>
     	            <li><input type="radio" name="nn">&nbsp;&nbsp;Date</li>
     	    </div>
     	    <br><br>
     	    <div class="wrap_filter">
     	            <li class="center bottom_border theme_color
     	            ">Announcements</li>
     	            <li>The meeting will be held after the event at 07:00 p.m. in the main shop</li>
     	            <li>Please collect your badges before starting the job</li>
     	            <br><br><br><br>
     	            <li style="background: #cecece;"><input type="text" style="width: 80%" style="border: none;" name=""><span>&nbsp; Send</span></li>
     	            
     	    </div>
     	
     </div>
    </div>

    <div id="requests" class="tab-pane fade">
      
        <div class="wrap_payment top_margin">
             <span class="fa fa-star checked"></span>
             <span class="fa fa-star checked"></span>
             <span class="fa fa-star checked"></span>
             <span class="fa fa-star"></span>
             <span class="fa fa-star"></span>
             <h5><img src="user/pp.png" class="img_round"><span>&nbsp; Rahul Mittal<br><span class="location">New Delhi</span></span></h5>
     		<p>I have completed b-tech in computer science. I am full stack developer. I have completed 10 projects in php and codeigniter.</p>
     		<h4 class="center"><span class="chat_r">Chat Now</span><span class="accept_r">Accept</span><span class="reject_r">Reject</span></h4>


     		
     		
     		
     	</div>

    </div>



  </div>
</div>

</body>
</html>

