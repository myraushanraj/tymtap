<style>
    .chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}


#new_chat_count{
      border-radius: 50%;
    background: green;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    float: right;

    margin-right: 2px;
}

#typing-notify{
      background: #fdfdff;
    padding-left: 17%;
    height: 26px;
    width: 100%;
}
.online-icon{
    background: #4e9e33;
    width:11px;
    height:11px;display: inline-block;
    border-radius: 50%;
}

h6.company-title{
      font-size: 15px;
    font-weight: bold;
}


::-webkit-scrollbar {
    width:10px;
}
::-webkit-scrollbar-track {
    background-color: #eaeaea;
    border-left: 1px solid #ccc;
}
::-webkit-scrollbar-thumb {
    background-color: #ccc;
}
::-webkit-scrollbar-thumb:hover {
    background-color:grey;
}
.chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

.chat-time {
    color: #1a76d2;
    float: right;
    margin-right: 5%;
}
.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}
#msg-close-btn{
    margin-right: 20px;
    color: #c2c2c3;
    font-family: cursive;
    margin-top: 9px;
    position: absolute;
    z-index: 2222222222;
    margin-left: 91%;
}
.chat-content {
    height: auto;
    line-height: 14px;
    margin-right: 3%;
    float: left;
    /* position: absolute; */
    margin-left: 21%;
    background: #e6e6e6;
    padding: 3%;
}

#new_chat_count {
    border-radius: 50%;
    background: #ffb748;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    float: right;
    margin-right: 2px;

}
.chat-logo {
    width: 56px;
    height: 56px;
    border: 2px solid #9a9fa4;
    border-radius: 50%;
}
</style>
<div id="messenger-chat" style="display:none">
<a href="javascript:void(0)" onclick="this.style.display='none';close_messenger()" id="msg-close-btn">X</a>
<iframe src="" id="msg_id"></iframe>
</div>
<script>
function open_messenger(member){
  document.getElementById('msg_id').src="my-chat-small.php?member="+member;
  $("#messenger-chat").show();
    $("#msg-close-btn").show();
}
function close_messenger(){
  $("#messenger-chat").hide();
}
</script>