<?php
include_once "../class/db-connection.php";
$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
$max_file_size = 280 * 1024; #200kb
$nw = $nh = 200; # image with # height

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	if ( isset($_FILES['image']) ) {
		if (! $_FILES['image']['error'] && $_FILES['image']['size'] < $max_file_size) {
			$ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
			if (in_array($ext, $valid_exts)) {
				   $picname=uniqid();
					$path = '../../img-profile/'.$picname.'.' . $ext;
					$size = getimagesize($_FILES['image']['tmp_name']);

					$x = (int) $_POST['x'];
					$y = (int) $_POST['y'];
					$w = (int) $_POST['w'] ? $_POST['w'] : $size[0];
					$h = (int) $_POST['h'] ? $_POST['h'] : $size[1];

					$data = file_get_contents($_FILES['image']['tmp_name']);
					$vImg = imagecreatefromstring($data);
					$dstImg = imagecreatetruecolor($nw, $nh);
					imagecopyresampled($dstImg, $vImg, 0, 0, $x, $y, $nw, $nh, $w, $h);
					imagejpeg($dstImg, $path);
					imagedestroy($dstImg);
					

//updating the succefully uploaded pic to the database 
					$picpath=$picname.'.' . $ext;
				
						
$obj1=new dbconn();
$con=$obj1->connect();
	$customer_code=$_SESSION['member_id'];
         $sql="update organisation_team set profile_pic='$picpath' where member_id='$customer_code'";
         $con->query($sql);
?>
<script>
window.location='../user-dashboard.php'
</script>
<?php		
				} else {
					?>
					<script>
					alert('unknown error ! upload again');
					window.location='index.php';
				    </script>
                 <?php
             } 
		     }else
		     {
			

						?>
											<script>
													alert('File limit should be 500KB');

										window.location='index.php';
											</script>


								<?php


		}
	} else {
				?>
											<script>
													alert('File name not set');

										window.location='index.php';
											</script>


								<?php
	}
} else {
			?>
											<script>
													alert('Bad request !');

										window.location='index.php';
											</script>


								<?php
}

?>