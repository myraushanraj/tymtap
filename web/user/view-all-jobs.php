<?php 
require_once "class/db-connection.php";
require_once "class/thread-class.php";
if(isset($_SESSION['company_id']))
{

$company_id=$_SESSION['company_id'];
$job_arr=array();
$db=new dbconn();
$con=$db->connect();
$cmd="SELECT * FROM job_process where owner_id='$company_id'";
	$result=$con->query($cmd);
    // output data of each row
    while($row = $result->fetch()) {
     array_push($job_arr,$row);
    }

}else
{
	?>
<script>
	window.location="company-login.php";
</script>
	<?php 
}

if(isset($_POST['submit']))
{
$thread_id=$_POST['threads'];

$db=new dbconn();
$con=$db->connect();
$sql = "SELECT * FROM job_application WHERE thread_id='$thread_id' ";
 $result=$con->query($sql);
 $applicant_arr=array();
 while($row = $result->fetch()) {
     array_push($applicant_arr,$row);
    }
}
?>
<?php

if(isset($_GET['accept']))
{
$request_id=$_GET['ap'];
// echo "thread id ".$thread_id;

$db=new dbconn();
$con=$db->connect();

$cmd2="UPDATE job_application SET job_status ='Accepted' WHERE request_id = '$request_id' ";
$query=$con->prepare($cmd2);
$query->execute();
}

if(isset($_GET['reject']))
{
$request_id=$_GET['ap'];
// echo "thread id ".$thread_id;

$db=new dbconn();
$con=$db->connect();

$cmd2="UPDATE job_application SET job_status ='Rejected' WHERE request_id = '$request_id' ";
$query=$con->prepare($cmd2);
$query->execute();
}

?>
<!DOCTYPE html>
<html>
<head>
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Admin Panel</title>
	<style>
		table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    
}
th, td {
    padding: 5px;
    text-align: left;
}

	</style>
</head>
<body>
<h1>Job posted (<?php echo sizeof($job_arr);?>)</h1>
<!--- === Fetch all jobs ====================== -->
<div class="container">


<table style="margin-left: 17px;">
	<tr>
<TH>Thread ID</TH><TH>Job title</TH><th>Application</th><th>Job status</th><th>ACTION</th>
	</tr>
<tr>
	<?php for ($i=0; $i < sizeof($job_arr); $i++) { 
		
		$job=new thread($job_arr[$i]['thread_id']);
	?>
    <tr>
		<th><?php echo $job->id;?></th>
		<TH><?php echo $job->title?></TH>
		<TH><?php echo $job->description?></TH>
		<TH>
			<ul>
				<li>Attended : </li></ul><?php echo $job->apply_count?></li>
			    <li>Accepted : </li></ul><?php echo $job->accept_count?></li>
			    <li>Rejected : </li></ul><?php echo $job->reject_count?></li>

			</ul>



		</TH>
		<th> 
			<a href="accept-reject.php?id=<?php echo $job->id; ?>" class="btn btn-primary">View Application</a>

		</th>
		</tr>
	<?php } ?>



</table>
</div>

</body>
</html>