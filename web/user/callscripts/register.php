<?php require_once "../class/db-connection.php";
require_once "../class/notification-class.php";
if(isset($_POST['reg_user']))
{
$name = $_POST['name'];
$email  = $_POST['email'] ;
$password = $_POST['password_1'];
$cpassword=$_POST['password_2'];
$phone=$_POST['phone']; 
$member_id=rand(1234,9999);



$db=new dbconn();
$con=$db->connect();
$cmd="INSERT INTO organisation_team(name,email,password,phone,member_id) values (:name1,:email1,:password1,:phone1,'$member_id')";
$query=$con->prepare($cmd);
$query->execute(array(":email1"=>$email,":password1"=>$password,":phone1"=>$phone,":name1"=>$name));



$admin=new notification('admin');
$link="";
$from=$member_id;
$notification="New Member joined : ".$name;
$admin->push_notification($notification,$from,$link);
/*------- push notification ========================= */
}




  ?>
<!DOCTYPE html>
<html>
<head>
  <title>Registration system</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="header">
  	<h2>Register</h2>
  </div>
	
  <form method="post" action="register.php">

  	<div class="input-group">
  	  <label>Name</label>
  	  <input type="text" name="name">
  	</div>
  	<div class="input-group">
  	  <label>Email</label>
  	  <input type="text" name="email">
  	</div>
        <div class="input-group">
      <label>Phone</label>
      <input type="text" name="phone">
    </div>
  	<div class="input-group">
  	  <label>Password</label>
  	  <input type="password" name="password_1">
  	</div>
  	<div class="input-group">
  	  <label>Confirm password</label>
  	  <input type="password" name="password_2">
  	</div>
  	<div class="input-group">
  	  <button type="submit" class="btn" name="reg_user">Register</button>
  	</div>
  	<p>
  		Already a member? <a href="login.php">Sign in</a>
  	</p>
  </form>
</body>
</html>