<?php 
require_once "class/db-connection.php";
require_once "class/member-class.php";
// print_r($_SESSION);
if(!isset($_SESSION['member_id']))
{
  // echo 'not set';
  header('location:login.php');
}
 if(isset($_GET['logout'])) {
  $db = new dbconn();
$con = $db->connect();
   $job_id=$_SESSION['member_id'];
    unset($_SESSION['username']);
     unset($_SESSION['member_id']);
      unset($_SESSION['email']);
          $sql = "UPDATE organisation_team SET is_online='0' WHERE member_id='$job_id'";
    $result=$con->query($sql);
    header("location: login.php");
  }
// print_r($member);
if (isset($_POST['edit_change'])) {
  $member=new member($_SESSION['member_id']);
$name = $_POST['name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$bank_name = $_POST['bank_name'];
$bank_location = $_POST['bank_location'];
$IFSC_code = $_POST['IFSC_code'];
$bank_account = $_POST['bank_account'];
$db=new dbconn();
$con=$db->connect();
$cmd1="UPDATE organisation_team SET name = :name1 ,email = :email1, phone = :phone1 where member_id = '$member->id'";
echo $cmd1;
$query1=$con->prepare($cmd1);
$query1->execute(array(":email1"=>$email,":name1"=>$name,":phone1"=>$phone));
$cmd2="UPDATE bank_process SET bank_name = :bank_name1 , bank_location = :bank_location1, bank_account = :bank_account1,  IFSC_code = :IFSC_code1 where member_id = $member->id";
$query2=$con->prepare($cmd2);
$query2->execute(array(":bank_name1"=>$bank_name,":bank_location1"=>$bank_location,":bank_account1"=>$bank_account,":IFSC_code1"=>$IFSC_code));
  }
$member=new member($_SESSION['member_id']);
  ?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
</head>
<body style="    background: url(../img/background.png);
    width: 100%;
    height: 100%;">
<div class="container gap-top theme-background text-center" >
<div class="col-md-4 sidenav">

      <ul class="nav nav-pills nav-stacked">
        <li class="active">
              <?php  if (isset($_SESSION['member_id'])) : ?>
      <p>Welcome <strong><?php echo $_SESSION['name']; ?></strong>

      </p>

    <?php endif ?>
        </a></li>
        <li><a href="user-dashboard.php">Dashboard</a></li>
        <li><a href="update-profile.php">Payments</a></li>
        <li><a href="update-profile.php">My Profile</a></li>
        <li><a href="account-settings.php">Account Settings</a></li>
        <li><a href="index.php?logout='1'" style="color: red;">logout</a> </li>
      </ul><br>
</div>
<div class="col-md-8">
      <!-- logged in user information -->

 <form method="post" class='trans-form' action="#">

    <div class="input-group">
      <label>Name</label>
      <input type="text" name="name" value="<?php echo $member->name?>">
    </div>
    <div class="input-group">
      <label>Email</label>
      <input type="text" name="email" value="<?php echo $member->email?>">
    </div>
        <div class="input-group">
      <label>Phone</label>
      <input type="text" name="phone" value="<?php echo $member->phone?>">
    </div>
    <div class="input-group">
      <label>Bank Name</label>
      <input type="text" name="bank_name" value="<?php echo $member->bank?>">
    </div>
    <div class="input-group">
      <label>Bank Location</label>
      <input type="text" name="bank_location" value="<?php echo $member->location?>" >
    </div>
    <div class="input-group">
      <label>Account Number</label>
      <input type="text" name="bank_account" value="<?php echo $member->account_number?>" >
    </div>
    <div class="input-group">
      <label>IFSC Code</label>
      <input type="text" name="IFSC_code" value="<?php echo $member->ifsc?>">
    </div>
    <div class="input-group" style="text-align: center;">
      <button type="submit" class="btn" name="edit_change" >Edit and Save</button>
    </div>
  </form>

</div>    

</div>
</body>
</html>