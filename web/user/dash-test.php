<?php 
include_once 'class/thread-class.php';
include_once 'class/misc-class.php';
include_once 'class/company-class.php';
include_once 'class/message-data-class.php';
if(isset($_GET['apply']))
{

  $thread=new thread($_GET['apply']);
  $thread->apply_job();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body style="">
<?php  include_once 'inc/header.php';include_once 'inc/admin_pops.php';?>
<div class="container">
  <h2>My Dashboard (12)</h2>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">FEED</a></li>
    <li><a data-toggle="tab" href="#menu1">MESSAGE</a></li>
    <li><a data-toggle="tab" href="#menu2">REQUEST</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Feed</h3>
<!-- ===  feed comes here ========================================== -->
<div class="container theme-background" style="height: 50px;color: white;padding-top: 12px">Upcoming Jobs (5)
</div>
        <?php 
        $all_thread=all_thread();
        for($k=0;$k<sizeof($all_thread);$k++)
        {                
                 $thread=new thread($all_thread[$k]['thread_id']);
                 echo $thread->thread_box();
        } ?>
 </div>
<!-- ==== ----  feed endshere ===================================== --> 
    <div id="menu1" class="tab-pane fade">
<!-- ===  message class starts ================================= -->
   <!-- ==== inbox messages chart ================================================================================ -->
               <?php $r1=new message_records($_SESSION['member_id']);
               $thread_arr=$r1->fetch_all_msg_thread();
               ?>
                <table class="table" id='inbox_message'>
                    <tbody>
                      <?php for($k=0;$k<sizeof($thread_arr);$k++){
                        $comp=$thread_arr[$k]['triggered_from'];
                        $last_msg_id=$r1->last_message_id($thread_arr[$k]['triggered_from']);
                      if(!empty($last_msg_id))
                      {
                        $ms=new message_flow($last_msg_id);
                        $m1=new misc();
                        $member1=new company($ms->other_person_involed);
                      ?>
                     <tr>
<td>&nbsp;</td>
                        <td>
<a href="my-chat.php?comp=<?php echo $member1->id?>" target='_blank' data-toggle='modal' onclick="open_messenger('<?php echo $comp;?>')"  class="pull-right">Massenger Window</a><?php echo $ms->timespan?> 
                         </td>
                      </tr>
                        <tr class="table-row">
                            <td class="table-img">
                               <img src="<?php echo get_pic($member1->pic);?>" width='45' alt="">
                            </td>
                            <td class="table-text">
                              <h6 class="company-title"><?php echo $member1->title;if($member1->is_online)
                                echo "<div class='online-icon'></div>";
                                ?>
                              </h6>
                               <img src="../img-company/<?php echo $member1->pic;?>" width='80'>
                                <p class="desc"><?php echo $ms->content?>
                                  <br>
                                   <a href="javascript:void(0)" onclick="open_messenger('<?php echo $comp;?>')" class="btn btn-primary btn-chat">Message </a>
                                </p>
                            </td>

                             <td>
               <i class="fa fa-star-half-o icon-state-warning"></i>
                            </td>
                        </tr>
                        <?php } } ?>
                       
                    </tbody>
                </table>


<!-- ===  message ends here ====================================== -->
    </div>
    <!--- rEQUESTED JOBS COMES HERE ======================== -->
    <div id="menu2" class="tab-pane fade">
      <h5 class="large-italic theme1">APPLIED (<?php  $my_thread=$thread->my_applied_jobs();
      echo sizeof($my_thread); ?>)</h5>
         <?php 
        if(!sizeof($my_thread))
          echo "<span class='large-italic theme1'>You have not applied to any of the jobs ! Have a pleasant stay at Time tap </span>";
        for($k=0;$k<sizeof($my_thread);$k++)
      {                
                 $thread=new thread($my_thread[$k]['thread_id']);
                 echo $thread->thread_box();
       } ?>
    </div>
    <!-- ====  REQUESTED JOBS ENDS HERE =================== -->
  </div>



<div id="messenger-chat" >
<a href="javascript:void(0)" onclick="this.style.display='none';close_messenger()" id="msg-close-btn">X</a>
<iframe src="" id="msg_id"></iframe>
</div>

</div>
<script>  
function open_messenger(comp){

  document.getElementById('msg_id').src="my-chat-small.php?comp="+comp;
  $("#messenger-chat").show();
    $("#msg-close-btn").show();

}
function close_messenger(){
  $("#messenger-chat").hide();

}
</script>
</body>
</html>
