<?php 
include_once 'class/thread-class.php';
include_once 'class/misc-class.php';
include_once 'class/company-class.php';
include_once 'class/message-data-class.php';
if(isset($_GET['comp']))
{
     $comp=$_GET['comp'];
     $company=new company($comp);
}
if(isset($_GET['apply']))
{
  $thread=new thread($_GET['apply']);
  $thread->apply_job();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="" >
<?php  include_once 'inc/header.php';include_once 'inc/admin_pops.php';
  $r1=new message_records($_SESSION['member_id']);
  $message_arr=$r1->company_conversation($company->id);
?>
<input type="hidden" id="chat_count_old" value='<?php echo sizeof($message_arr); ?>'>
<div class="agile-grids" id='chat_container' style="max-width: 700px">
</div>
<!-- ===  chat bar ================================ -->
<div class="row fixed-bottom-chat">
<div id="typing-notify">Raushan is typing..</div>
<textarea class="chat-textbar" id="text_id"></textarea>
<a href="javascript:void(0)" onclick="chat_ajax()" class="pull-right post-btn">POST</a>
<div id="new_msg">
<div id="new_chat_count">0
</div>
<a href="#chat_footer"><img src="../icon/down1.png" width="23"></a>
</div>
</div>
<!--- chat bar ends here =========================== -->
<div id="chat_footer"></div>
<script>
function chat_ajax(){
  var chat1=$('#text_id').val();
  if(chat1!='')
  {
var add_chat="<?php echo $_GET['comp'];?>";
//alert(chat1+group_id);
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
document.getElementById("text_id").value='';
chatRefresh('<?php echo $company->id?>');
 // Scroll 100px to the right
    }
  }
xmlhttp.open("GET","callscripts/send_message.php?add_chat="+add_chat+"&message="+chat1,true);
xmlhttp.send();
  }
}
function chatRefresh(comp){
  var chat_count_old=$("#chat_count_old").val();
  var chat_count_new=$("#chat_count_new").val();
  var new_chat_count=(+chat_count_new-chat_count_old);
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById('chat_container').innerHTML=xmlhttp.responseText;
    window.scrollBy(0,200);

    if(new_chat_count>0)
    {
           $('#new_msg').show();
      document.getElementById('new_chat_count').innerHTML=new_chat_count;
    }else{
        $('#new_msg').hide();
    }
    }
  }
xmlhttp.open("GET",'callscripts/chat-loader-refresh.php?comp='+comp,true);
xmlhttp.send();
}
function is_typing(comp){
var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
if(xmlhttp.responseText=='1')
document.getElementById("typing-notify").style.display='block';
 else
  document.getElementById("typing-notify").style.display='none';
 // Scroll 100px to the right
    }
  }
xmlhttp.open("GET","callscripts/other-side-typing.php?comp="+comp,true);
xmlhttp.send();
  }
    $(document).ready(function(){
      refreshchat();
    });

    function refreshchat(){
chatRefresh('<?php echo $company->id?>');
is_typing('<?php echo $company->id?>');
   setTimeout(refreshchat,8200);
   }

  $(window).scroll(function() {
   if($(window).scrollTop() + $(window).height() == $(document).height()) {
// logic for user has voewed the ,essage 
        $('#new_msg').hide();
  document.getElementById('chat_count_old').value=$("#chat_count_new").val();
   }
});
</script>
   </body>
   </html>