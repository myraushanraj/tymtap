<?php 
require_once "class/db-connection.php";
require_once "class/member-class.php";
// print_r($_SESSION);
if(!isset($_SESSION['member_id']))
{
  // echo 'not set';
  header('location:login.php');
}
 if(isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
// print_r($member);
if (isset($_POST['edit_change'])) {
  $member=new member($_SESSION['member_id']);
$bank_name = $_POST['bank_name'];
$bank_location = $_POST['bank_location'];
$IFSC_code = $_POST['IFSC_code'];
$bank_account = $_POST['bank_account'];
$db=new dbconn();
$con=$db->connect();

$cmd2="UPDATE bank_process SET bank_name = :bank_name1 , bank_location = :bank_location1, bank_account = :bank_account1,  IFSC_code = :IFSC_code1 where member_id = $member->id";
$query2=$con->prepare($cmd2);
$query2->execute(array(":bank_name1"=>$bank_name,":bank_location1"=>$bank_location,":bank_account1"=>$bank_account,":IFSC_code1"=>$IFSC_code));
  }
$member=new member($_SESSION['member_id']);
  ?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="style.css">
  <style type="text/css">
    label{color: black !important;}
  </style>
</head>
</head>
<body>
<?php include_once "inc/header.php";include_once "inc/side-bar.php";?>
<div class="col-md-8">


      <!-- logged in user information -->

 <form method="post" class='trans-form' action="#">

    <div class="input-group">
      <label>Bank Name</label>
      <input type="text" name="bank_name" value="<?php echo $member->bank?>">
    </div>
    <div class="input-group">
      <label>Bank Location</label>
      <input type="text" name="bank_location" value="<?php echo $member->location?>" >
    </div>
    <div class="input-group">
      <label>Account Number</label>
      <input type="text" name="bank_account" value="<?php echo $member->account_number?>" >
    </div>
    <div class="input-group">
      <label>IFSC Code</label>
      <input type="text" name="IFSC_code" value="<?php echo $member->ifsc?>">
    </div>
    <div class="input-group" style="text-align: center;">
      <button type="submit" class="btn" name="edit_change" >Edit and Save</button>
    </div>
  </form>

</div>    
</body>
</html>