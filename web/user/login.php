<?php // LOGIN USER
require_once "class/db-connection.php";
require_once "class/misc-class.php";
if (isset($_POST['login_user'])) {
  $email = $_POST['email'];
  $password = $_POST['password'];
  // $username = mysqli_real_escape_string($db, $_POST['username']);
  // $password = mysqli_real_escape_string($db, $_POST['password']);
login_now($email,$password);
     }
 ?>
<!DOCTYPE html>
<html>
<head>
  <title>Registration system</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body style="    background: #0d0000;">

<center>
  <img src="../icon/logo2.png" style="width:200px">
</center>
<h4 style="color:grey;margin-left:33%">Freelancer Sign In</h4>
  <form method="post" action="login.php" style="    background: none;
    border: none;
    width: 482px;">
  	
  	<div class="input-group">
  		<label>Email</label>
  		<input type="text" name="email" required  class="black-input">
  	</div>
  	<div class="input-group">
  		<label>Password</label>
  		<input type="password" name="password" required class="black-input">
  	</div>
  	<div class="input-group">
  		<button type="submit" class="btn" name="login_user" class="login-btn">Login</button>
  	</div>
  	<p style="color:grey">
  		Not yet a member? <a href="register.php" style="    color: #25a2a2;">Sign up</a>
  	</p>
  </form>
</body>
</html>