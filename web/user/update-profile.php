<?php 
require_once "class/db-connection.php";
require_once "class/member-class.php";
// print_r($_SESSION);
if(!isset($_SESSION['member_id']))
{
  // echo 'not set';
  header('location:login.php');
}
 if(isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
  }
// print_r($member);
if (isset($_POST['edit_change'])) {
  $member=new member($_SESSION['member_id']);
$name = $_POST['name'];
$about = $_POST['about'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$db=new dbconn();
$con=$db->connect();
$cmd1="UPDATE organisation_team SET about_me='$about',name = :name1 ,email = :email1, phone = :phone1 where member_id = '$member->id'";
$query1=$con->prepare($cmd1);
$query1->execute(array(":email1"=>$email,":name1"=>$name,":phone1"=>$phone));
  }
$member=new member($_SESSION['member_id']);
  ?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <title>Home</title>
  <link rel="stylesheet" type="text/css" href="style.css">
    <style type="text/css">
    label{color: black !important;}
  </style>
</head>
</head>
<body>
<?php include_once "inc/header.php";include_once "inc/side-bar.php";?>
<div class="col-md-8">


      <!-- logged in user information -->

 <form method="post" class='trans-form' action="#">


    <div class="input-group">
      <label>Name</label>

<img src="<?php echo $member->pic?>" style="border-radius: 50%;
    width: 118px;">
<a href="profile-uploader/" style="margin-left:12px;color:green">Upload New</a>

    </div>

    <div class="input-group">
      <label>Name</label>
      <input type="text" name="name" value="<?php echo $member->name?>">
    </div>
    <div class="input-group">
      <label>Email</label>
      <input type="text" name="email" value="<?php echo $member->email?>">
    </div>
   <div class="input-group">
      <label>Phone</label>
      <input type="text" name="phone" value="<?php echo $member->phone?>">
    </div>
     <div class="input-group">
      <label>About me </label>
         <textarea id="" name="about" ><?php echo $member->about?></textarea>
    </div>

    <div class="input-group" style="text-align: center;">
      <button type="submit" class="btn" name="edit_change" >Edit and Save</button>
    </div>
  </form>

</div>    
</body>
</html>