<?php 
include_once 'class/thread-class.php';
include_once 'class/misc-class.php';
include_once 'class/company-class.php';
include_once 'class/notification-class.php';
include_once 'class/message-data-class.php';
if(isset($_GET['checkin']))
{
$application=new application($_GET['checkin']);
$application->update_status('checked_in');

$thread=new thread($application->thread_id);
/*------- push notification ========================= */
$admin=new notification($thread->company_id);
$link="";
$from=$thread->job_id;
$notification="New check in for JOB ID : ".$thread->id;
$admin->push_notification($notification,$from,$thread->id,'1');
/*------- push notification ========================= */
}
if(isset($_GET['checkout']))
{
$application=new application($_GET['checkout']);
$application->update_status('checked_out');

$thread=new thread($application->thread_id);
/*------- push notification ========================= */
$admin=new notification($thread->company_id);
$link="";
$from=$thread->job_id;
$notification="New check out for JOB ID : ".$thread->id." Visit Payment for settlements";
$admin->push_notification($notification,$from,$thread->id,'1');
/*------- push notification ========================= */
}
if(isset($_GET['apply']))
{
 $thread=new thread($_GET['apply']);
  if(!$thread->has_appplied())
  {
  $thread->apply_job();
  }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>User Dashboard</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="style.css">
<style type="text/css">
  #new_chat_count {
    border-radius: 50%;
    background: #ffb748;
    width: 30px;
    height: 30px;
    color: white;
    padding: 7px;
    float: right;
    margin-right: 2px;
}
</style>
</head>
<body style="">
<?php  include_once 'inc/header.php';include_once 'inc/admin_pops.php';?>
<div class="container" >
  <h2>My Dashboard (12)</h2>
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home" onclick="show_my_feeds()">FEED</a></li>
    <li><a data-toggle="tab" href="#menu1" onclick="show_my_messages()">MESSAGE</a></li>
    
    <li><a data-toggle="tab" href="#menu2" onclick="show_my_requests()">REQUEST</a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>Feed</h3>
 </div>
<!-- ==== ----  feed endshere ===================================== --> 
    <div id="menu1" class="tab-pane fade">
    </div>
    <!--- rEQUESTED JOBS COMES HERE ======================== -->
    <div id="menu2" class="tab-pane fade">

    </div>
    <!-- ====  REQUESTED JOBS ENDS HERE =================== -->
  </div>
  
<div id="messenger-chat" >
<a href="javascript:void(0)" onclick="this.style.display='none';close_messenger()" id="msg-close-btn">X</a>
<iframe src="" id="msg_id"></iframe>
</div>

</div>
<script>  
function open_messenger(comp){

  document.getElementById('msg_id').src="my-chat-small.php?comp="+comp;
  $("#messenger-chat").show();
  $("#msg-close-btn").show();

}
function close_messenger(){
  $("#messenger-chat").hide();
}

function show_my_messages(){

  var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
document.getElementById("menu1").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","callscripts/show_messages_company_list.php?show_messages=1",true);
xmlhttp.send();
}
function show_my_requests(){

  var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
document.getElementById("menu2").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","callscripts/show_applied_list.php?show_all=1",true);
xmlhttp.send();
}
function show_my_feeds(){

  var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    { 
document.getElementById("home").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","callscripts/show_my_feeds.php?show_all=1",true);
xmlhttp.send();
}
function load_all_interview_questions(thread){
  var xmlhttp;   
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if(xmlhttp.readyState==4 && xmlhttp.status==200)
    {
document.getElementById('question_loader_apply').innerHTML=(xmlhttp.responseText);
    }
  }
xmlhttp.open("GET","callscripts/load_interview_question_list.php?thread="+thread,true);
xmlhttp.send();
}
</script>
</body>
</html>
