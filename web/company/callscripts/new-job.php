<?php
require_once "../class/db-connection.php";
require_once "../class/notification-class.php";
require_once "../class/company-class.php";
if (isset($_POST['new_job'])) {

$job_title = $_POST['job_title'];
$job_description  = $_POST['job_description'] ;
$job_fare_rate = $_POST['job_fare_rate'];
$working_hours = $_POST['working_hours'];
$job_location=$_POST['job_location'];
$no_of_freelancer=$_POST['no_of_freelancer'];
$duration_in_days=$_POST['duration_in_days'];
$deadline_date=$_POST['deadline_date'];
$job_location=$_POST['job_location'];
$start_date=$_POST['start_date'];
$end_date=$_POST['end_date'];
$skills=$_POST['skills'];
$deadline_date=$_POST['deadline_date'];
$job_actual_time=$_POST['job_actual_time'];
$job_end_time=$_POST['job_end_time'];
$city=$_POST['city'];
$ques1=$_POST['ques1'];
$ques2=$_POST['ques2'];
$ques3=$_POST['ques3'];
$ques4=$_POST['ques4'];
$ques5=$_POST['ques5'];
$ques6=$_POST['ques6'];
$ques7=$_POST['ques7'];
$ques8=$_POST['ques8'];
$ques9=$_POST['ques9'];
$ques10=$_POST['ques10'];
date_default_timezone_set("Asia/calcutta");

$post_time =(date('Y-m-d H:i:s'));
// security data 
$owner_id=$_SESSION['company_id'];
$c1=new company($owner_id);
$thread_id=rand(1234,9999);
$db=new dbconn();
$con=$db->connect();
$cmd="INSERT INTO job_process(thread_id,thread_title,location,fare_rate,thread_description,city,job_duration,start_date,end_date,deadline_date,owner_id,post_timings,job_count,skills,job_actual_time,end_timings) values ('$thread_id',:job_title1,:job_location1,:job_fare_rate1,:job_description1,'$city','$working_hours','$start_date','$end_date','$deadline_date','$owner_id','$post_time','$no_of_freelancer','$skills','$job_actual_time','$job_end_time')";
$query=$con->prepare($cmd);
$result=$query->execute(array(":job_title1"=>$job_title,":job_location1"=>$job_location,":job_fare_rate1"=>$job_fare_rate,":job_description1"=>$job_description));

if($result)
{
    $cmd="INSERT INTO job_interview(thread_id,query1,query2,query3,query4,query5,query6,query7,query8,query9,query10) values ('$thread_id',:ques1,:ques2,:ques3,:ques4,:ques5,:ques6,:ques7,:ques8,:ques9,:ques10)";
$query=$con->prepare($cmd);
$query->execute(array(":ques1"=>$ques1,":ques2"=>$ques2,":ques3"=>$ques3,":ques4"=>$ques4,":ques5"=>$ques5,":ques6"=>$ques6,":ques7"=>$ques7,":ques8"=>$ques8,":ques9"=>$ques9,":ques10"=>$ques10));


}


/* ================== notify theother users realted to this job 
/*------- push notification ========================= */
				 	$d1=new dbconn();$all_users=array();
	               	$db=$d1->connect();
        			 $cmd="SELECT member_id,name FROM organisation_team";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
				                   	                      array_push($all_users,$row);
				                                               }
for($k=0;$k<sizeof($all_users);$k++)
{
$admin=new notification($all_users[$k]['member_id']);
$link=$thread_id;
$from=$_SESSION['company_id'];
$notification="Dear ".$all_users[$k]['name']." New Job titled - ".$job_title."has been pposted at the time tap.click below to view.";
$admin->push_notification($notification,$from,$link);
}
$admin=new notification($owner_id);
$link="";
$from=$owner_id;
$notification="Congratulations ! Your job has been posted successfully. Soon you wil start recieving job requests";
$admin->push_notification($notification,$from,$link);
/*------- push notification ========================= */
            // send push notifications ===================
$admin=new notification('admin');
$link="";
$from=$owner_id;
$notification="New Job posted ".$c1->title." at Time Tap ";
$admin->push_notification($notification,$from,$link,'0','New Job');
/*------- push notification ========================= */  
header("location:../index.php");

}
?>