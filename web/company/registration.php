<?php 
  include("common-header.php"); 
?>
  <div class="tab-content" >
    <div id="home" class="tab-pane fade in active">
      <form action="callscripts/register-now.php" method="post">
      <h4 class="body-font" style="margin-left: 20vw; color: #0062cc;margin-left:12%"> 
        REGISTRATION
      </h4>
      <hr>
      <div class="container" style="padding-left: 3.5vw ; padding-right: 8vw;">      
        <h4 class = "body-font" style="margin-left:0vw; font-size: 130%;">
          Company title
        </h4>
        <div class="form-group margin-bottom">
          <input type="text" class="body-font form-control back-color" rows="5" id="title" name="title" required >
        </div>
        <h4 class = "body-font" style="font-size: 120%">
          Company Description
        </h4>
        <div class="form-group margin-bottom">
          <textarea class="form-control back-color" rows="5" id="comment" name="description" required >
          </textarea>
        </div>
        <h4 class = "body-font" style="font-size: 120%">
          Registered Address
        </h4>
        <div class="form-group margin-bottom">
          <textarea class="form-control back-color" rows="5" name="address" required ></textarea>
        </div>
        <h4 class = "body-font" style="font-size: 120%">
          Mobile Number
        </h4>
        <div class="form-group margin-bottom">
          <input type="number" name="mobile" class="form-control back-color" required >
        </div>
        <h4 class = "body-font" style="font-size: 120%">
          Category
        </h4>  
        <br>
        <div class="row">
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('IT')" class="btn_bottom body-font margin-bottom" style="font-size: 100%">
              &nbsp;
              &nbsp; 
              IT 
              &nbsp; 
              &nbsp; 
            </a>
          </div>
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('Design')" class="btn_bottom body-font margin-bottom" style="font-size: 100%;">
              Design
            </a>
          </div>
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('Marketing')" class="btn_bottom body-font margin-bottom" style="font-size: 100%;">
              Marketing
            </a>
          </div>
        </div>
        <div class="row top_margin">
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('Events')" class="btn_bottom body-font margin-bottom" style="font-size: 100%;">
              Events
            </a>
          </div>
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('Automonbile')" class="btn_bottom body-font margin-bottom" style="font-size: 100%;">
              Automobile
            </a>
          </div>
          <div class="col-md-3">
            <a href="javascript:void(0)" onclick="add_cat('Manufacturing')" class="btn_bottom body-font margin-bottom" style="font-size: 100%">
              Manufacturing
            </a>
          </div>
        </div>

<!-- ----  company's catwegory hideen input ================ -->
        <input type="hidden" name="category" id="cat_id">
<!--- =  category ends here ================================= -->
        <div class="row top_margin">
          <div class="col-md-3">
            <a href = "javascript:void(0)" class="btn_bottom body-font margin-bottom" style="font-size: 100%">
              Others
            </a>
          </div>
        </div>
        <br>
        <h4 class = "body-font" style="font-size: 100%;">
          If others please mention 
          &nbsp; 
        <input type="text" class="bottom_border margin-bottom" name="other_category" > 
        </h4>
        <br>
               <h4 class = "body-font margin-bottom" style="font-size: 120%;">
        Supervisor Details
        </h4>
            <div class="form-group ">
                    <input type="text" class="body-font form-control back-color margin-bottom" placeholder="Supervisor Email" name="supervisor_email" style="width: 50%;">
              </div>
              <div class="form-group">
                    <input type="password" class="body-font form-control back-color margin-bottom" placeholder="Supervisor password"  name="supervisor_password" style="width: 50%;">
              </div>
        <input type="submit" value="Save" class="submit_btn" name="reg_comp">  
        </form>    
        </div>

      </div>
    </div>

    </div>
<script>
function add_cat(cat){
document.getElementById('cat_id').value=cat;
}
</script>