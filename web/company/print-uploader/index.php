<?php require_once "../class/db-connection.php";require_once "../class/company-class.php";
$m1=new company($_SESSION['company_id']);?>
<!doctype html>
<html>
<head lang="en">
	<meta charset="utf-8">
	
	<link rel="stylesheet" type="text/css" href="css/imgareaselect-animated.css" />
	<!-- scripts -->
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.imgareaselect.pack.js"></script>
	<script type="text/javascript" src="js/script.js"></script>

	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
	a, a h1{
		font-family: Georgia, "Times New Roman", Times, serif;
		font-size: 1.2em;
		color: #645348;
		font-style: italic;
		text-decoration: none;
		font-weight: 100;
		padding: 10px;
	}
	body{
		font: 12px Arial,Tahoma,Helvetica,FreeSans,sans-serif;
		text-transform: inherit;
		color: #582A00;
		background: #E7EDEE;
		width: 100%;
		margin: 0;
		padding: 0;
	}
	.wrap{
		width: 700px;
		margin: 10px auto;
		padding: 10px 15px;
		background: white;
		border: 2px solid #DBDBDB;
		-webkit-border-radius: 5px;
		-moz-border-radius: 5px;
		border-radius: 5px;
		text-align: center;
		overflow: hidden;
	}
	img#uploadPreview{
		border: 0;
		border-radius: 3px;
		-webkit-box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		box-shadow: 0px 2px 7px 0px rgba(0, 0, 0, .27);
		margin-bottom: 30px;
		overflow: hidden;
	}
	input[type="submit"]{
		  background-color: #61B3DE;
  border: 0;
  color: white;
  font-weight: bold;
  text-transform: uppercase;
  /* font-style: italic; */
  padding: 6px 15px 5px;
  cursor: pointer;
	}
	</style>
</head>
<body style='background:white;'>

<center>
<div class="container" style="border:2px grey solid;display: none">
<h2>Customize your T-shirt here </h2>
<img width="300" src="https://images.vexels.com/media/users/3/142546/isolated/preview/2f6d0faa355125320122dc57e8b07084-tshirt-icon-by-vexels.png" style="">
<img src="../../img-print/<?php echo $m1->print_pic?>" id="logo" style="background: none;width:60px;    margin-left: -168px;" >


<div class="row" style="dis">
      <a class="btn btn-danger " onclick="go_left()">LEFT</a>
      <a class="btn btn-danger " onclick="go_right()">RIGHT</a>
      <a class="btn btn-danger " onclick="go_top()">TOP</a>
      <a class="btn btn-danger " onclick="go_bottom()">BOTTOM</a>
</div>
<div class="row" style="">
      <a class="btn btn-primary " onclick="make_small()">SMALL</a>
      <a class="btn btn-primary " onclick="make_large()">LARGE</a>

</div>
<div class="row" style="">
      <a class="btn btn-primary " onclick="fade_in()">More saturation</a>
      <a class="btn btn-primary " onclick="fade_out()">Fade away</a>

</div>

</div>


<div class="wrap">
<img src="../../img-print/<?php echo $m1->print_pic?>" id="logo" style="background: none;width:60px;    margin-left: -168px;" >

	<h2>This image logo will appear fro al future printing apparels </h2>
		<strong class="theme1">Resize your image to upload</strong>
	<!-- image preview area-->
	<img id="uploadPreview" style="display:none;  max-width: 600px;
  max-height: 600px;"/>
	
	<!-- image uploading form -->
	<form action="upload.php" method="post" enctype="multipart/form-data">
		<input id="uploadImage" type="file" accept="image/jpeg" name="image" style=''/>
		<input type="submit" value="Upload">

		<!-- hidden inputs -->
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
	</form>
	
</div><!--wrap-->


<!-- LOGIC FOR T-SHIRTT BULIDER STARTS ======================= -->
<script>
function go_left()
{

var curr=document.getElementById("logo").style.marginLeft;
var curr_value=curr.split('px')[0];
var new1=+curr_value-10;
document.getElementById("logo").style.marginLeft=new1+"px";
}
function go_right()
{
var curr=document.getElementById("logo").style.marginLeft;
var curr_value=curr.split('px')[0];
var new1=+curr_value+10;
document.getElementById("logo").style.marginLeft=new1+"px";
}
function go_top()
{

var curr=document.getElementById("logo").style.marginTop;
var curr_value=curr.split('px')[0];
var new1=+curr_value-10;
document.getElementById("logo").style.marginTop=new1+"px";
}

function go_bottom()
{

var curr=document.getElementById("logo").style.marginTop;
var curr_value=curr.split('px')[0];
var new1=+curr_value+10;
document.getElementById("logo").style.marginTop=new1+"px";
}


function make_small()
{

var curr=document.getElementById("logo").style.width;
var curr_value=curr.split('px')[0];
var new1=+curr_value-10;
document.getElementById("logo").style.width=new1+"px";
}


function make_large()
{

var curr=document.getElementById("logo").style.width;
var curr_value=curr.split('px')[0];
var new1=+curr_value+10;
document.getElementById("logo").style.width=new1+"px";
}



function fade_in()
{var em = document.getElementById("logo");
var  curr = window.getComputedStyle(em).getPropertyValue("opacity");

var curr_value=curr.split('px')[0];
var new1=+curr_value+(0.06);
document.getElementById("logo").style.opacity=new1;
}


function fade_out()
{var em = document.getElementById("logo");
var  curr = window.getComputedStyle(em).getPropertyValue("opacity");

var curr_value=curr.split('px')[0];
var new1=+curr_value-(0.06);
document.getElementById("logo").style.opacity=new1;
}
</script>


<!-- ==  LOGIC FOR T-SHIRT BUILDER ENDS HERE ================= -->
</center>
</body>
</html>