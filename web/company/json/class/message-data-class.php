<?php
require_once "db-connection.php";
require_once "member-class.php";
require_once "misc-class.php";

/* class : for front end includes 
/* author : chandan jha 
createiond date : 23 march 
last modifies : 23 march 2017
*/


function new_message($content1,$triggered_from,$triggered_to,$owner_id,$attach_mail){
   	    $d1=new dbconn();
	   	$db=$d1->connect();
    // set the PDO error mode to exception
	   	date_default_timezone_set("Asia/calcutta");
    $timings1=date('Y-m-d H:i:s');
    $qry=$db->prepare("INSERT INTO `message_history`(`message_content`, `timings`, `triggered_from`, `member_id`,`attach_mail`,`owner_id`) VALUES (:content1,'$timings1','$triggered_from', '$triggered_to','$attach_mail','$owner_id');");
    $qry->execute(array(':content1'=>$content1));



    }

class message_records{

function __construct($member_id){
$this->id=$member_id;
$this->message_record_data();
     }



function is_other_one_typing($company_id){

		   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
        			 $cmd="SELECT typing from message_history where triggered_from='$company_id' and member_id='$this->id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				              $is_typing=$row['typing'];
				                   	                   }
				                   	          return $is_typing;
}

     function company_conversation($company_id){

				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
        			 $cmd="SELECT mid from message_history where owner_id='$company_id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   	 array_push($arr1,$row);
				                   	               }
				                   	               return $arr1;

     }
     function fetch_all_msg_thread(){

				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
        			 $cmd="SELECT DISTINCT(triggered_from) from message_history where member_id='$this->id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   	 array_push($arr1,$row);
				                   	               }
				                   	               return $arr1;


     }

     function last_message_id($id)
     {
     		   	$d1=new dbconn();$arr1=array();$arr2=array();$arr3=array();
	               	$db=$d1->connect();
        			 $cmd="SELECT (mid) from message_history where triggered_from='$id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   $mid=$row['mid'];
				                   	               }

				                   	               return $mid;
     }
					function message_record_data(){
				   	$d1=new dbconn();$arr1=array();$arr2=array();$arr3=array();
	               	$db=$d1->connect();
        			 $cmd="select mid from message_history where member_id='$this->id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   	 array_push($arr1,$row);
				                   	               }
				     $this->reciever_count=sizeof($arr1);
        			 $cmd="select mid from message_history where triggered_from='$this->id' and is_important='1'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   	 array_push($arr2,$row);
				                   	               }
				       $this->starred_count=sizeof($arr2);            	               
				      $cmd="select mid from message_history where triggered_from='$this->id'";
				     $result=$db->query($cmd);
                                     while($row=$result->fetch()){ 
				                   	 array_push($arr3,$row);
				                   	               }


                                                   $this->sender_count=sizeof($arr3);
				                   	               $this->sender_count=sizeof($arr2);

				                   	               $this->all_message_count=$this->reciever_count+$this->sender_count;
                                         }



                     function fetch_all_messages(){
                     $d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT * from message_history where member_id='$this->id' ORDER BY mid desc";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }


                                         }



                    function fetch_starred_messages(){

                                         				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT * from message_history where triggered_from='$this->id' and is_important='1' ORDER BY mid desc";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }
				                   	                    	   return $arr1;


                    }

                                           function fetch_send_messages(){
                                         				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT * from message_history where triggered_from='$this->id' ORDER BY mid desc";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }
				                   	                    	   return $arr1;

                                         }

                                          function fetch_inbox_messages(){
                                         				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	$id=$this->id;
        			 $cmd="SELECT * from message_history where member_id='$this->id' ORDER BY mid desc";
				     $result=$db->query($cmd);
                                   while($row=$result->fetch()){ 
                                   		     array_push($arr1,$row);
				                   	                    	   }

				                   	                    	   return $arr1;
                                         }



}
class message_flow{


function __construct($mid){
$this->id=$mid;
$this->message_data();
     }

function return_members_profile_pic(){
	$dir='';
	$member1=new member($this->sender);
	$this->sender_pic=$dir.$member1->profile_pic;
	$this->sender_name=$member1->name;
	$member2=new member($this->reciever);
	$this->reciever_pic=$dir.$member2->profile_pic;
	$this->reciever_name=$member2->name;
                             }


function star_unstar(){
$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
	               	if($this->is_starred){
				     	 $cmd="UPDATE message_history set is_important='0' where mid='$this->id'";
                        }
                        else
				     	 $cmd="UPDATE message_history set is_important='1' where mid='$this->id'";
				     $result=$db->query($cmd);

}
			function message_data(){
				   	$d1=new dbconn();$arr1=array();
	               	$db=$d1->connect();
        			 $cmd="select * from message_history where mid='$this->id'";
				     $result=$db->query($cmd);
                     while($row=$result->fetch()){ 
				                   	        array_push($arr1,$row);
				                                 }
				            // initialize all values here
				                    $this->content=$arr1[0]['message_content'];
				                    $this->subject=$arr1[0]['msg_subject'];
				                    $this->company=$arr1[0]['owner_id'];
				                    $this->attachment=$arr1[0]['attach1'];
				                    $this->sent_at=$arr1[0]['timings'];
				                    $this->sender=$arr1[0]['triggered_from'];
				                    $this->reciever=$arr1[0]['member_id'];
				                    $this->is_read=$arr1[0]['is_read'];
				                    $this->is_starred=$arr1[0]['is_important'];
				                    $this->reply_text=$arr1[0]['reply_text'];
				                    if(empty($this->reply_text))
				                    {
				                    	$this->replied=0;
				                    }
				                    else
				                    	$this->replied=1;
									if($this->is_read)
									{
									$this->read_at=$arr1[0]['read_timings'];	
									}
									//$this->return_members_profile_pic();	
									if(($this->sender)==($this->company))
									{
									$company=new company($arr1[0]['triggered_from']);
									$this->pic=$company->pic;
									$this->sender_name=$company->title;
									}
								   else
								   {
								   	$member=new member($arr1[0]['triggered_from']);
									$this->pic=$member->pic;
									
									$this->name=$member->name;
								    }
						
									

									$m1=new misc();
									$this->timespan=$m1->return_timespan($this->sent_at);	
									// return the other person than the logged member 
									if(($this->sender==$_SESSION['member_id']))
									$this->other_person_involed=$this->reciever;	 
									else
								    $this->other_person_involed=$this->sender;	 


			                  }

			                        function reply_seen(){
			                        	$d1=new dbconn();
	                                        	$db=$d1->connect();
			                        	$cmd="update message_history set reply_seen='1' where mid='$this->id'";
                                        $result=$db->query($cmd);
			                        	$this->is_read=1;
			                            }

			                             function reply_sender($reply_message){
			                             	  	$d1=new dbconn();
	                                        	$db=$d1->connect();
			                             	if(!$this->replied)
			                             	{
			                         	$cmd="update message_history set reply_text='$reply_message' where mid='$this->id'";
                                        $result=$db->query($cmd);
			                        	$this->is_read=1;
			                        		}

			                             }


}



?>