<?php error_reporting(0);
require_once 'class/thread-class.php';
require_once 'class/application-class.php';
require_once 'class/misc-class.php';
require_once 'class/company-class.php';
require_once 'class/notification-class.php';
require_once 'class/message-data-class.php';
if(isset($_GET['id']))
{
$application=new application($_GET['id']);
$application->update_status('checked_out');
$thread=new thread($application->thread_id);
/*------- push notification ========================= */
$admin=new notification($thread->company_id);
$link="";
$from=$application->job_seeker;
$notification="New check out for JOB ID : ".$thread->id." Visit Payment for settlements";
$admin->push_notification($notification,$from,$thread->id,'1');
/*------- push notification ========================= */
  $arr['status']='success';
  $arr['status_text']='Congratulations ! '.$application->job_seeker_name." You are successfully checked out of this job ".$application->thread_id." and you will be notified for your payment settlements soon.";
  $myJSON = json_encode($arr);
  echo $myJSON;
}else
{
  $arr['status']='failure';
  $arr['status_text']='Please provide id as parameter';
  $myJSON = json_encode($arr);
  echo $myJSON;
}
?>