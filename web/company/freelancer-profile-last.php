<?php require_once("inc/header.php"); 
include_once "inc/chat-box.php"; 
require_once("class/member-class.php");
$member=new member($_GET['user']);
$member->task_data();
?>
<div class="container-fluid profile-box" style="padding-top:42px;">

<div class="row" style="">
<div class="col-md-3"></div>
<div class="col-md-6 image-box">
<div class="pull-left col-md-6 text-right">
<div class="profile-wrap">
  <img src="<?php echo $member->pic?>" width="120px" style="  border: 3px solid #efefef;">
   <p class="job-seeker-name"><?php echo $member->name?>
    <?php 
if($member->is_online)
echo "<div class='online-icon'></div>";
?>
  </p>
  <p><?php echo $member->hometown?></p>
  <p><?php echo $member->designation?></p>
  <p><?php echo $member->passed_out?></p>
</div>
 

</div>
<ul class="pull-right job-stats col-md-6">
 <li style="">Total jobs : <?php echo $member->total_count?></li>
    <li style="">Ongoing jobs : <?php echo $member->ongoing_count ?></li>
    <li style="">Delivered jobs : <?php echo $member->paid_count ?></li>
    <li style="">Disputed jobs : <?php echo $member->disputed_count ?></li>
    <li style=""><a href='javascript:void(0)' onclick="open_messenger('<?php echo $member->id?>')" class="btn chat-now-btn" style="color:white">Chat Now</a></li>
        <li style="color:grey;display:none" >
   <a href='javascript:void(0)' onclick="open_messenger('<?php echo $member->id?>')" class="pull-right btn chat-now-btn" style="color:white">Chat Now</a>
     </li>
</ul>
</div>
<div class="col-md-4">
</div>

</div>

<div class="col-md-2"></div>

</div><!-- row ========== -->
</div>
<!-- === about ============================= -->
<div class="container profile-about">

<div class="row text-center">

<div class="col-md-4">
 
</div>
<!-- ===  tab ========================================================================= -->
<div class="container">
 <div class="col-md-8">
    <ul class="nav nav-tabs" style="margin-bottom: 43px;">
    <li class="active"><a data-toggle="tab" href="#home">Overview</a></li>
    <li><a data-toggle="tab" href="#menu1">Reviews</a></li>
    <li><a data-toggle="tab" href="#menu2">Job stats</a></li>
    </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
              <h4 class="about theme1">About myself </h4>
                    <p class="about">
                     <?php echo $member->about?>
                    </p>
    </div>
    <div id="menu1" class="tab-pane fade">
    <!-- reviews ====================================== -->
<h4 class="about theme1">Reviews </h4>
<br><br>
<div class="row">
<?php 
$all_review=$member->all_review();
for ($r=0; $r < sizeof($all_review); $r++) { 
  # code...
?>
<div class="review-box" style="width:70%;">
<div class="pull-left">
<img src="../img-company/5a72c1831f4e7.jpg" width="75" style="border-radius: 50%">
</div>
<div class="pull-right">
<ul>
<li class="same-line">
<?php for ($p=0; $p <4; $p++) { 
   echo "<img src='../icon/star_bold.png' width='12'>"; 
} ?>
</li>
<li class="same-line">
<?php for ($i=3; $i < 10; $i++) { 
   echo "<img src='../icon/star.png' width='12'>"; 
} ?>
</li>
</ul>
<ul style="margin-left: 13%;
    font-size: 15px;">
<li><?php echo $all_review[$r]['reviews']?></li>
<li><?php echo return_timespan($all_review[$r]['timings'])?></li>
</ul>
</div>  
</div>
<?php } ?>
    <!-- == reviews ends here =========================== -->
    </div>
    </div>
    <div id="menu2" class="tab-pane fade">
  <?php  $thread_arr=$member->member_job_delivered();
for($r=0;$r<sizeof($thread_arr);$r++)
{
$thread=new thread($thread_arr[$r]['thread_id'])
?>
<ul type="none" class="pull-left text-left grey-box">
<li><h4 class="theme1"><?php echo $thread->title?></h4></li>
<li><strong><?php echo $thread->job_start?></strong></li>
<li><img src="../img-company/<?php echo $thread->company_logo?>" class="profile-pic"></li>
<li><strong><?php echo $thread->company?></strong></li>
<li><strong><?php echo $thread->job_start?></strong></li>
</ul>
<?php 
}  ?>
    </div>
  </div>

<!-- == tab ends here ================================================================== -->
</div><!-- tab content ========== -->
</div><!-- == comntainer ======== -->
<script>
function add_cat(cat){
document.getElementById('cat_id').value=cat;
}
function verify_number(){
var mobile=document.getElementById('mobile1').value;
window.location="otp-verification.php?mobile="+mobile;
}
</script>
<?php include_once "inc/footer.php"; ?>