package com.example.pragya.tym_tap.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin;
    TextView tv_sign_up, tv_forgotpassword;
    ImageView fb_login;
    String httpURL = "http://monkhub.com/tap-real/json/login.php?email=";
    EditText edt_email, edt_password;
    String email,password;
    String url;

    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        initialize();

    }

    private void initialize() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        edt_email=(EditText)findViewById(R.id.edtUserId);
        edt_password=(EditText)findViewById(R.id.edtPassword);
        tv_sign_up=(TextView)findViewById(R.id.tv_sign_up);
        btnLogin=(Button)findViewById(R.id.btnSignIn);
        tv_forgotpassword = (TextView)findViewById(R.id.tvforgotPassword);
        fb_login = findViewById(R.id.facebooklogin);


        tv_forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent in_forgotpass = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(in_forgotpass);
                finish();

            }
        });

        tv_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(in);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = edt_email.getText().toString();
                password = edt_password.getText().toString();


                if(email.isEmpty()|| password.isEmpty()){

                    Toast.makeText(LoginActivity.this, "fill all details", Toast.LENGTH_SHORT).show();

                }else
                {
                    btnSubmit(email,password);
                }

            }
        });
    }

    private void btnSubmit(final String email, final String password) {

        showpDialog();

        url = httpURL+email+"&password="+password;

        //url = "http://monkhub.com/tap-real/json/login.php?email="+email+"&password="+password;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SharedPref.putString(LoginActivity.this,"LOGIN_DATA",response.toString());
                try {
                    if (response.getString("status").equals("success")){
                        SharedPref.putString(LoginActivity.this, AppConstant.SF.LOGIN_RESPONSE,response.toString());
                        Intent in_tent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(in_tent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(),"Login failed",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }
                hidepDialog();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error:"+error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to login. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
                hidepDialog();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("email", edt_email.getText().toString().trim());
                params.put("password", edt_password.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
}



