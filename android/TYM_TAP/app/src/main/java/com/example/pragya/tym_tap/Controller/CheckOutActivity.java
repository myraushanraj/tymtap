package com.example.pragya.tym_tap.Controller;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.tabs.request.RequestModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckOutActivity extends AppCompatActivity {


    Dialog MyDialog;
    Button yes,no;
    @BindView(R.id.tvJobName)
    TextView tv_job_name;

    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;

    @BindView(R.id.tvLabel)
    TextView tvLabel;

    @BindView(R.id.tvLabe2)
    TextView tvLabe2;

    //    String status_text;
    RequestModel requestModel;

    Button btn_check_out;
//    Dialog MyDialog;
 //  Button btn_yes, btn_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        ButterKnife.bind(this);

        requestModel = (RequestModel) getIntent().getSerializableExtra(AppConstant.INTENT_TAG.CHECKING_ID);
        tv_job_name.setText(requestModel.getJob_title());
        tvCompanyName.setText(requestModel.getCompany_name());
        //tvLabel.setText(requestModel.getJob_start_in());
        tvLabe2.setText(requestModel.getJob_end_time());


        btn_check_out = findViewById(R.id.btnCheckoutNow);
        btn_check_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MyCustomAlertDialog();

            }
        });
    }

    private void MyCustomAlertDialog() {


        MyDialog = new Dialog(CheckOutActivity.this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.checkout_popup);


        yes = (Button) MyDialog.findViewById(R.id.yes);
        no = (Button) MyDialog.findViewById(R.id.no);

        yes.setEnabled(true);
        no.setEnabled(true);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    jobCheckOut();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.cancel();
            }
        });

        MyDialog.show();
    }


    private void jobCheckOut() throws JSONException {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "http://monkhub.com/tap-real/json/check-out.php?id=" + requestModel.getId();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("success")) {
                                btn_check_out.setVisibility(View.INVISIBLE);
                                Intent redirect_intent = new Intent(getApplicationContext(), ApplyMoreJobsActivity.class);
                                startActivity(redirect_intent);

                            } else {
                                Toast.makeText(CheckOutActivity.this, "Data is not updated", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(CheckOutActivity.this, "error: " + e, Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(CheckOutActivity.this, "Error:" + error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to Check-Out. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);

    }


}
