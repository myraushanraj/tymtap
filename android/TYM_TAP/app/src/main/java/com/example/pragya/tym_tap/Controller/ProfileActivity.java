package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    Toolbar profile_toolbar;

    TextView tv_email, tv_member;

    Button btn_update;
    EditText edt_Name, edt_PhoneNo, edt_About, edt_designation, edt_email, edt_hometown, edt_institute;

    String name, email, about, phone, member, designation, hometown, institute;
    JSONObject jsonObject = null;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        profile_toolbar = findViewById(R.id.toolbar_prof);
        setSupportActionBar(profile_toolbar);

        try {
            jsonObject = new JSONObject(SharedPref.getString(ProfileActivity.this, AppConstant.SF.LOGIN_RESPONSE));
            initialize(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
            try {
                initialize(jsonObject);
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        if (id == R.id.user) {
//
//            Intent intent = new Intent(this, ProfileDetailsActivity.class);
//            this.startActivity(intent);
//            return true;
//        }
//
//        if (id == R.id.bell_noti) {
//            Intent intent = new Intent(this, NotificationActivity.class);
//            this.startActivity(intent);
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }


    private void initialize(JSONObject jsonObject) throws JSONException {
        edt_Name = (EditText) findViewById(R.id.edtName);
        edt_About = (EditText) findViewById(R.id.edtAbout);
        edt_PhoneNo = (EditText) findViewById(R.id.edtPhoneNumber);
        tv_email = (TextView) findViewById(R.id.edtEmailId);
        tv_member =(TextView)findViewById(R.id.tv_member);
        edt_designation = (EditText) findViewById(R.id.edtDesignation);
        edt_hometown = (EditText) findViewById(R.id.edtHometown);
        edt_institute = (EditText) findViewById(R.id.edtInstitute);

        getdata();

        btn_update = (Button) findViewById(R.id.btn_Update);

        if (jsonObject == null) {
            return;
        }



        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = edt_Name.getText().toString();
                about = edt_About.getText().toString();
                //email = edt_email.getText().toString();
                //String conpassword = edt_ConPassword.getText().toString();
                member = tv_member.getText().toString();
                phone = edt_PhoneNo.getText().toString();
                designation = edt_designation.getText().toString();
                hometown = edt_hometown.getText().toString();
                institute = edt_institute.getText().toString();

                if (validateMobile(phone)) {
                    btnSubmit(name, designation, institute,hometown, phone, about, member);
                }

            }
        });

    }

    public void getdata() throws JSONException {

        jsonObject = new JSONObject(SharedPref.getString(ProfileActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        String url_load = "http://monkhub.com/tap-real/json/member_data.php?member="+jsonObject.getJSONObject("0").getString("member_id");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
            url_load, null, new Response.Listener<JSONObject>() {

        @Override
        public void onResponse(JSONObject response) {
            //Log.d(TAG, response.toString());

            try {
                edt_Name.setText(response.getString("name"));
                edt_PhoneNo.setText(response.getString("phone"));
                edt_About.setText(response.getString("about"));
                tv_member.setText(response.getString("id"));
                tv_email.setText(response.getString("email"));
                edt_designation.setText(response.getString("designation"));
                edt_hometown.setText(response.getString("hometown"));
                edt_institute.setText(response.getString("institute"));


            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),
                        "Error: " + e.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
            //hidepDialog();
        }
    }, new Response.ErrorListener() {

        @Override
        public void onErrorResponse(VolleyError error) {
            //VolleyLog.d(TAG, "Error: " + error.getMessage());
            //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
            if (error.networkResponse == null){
                if (error.getClass().equals(TimeoutError.class)){
                    Toast.makeText(getApplicationContext(),
                            "Failed to upload user profile. Please try again later.", Toast.LENGTH_LONG).show();
                }
            }
            // hide the progress dialog
            //hidepDialog();
        }
    });

        // Adding request to request queue
        ApppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    private boolean validateMobile(String string) {

        if (string.length() != 10) {
            edt_PhoneNo.setError("Enter 10 digit mobile number");
            return false;
        }
        //edt_number.setErrorEnabled(false);
        return true;
    }

    public void btnSubmit(final String name, final String designation,final String institute, final String hometown, final String phone, final String about, final String member) {
        url = "http://monkhub.com/tap-real/json/edit-profile.php?name="+name+"&phone="+phone+"&member="+member+"&about="+about+"&institute="+institute+"&hometown="+hometown+"&designation="+designation;
//url="http://monkhub.com/tap-real/json/edit-profile.php?name=raj%20sharma&phone=987654321&member=4910&about=hi%20i%20am%20chandan%20jha%20&institute=manipal&hometown=ranchi&designation=hero";
        //Toast.makeText(getApplicationContext(),"URL: "+url,Toast.LENGTH_LONG).show();
        url=url.replace(" ", "%20");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equals("success")) {
                        //String url="http://monkhub.com/tap-real/json/member_data.php?member="+jsonObject.getJSONObject("0").getString("member_id");
                        //Toast.makeText(getApplicationContext(), "Data successfully updated", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(intent);
                        finish();
                    } else {

                        Toast.makeText(getApplicationContext(), "Data is not updated", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: " + e, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error:" + error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Oops. Timeout error! Try Again", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                //params.put("email", edt_email.getText().toString().trim());
                params.put("about", edt_About.getText().toString().trim());
                params.put("name", edt_Name.getText().toString().trim());
                params.put("phone", edt_PhoneNo.getText().toString().trim());
                params.put("designation", edt_designation.getText().toString().trim());
                params.put("hometown", edt_hometown.getText().toString().trim());
                params.put("institute", edt_institute.getText().toString().trim());
                params.put("member_id", tv_member.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
