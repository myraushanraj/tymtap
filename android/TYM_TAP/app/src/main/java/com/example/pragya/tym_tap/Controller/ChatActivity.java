package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChatActivity extends AppCompatActivity {
    private WebView myWebView, webview1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        myWebView = (WebView) findViewById(R.id.webview);

        //webview1 = findViewById(R.id.webview1);

        //String url = getIntent().getStringExtra("url");
        myWebView.setWebViewClient(new WebViewClient());
        //WebSettings webSettings = myWebView.getSettings();

        /*Intent supervisor_chatClick = getIntent();
        String uri = supervisor_chatClick.getStringExtra("uri");
        setContentView(mywebView);
        mywebView.loadUrl(uri);*/
        //myWebView.loadUrl(url);

        WebSettings webSettings= myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);


        try {
            JSONObject jsonObject = new JSONObject(SharedPref.getString(this, AppConstant.SF.LOGIN_RESPONSE));

            //String company_ID = getIntent().getStringExtra("company");

            //String company_ID = getIntent().getStringExtra("Company_id");

            //mywebView.loadUrl("http://monkhub.com/chat-box/my-chat-small.php?owner=" + jsonObject
                    //.getJSONObject("0").getString("member_id") + "&member="+companyID);

            myWebView.loadUrl("http://monkhub.com/chat-box/my-chat-small.php?owner=" + jsonObject
                    //.getJSONObject("0").getString("member_id") + "&member="+getIntent().getStringExtra("Company_id"));
                    .getJSONObject("0").getString("member_id") + "&member="+getIntent().getStringExtra("Company_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


       // http://monkhub.com/chat-box/my-chat-small.php?member=3841&owner=1720
        // Line of Code for opening links in app
        //mywebView.loadData(myHtmlString, "text/html", null);
        myWebView.setWebViewClient(new WebViewClient());
    }

    //Code For Back Button
    @Override
    public void onBackPressed() {
        if(myWebView.canGoBack())
        {
            myWebView.goBack();
        }
        else
        {
            super.onBackPressed();
        }
    }
}