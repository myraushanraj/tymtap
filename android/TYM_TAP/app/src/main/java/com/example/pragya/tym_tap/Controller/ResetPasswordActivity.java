package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity {

    EditText edt_NewPassword, edt_ConfirmPassword;
    Button btn_pass_submit;


    String update_password, confirm_password;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);




        initialize();


    }

    private void initialize() {

        edt_NewPassword = (EditText)findViewById(R.id.edt_NewPaswrd);
        edt_ConfirmPassword = (EditText)findViewById(R.id.edt_ConfirmPswrd);

        btn_pass_submit = (Button)findViewById(R.id.btn_NewPswrd);
        btn_pass_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                update_password = edt_NewPassword.getText().toString();

                confirm_password = edt_ConfirmPassword.getText().toString();


                if (update_password.isEmpty()||confirm_password.isEmpty()){

                    Toast.makeText(getApplicationContext(), "Fill all the details",Toast.LENGTH_SHORT).show();

                }else if (update_password.equals(confirm_password)){

                    submit_password(update_password);
                    //login_account(update_password);

                }else if (!update_password.equals(confirm_password)){


                    Toast.makeText(getApplicationContext(), "Password & Confirm Password are not match",Toast.LENGTH_SHORT).show();


                }
            }
        });
    }

    /*private void login_account(final String update_password) {

        Intent i = getIntent();
        final String email = i.getStringExtra("TextBox");

        url = "http://monkhub.com/tap-real/json/login.php?email="+email+"&password="+update_password;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SharedPref.putString(ResetPasswordActivity.this,"LOGIN_DATA",response.toString());
                try {
                    if (response.getString("status").equals("success")){
                        SharedPref.putString(ResetPasswordActivity.this, AppConstant.SF.LOGIN_RESPONSE,response.toString());
                        Intent in_tent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(in_tent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(),"Login failed",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error:"+error, Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                //params.put("email", edt_email.getText().toString().trim());
                //params.put("password", edt_password.getText().toString().trim());
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }*/

    private void submit_password(final String update_password) {

        Intent i = getIntent();
        final String email = i.getStringExtra("TextBox");

        url = "http://monkhub.com/tap-real/json/forgot-password-user.php?email="+ email +"&update_password="+update_password;

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")){

                        Toast.makeText(getApplicationContext(), "Password successfully reset",Toast.LENGTH_SHORT).show();
                        Intent in_newpass = new Intent(getApplicationContext(), LoginActivity.class);
                        in_newpass.putExtra("TextPassword",update_password);
                        in_newpass.putExtra("TextBox",email);
                        startActivity(in_newpass);
                        finish();

                    }else {
                        Toast.makeText(getApplicationContext(), "Password is not reset",Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(), "error: "+e ,Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error: "+error ,Toast.LENGTH_SHORT).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to reset password. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("update_password", edt_NewPassword.getText().toString().trim());

                return params;
            }

        };
        requestQueue.add(jsonObjectRequest);
    }
}
