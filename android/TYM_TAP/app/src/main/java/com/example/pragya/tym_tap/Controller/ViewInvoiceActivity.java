package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.tabs.request.RequestModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Pragya on 3/26/2018.
 */

public class ViewInvoiceActivity extends AppCompatActivity {


    //RequestModel requestModel;tv_jobTitletv_transaction_details

    @BindView(R.id.tv_jobTitle)
    TextView tv_jobTitle;

    @BindView(R.id.tv_transaction_details)
    TextView tv_transaction_details;

    @BindView(R.id.tv_checkin_details)
    TextView tv_checkin_details;

    @BindView(R.id.tv_checkout_details)
    TextView tv_checkout_details;

    @BindView(R.id.tv_settlement_details)
    TextView tv_settlement_details;

    @BindView(R.id.tv_hoursinnum)
    TextView tv_hoursinnum;

    @BindView(R.id.tv_fare_rate)
    TextView tv_fare_rate;

    @BindView(R.id.tv_total_billing)
    TextView tv_total_billing;

    @BindView(R.id.tv_sercharge_ammount)
    TextView tv_sercharge_ammount;

    @BindView(R.id.tv_gst_ammount)
    TextView tv_gst_ammount;

    @BindView(R.id.tv_payable_amount)
    TextView tv_payable_amount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_invoice);
        ButterKnife.bind(this);
        RequestModel requestModel = (RequestModel) getIntent().getSerializableExtra(AppConstant.INTENT_TAG.CHECKING_ID);
        ViewInvoice(requestModel.getId());


        // @onClick(R.id.btn_redeem)
    }

    @OnClick(R.id.btn_redeem)
    void btn_redeem(){
        Intent wallet_intent = new Intent(getApplicationContext(), RedeemActivity.class);
        startActivity(wallet_intent);
    }

    @OnClick(R.id.btn_download_invoice)
    void btn_download_invoice(){
        Intent invoice_intent = new Intent(getApplicationContext(), WalletActivity.class);
        startActivity(invoice_intent);
    }

    private void ViewInvoice(String id) {
        String url = "http://monkhub.com/tap-real/json/application_payment_data.php?id="+id;
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    /*tv_jobs_number.setText(response.getString("id"));
                    tv_jobs_timedate.setText(response.getString("check_out_time"));
                    tv_paument_timedate.setText(response.getString("job_status"));
                    tv_hours_rate.setText(response.getString("job_duration"));
                    tv_rate.setText(response.getString("fare_rate"));
                    tv_final_rate.setText(response.getString("billed_formatted"));
//                    tv_service_rate.setText(response.getString("job_end"));
                    tv_service_rate.setText("0");
                    tv_gst_rate.setText(response.getString("job_end"));
                    tv_gst_rate.setText("0");
                    tv_netpayable_rate.setText(response.getString("fare_rate_formatted"));*/

                    tv_jobTitle.setText(response.getString("job_title"));
                    tv_transaction_details.setText(response.getString("id"));
                    tv_checkin_details.setText(response.getString("check_in_time"));
                    tv_checkout_details.setText(response.getString("check_out_time"));
                    tv_settlement_details.setText(response.getString("payment_release_time"));
                    tv_hoursinnum.setText(response.getString("job_duration"));
                    tv_fare_rate.setText(response.getString("fare_rate_formatted"));
                    tv_total_billing.setText(response.getString("billed"));
                    tv_sercharge_ammount.setText(response.getString("service_charge"));
                    tv_gst_ammount.setText(response.getString("gst"));
                    tv_payable_amount.setText(response.getString("payable"));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error:" + error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload invoice. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        requestQueue.add(jsonObjectRequest);


    }

}
