package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.pragya.tym_tap.R;

public class TryAgainOTPActivity extends AppCompatActivity {

    Button btn_tryagain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try_again);
        btn_tryagain = (Button)findViewById(R.id.btn_tryagain);

        btn_tryagain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in_tryagain = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(in_tryagain);
                finish();
            }
        });
    }
}
