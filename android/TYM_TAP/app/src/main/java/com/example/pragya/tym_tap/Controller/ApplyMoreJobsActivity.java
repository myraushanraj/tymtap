package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.pragya.tym_tap.R;

public class ApplyMoreJobsActivity extends AppCompatActivity {

    TextView tv_congratulation;
    Button btn_more_jobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_more_jobs);

        tv_congratulation = findViewById(R.id.tv_congratulation);
        btn_more_jobs = findViewById(R.id.btn_more_jobs);
        btn_more_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent morejobs_intent = new Intent(getApplicationContext(), TymTapActivity.class);
                startActivity(morejobs_intent);
            }
        });

    }
}
