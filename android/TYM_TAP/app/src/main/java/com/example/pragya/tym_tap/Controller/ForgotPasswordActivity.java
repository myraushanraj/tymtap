package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends AppCompatActivity {

    Button btn_snd_email, btn_back_login;
    EditText edt_email;
    TextView tv_login;


    String send_otp;


    String url;

    //String url = "http://monkhub.com/tap-real/json/forgot-password-user.php?send_otp=akansha12@gmail.com";;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);



        initialize();
    }

    private void initialize() {

        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent backLogin_intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(backLogin_intent);
            }
        });

        edt_email  = (EditText)findViewById(R.id.edt_Verify_email) ;

        btn_snd_email=(Button)findViewById(R.id.btnSendEmail);

        btn_snd_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                send_otp = edt_email.getText().toString();
                if (send_otp.isEmpty()) {

                    // String send_otp=edt_email.getText().toString();

                    Toast.makeText(ForgotPasswordActivity.this, "Enter your email Id", Toast.LENGTH_SHORT).show();


                }else {

                    btnSubmit(send_otp);
                }


            }
        });
    }

    private void btnSubmit(String send_otp) {



        url = "http://monkhub.com/tap-real/json/forgot-password-user.php?send_otp="+send_otp;


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    if (response.getString("status").equals("success")){


                        Toast.makeText(getApplicationContext(), "OTP send to your email.",Toast.LENGTH_SHORT).show();

                        Intent in = new Intent(getApplicationContext(), SetpswrdOTPActivity.class);
                        in.putExtra("TextBox",edt_email.getText().toString());
                        startActivity(in);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error: ",Toast.LENGTH_SHORT).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to send OTP into your mail . Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("send_otp", edt_email.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }


}
