package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RedeemActivity extends AppCompatActivity {

    TextView tv_redeem;

    Button btn_ewallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);
        tv_redeem= findViewById(R.id.tv_redeem);
        btn_ewallet= findViewById(R.id.btn_ewallet);

        btn_ewallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wallet_intent = new Intent(getApplicationContext(),WalletActivity.class);
                startActivity(wallet_intent);
            }
        });

        try {
            redeemData();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void redeemData() throws JSONException {

        JSONObject jsonObject = new JSONObject(SharedPref.getString(RedeemActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        String url = "http://monkhub.com/tap-real/json/redeem_wallet.php?member=1731";

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {


                try {
                    if (response.getString("status").equals("success")){

                        tv_redeem.setText(response.getString("status_txt"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to redeem. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        requestQueue.add(jsonArrayRequest);
    }
}
