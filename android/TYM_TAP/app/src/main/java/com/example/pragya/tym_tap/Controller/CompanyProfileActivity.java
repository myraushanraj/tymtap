package com.example.pragya.tym_tap.Controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.example.pragya.tym_tap.tabs.feed.FeedAdaptor;
import com.example.pragya.tym_tap.tabs.feed.FeedModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CompanyProfileActivity extends AppCompatActivity {
    @BindView(R.id.tv_company_name)
    TextView tv_company_name;

    @BindView(R.id.tv_company_department)
    TextView tv_company_department;

    @BindView(R.id.tv_company_location)
    TextView tv_company_location;

    @BindView(R.id.tv_ratings)
    TextView tv_ratings;

    @BindView(R.id.tv_payment_status)
    TextView tv_payment_status;

    @BindView(R.id.tv_jobs_number)
    TextView tv_jobs_number;

    @BindView(R.id.tv_complete_number)
    TextView tv_complete_number;

    @BindView(R.id.tv_descripton)
    TextView tv_descripton;

    @BindView(R.id.iv_company_image)
    CircleImageView iv_company_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile);
        ButterKnife.bind(this);
        String company_id = getIntent().getStringExtra(AppConstant.INTENT_TAG.COMPANY_ID);
        try {
            loadCompanyProfile(company_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadCompanyProfile(String company_id) throws JSONException {
        JSONObject jsonObject = new JSONObject(SharedPref.getString(this, AppConstant.SF.LOGIN_RESPONSE));

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "http://monkhub.com/tap-real/json/company-data.php?id=" + company_id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                Log.i("eclipse", "Result response=" + response.toString());
                try {
                    tv_company_name.setText(response.getString("title"));
                    tv_company_department.setText(response.getString("category").trim());
                    tv_company_location.setText(response.getString("address"));
                    tv_ratings.setText(response.getString("rating"));
                    tv_jobs_number.setText(response.getString("total_count"));
                    tv_complete_number.setText(response.getString("paid_count"));
                    tv_descripton.setText(response.getString("about"));
                    Picasso.get().load(response.getString("image_url")).into(iv_company_image);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(CompanyProfileActivity.this, "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload company profile. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.i("eclipse", "Result1");
                return null;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

}
