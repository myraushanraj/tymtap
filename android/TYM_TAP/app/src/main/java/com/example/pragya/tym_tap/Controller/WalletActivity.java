package com.example.pragya.tym_tap.Controller;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletActivity extends AppCompatActivity {

    ImageView iv_wallet_back;

    Button yes, no;

    Dialog MyDialog;

    RecyclerView mList;

    LinearLayoutManager linearLayoutManager;
    DividerItemDecoration dividerItemDecoration;
    List<Wallet> movieList;
    RecyclerView.Adapter adapter;


    @BindView(R.id.tv_balance)
    TextView tv_balance;

    @BindView(R.id.tv_money_bank)
    TextView tv_money_bank;

    TextView tv_home;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

        mList = findViewById(R.id.rl_recyclerView);

        movieList = new ArrayList<>();
        iv_wallet_back = findViewById(R.id.iv_wallet_back);
        iv_wallet_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        tv_home = findViewById(R.id.tv_home);
        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent home_intent = new Intent(getApplicationContext(), TymTapActivity.class);
                startActivity(home_intent);
            }
        });

        adapter = new WalletAdapter(getApplicationContext(),movieList);
        tv_money_bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyCustomAlertDialog();

            }
        });




        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());



        mList.setHasFixedSize(true);
        mList.setLayoutManager(linearLayoutManager);
        mList.addItemDecoration(dividerItemDecoration);
        mList.setAdapter(adapter);


        try {
            load_data();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            loadwallet_data();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void MyCustomAlertDialog() {


        MyDialog = new Dialog(WalletActivity.this);
        MyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        MyDialog.setContentView(R.layout.customdialog);


        yes = (Button) MyDialog.findViewById(R.id.yes);
        no = (Button) MyDialog.findViewById(R.id.no);

        yes.setEnabled(true);
        no.setEnabled(true);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent redeem_intent = new Intent(getApplicationContext(),RedeemActivity.class);
                startActivity(redeem_intent);
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyDialog.cancel();
            }
        });

        MyDialog.show();
    }




    private void load_data() throws JSONException {


        JSONObject jsoObject = new JSONObject(SharedPref.getString(WalletActivity.this, AppConstant.SF.LOGIN_RESPONSE));


            String url = "http://monkhub.com/tap-real/json/my_wallet_transaction.php?id="+jsoObject.getJSONObject("0").getString("member_id");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        Wallet movie = new Wallet();

                        movie.setTimings(jsonObject.getString("timings"));
                        movie.setBalance(jsonObject.getString("balance"));
                        movie.setAmount(jsonObject.getString("amount"));
                        movie.setType1(jsonObject.getString("type1"));

                        //movie.setContent(jsonObject.getString("content"));
                        //movie.setTitle(jsonObject.getString("title"));


                        movieList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();


                        //progressDialog.dismiss();


                    }
                }
                adapter.notifyDataSetChanged();
                //progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Oops. Timeout error! Try Again", Toast.LENGTH_LONG).show();
                    }
                }
                //progressDialog.dismiss();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);


    }

    private void loadwallet_data() throws JSONException {
        JSONObject jso_Object = new JSONObject(SharedPref.getString(WalletActivity.this, AppConstant.SF.LOGIN_RESPONSE));


        String url = "http://monkhub.com/tap-real/json/my_wallet_transaction.php?id="+jso_Object.getJSONObject("0").getString("member_id");

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++){
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);
                        tv_balance.setText(jsonObject.getString("balance"));



                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload data. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
