package com.example.pragya.tym_tap.tabs.feed;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.Controller.ApplyQuesActivity;
import com.example.pragya.tym_tap.Controller.CallBack;
import com.example.pragya.tym_tap.Controller.DialogUtils;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedFragment extends Fragment implements CallBack {

    Context context;

    //ProgressDialog dialog;
    private List<FeedModel> feedMoelList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FeedAdaptor mAdapter;

    @BindView(R.id.tv_number_of_jobs_desc)
    TextView tv_number_of_jobs_desc;

    public FeedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //pDialog = new ProgressDialog(context);
        //pDialog.setMessage("Please wait...");
        //pDialog.setCancelable(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_feed, container, false);
        ButterKnife.bind(this, view);
        // Inflate the layout for this fragment
        recyclerView = view.findViewById(R.id.rv_feed_layout);
        mAdapter = new FeedAdaptor(getContext(), feedMoelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;


    }


    private void loadFeesData() throws JSONException {


        final ProgressDialog dialog= DialogUtils.showProgressDialog(getActivity(),"Loading...");

        JSONObject jsonObject = new JSONObject(SharedPref.getString(getActivity(), AppConstant.SF.LOGIN_RESPONSE));

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET, AppConstant.URLS.FEED_ROOT_URL
                + jsonObject.getJSONObject("0").getString("member_id"), null, new Response.Listener<JSONArray>() {

            @Override
            public void onResponse(JSONArray response) {

                Log.i("eclipse", "Result response=" + response.toString());
                if (dialog != null &&dialog.isShowing()) {
                    dialog.dismiss();
                }
                feedMoelList.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObj;
                    try {

                        jsonObj = (JSONObject) response.get(i);
                        FeedModel feedModel = new FeedModel();

                        feedModel.setId(jsonObj.getString("id"));
                        feedModel.setJob_id(jsonObj.getString("job_id"));
                        feedModel.setCourier(jsonObj.getString("courier"));
                        feedModel.setTitle(jsonObj.getString("title"));
                        feedModel.setDescription(jsonObj.getString("description"));
                        feedModel.setCompany(jsonObj.getString("company"));
                        feedModel.setCompany_id(jsonObj.getString("company_id"));
                        feedModel.setCompany_name(jsonObj.getString("company_name"));
                        feedModel.setCompany_logo(jsonObj.getString("company_logo"));
                        feedModel.setJob_seeker(jsonObj.getString("job_seeker"));
                        feedModel.setPosted_ago(jsonObj.getString("posted_ago"));
                        feedModel.setFare_rate(jsonObj.getString("fare_rate"));
                        feedModel.setImage_url(jsonObj.getString("image_url"));
                        //feedModel.setChecked_in_time_ago(jsonObj.getString("checked_in_time_ago"));
                        //feedModel.setChecked_out_time(jsonObj.getString("checked_out_time"));
                        //feedModel.setChecked_out_time_ago(jsonObj.getString("checked_out_time_ago"));
                        //feedModel.setCourier_process(jsonObj.getString("courier_process"));
                        //feedModel.setJob_time_ago(jsonObj.getString("job_time_ago"));
                        ///feedModel.setLast_seen(jsonObj.getString("last_seen"));

                        String duration=jsonObj.getString("job_start")+" to "+jsonObj.getString("job_end");
                        feedModel.setJob_start(duration);
                        //feedModel.setJob_end(jsonObj.getString("job_end"));
                        feedModel.setJob_status(jsonObj.getString("job_status"));
                        feedModel.setLocation(jsonObj.getString("location"));
                        feedModel.setCourier_alloted(jsonObj.getString("courier_alloted"));
                        feedModel.setJob_count(jsonObj.getString("job_count"));
                        feedModel.setAccept_count(jsonObj.getString("accept_count"));
                        feedModel.setReject_count(jsonObj.getString("reject_count"));
                        feedModel.setApply_count(jsonObj.getString("apply_count"));
                        feedModel.setRating(jsonObj.getString("rating"));
                        feedModel.setApplied(jsonObj.getString("applied"));

                        //Toast.makeText(getContext(), "before feed model", Toast.LENGTH_LONG).show();
// apply the logic for auser can apply or not



                        feedMoelList.add(feedModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }
                mAdapter = new FeedAdaptor(getContext(), feedMoelList);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                tv_number_of_jobs_desc.setText(" " + feedMoelList.size() + " ");
                //tv_number_of_jobs_desc.setText(feedMoelList.size());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //Toast.makeText(getContext(), "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getContext(),
                                "Oops. Timeout error! Try Again", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                dialog.dismiss();
                Log.i("eclipse", "Result1");
                return null;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    private void loadFeesDataObject() {

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, AppConstant.URLS.FEED_ROOT_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("eclipse", "Result response=" + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), "Error....", Toast.LENGTH_LONG).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.i("eclipse", "Result1");
                return null;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }

    @Override
    public void onCallBack() {
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /*@Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,new IntentFilter(
                "WHATEVER_ACTION"));
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                if (action.equals("WHATEVER_ACTION")) {
                    // Do
                    try {
                        loadFeesData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };*/
}

