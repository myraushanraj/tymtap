package com.example.pragya.tym_tap.Controller;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity {

    Toolbar tb_noti;
    TextView tv_notificationcount;


    String tag = NotificationActivity.class.getSimpleName();
    //a list to store all the products
    List<Product> productList;

    //the recyclerview
    RecyclerView recyclerView;

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        tb_noti = (Toolbar) findViewById(R.id.toolbar_notification);
        tv_notificationcount = (TextView)findViewById(R.id.notification_count);


        setSupportActionBar(tb_noti);
        getSupportActionBar().setTitle("Notification");
        tb_noti.setLogo(R.drawable.noti);

        tb_noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();


            }
        });


        recyclerView = findViewById(R.id.recylcerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        //initializing the product list
        productList = new ArrayList<>();

        onLoadNotification();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            onLoadNotification();
            handler.postDelayed(runnable, 10000);
        }
    };

    private void onLoadNotification() {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(SharedPref.getString(NotificationActivity.this, AppConstant.SF.LOGIN_RESPONSE));
            productList.clear();

            String url = "http://monkhub.com/tap-real/json/load_notification.php?show=" + jsonObject.getJSONObject("0").getString("member_id");

            final JsonArrayRequest stringRequest = new JsonArrayRequest(url,
                    new Response.Listener<JSONArray>() {
                        @Override
                        public void onResponse(JSONArray response) {
                            try {


                             int number = response.length();

                             tv_notificationcount.setText(String.valueOf(number));

                                for (int i = 0; i < response.length(); i++) {


                                    JSONObject product = response.getJSONObject(i);

                                    productList.add(new Product(
                                            product.getString("notification"),
                                            product.getString("timings"),
                                            product.getString("image_url")

                                    ));
                                }

                                //creating adapter object and setting it to recyclerview
                                ProductsAdapter adapter = new ProductsAdapter(NotificationActivity.this, productList);
                                recyclerView.setAdapter(adapter);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error.networkResponse == null){
                                if (error.getClass().equals(TimeoutError.class)){
                                    Toast.makeText(getApplicationContext(),
                                            "Failed to upload notification. Please try again later.", Toast.LENGTH_LONG).show();
                                }
                            }

                        }
                    });

            //adding our stringrequest to queue
            Volley.newRequestQueue(this).add(stringRequest);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(runnable, 10000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}