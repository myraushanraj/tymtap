package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SupportTextActivity extends AppCompatActivity {

    String httpURL = "http://monkhub.com/tap-real/json/new_support.php?member=";
    String url;

    Button btn_back;

    TextView tv_status_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support_text);

        btn_back= findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent back_intent = new Intent(getApplicationContext(), TymTapActivity.class);
                startActivity(back_intent);
            }
        });

        tv_status_text = findViewById(R.id.tv_status_text);
        try {
            getStatus_Text();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getStatus_Text()throws JSONException {

        Intent i = getIntent();
        final String subject = i.getStringExtra("subject");
        final String message = i.getStringExtra("message");

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JSONObject jsonObject = new JSONObject(SharedPref.getString(SupportTextActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        url = httpURL+jsonObject.getJSONObject("0").getString("member_id")+"&subject="+subject+"&message="+message;

        url=url.replace(" ", "%20");
        url=url.replace("\n", "%20");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")){

                        //Intent post_intent = new Intent(getApplicationContext(), SupportTextActivity.class);
                        //startActivity(post_intent);

                        tv_status_text.setText(response.getString("status_text"));

                        //Toast.makeText(getApplicationContext(),"Successfully Submited",Toast.LENGTH_LONG).show();

                    }else {
                        Toast.makeText(getApplicationContext(),"Data has not Submitted",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Error...."+error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });/*{
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("subject", sp_subject.getSelectedItem().toString().trim());
                params.put("message", edt_message.getText().toString().trim());
                //params.put("bank_location", edt_BankLocation.getText().toString().trim());
                //params.put("ifsc", edt_IFSCCode.getText().toString().trim());

                return params;
            }*/
        requestQueue.add(jsonObjectRequest);
        }
    }

