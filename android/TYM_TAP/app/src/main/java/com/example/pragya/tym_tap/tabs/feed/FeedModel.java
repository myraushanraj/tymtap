package com.example.pragya.tym_tap.tabs.feed;

import java.io.Serializable;

/**
 * Created by AJIT on 10-03-2018.
 */

public class FeedModel implements Serializable {
    String id;
    String job_id;
    String courier;
    String title;
    String description;
    String company;
    String company_id;
    String company_name;
    String company_logo;
    String job_seeker;
    String posted_ago;
    String fare_rate;
    String checked_in_time;
    String checked_in_time_ago;
    String checked_out_time;
    String checked_out_time_ago;
    String courier_process;
    String job_time_ago;
    String last_seen;
    String job_start;
    String job_end;
    String job_status;
    String location;
    String courier_alloted;
    String job_count;
    String accept_count;
    String reject_count;
    String apply_count;

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    String image_url;

    public String getApplied() {
        return applied;
    }

    public void setApplied(String applied) {
        this.applied = applied;
    }

    String applied;

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    String rating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getCourier() {
        return courier;
    }

    public void setCourier(String courier) {
        this.courier = courier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_logo() {
        return company_logo;
    }

    public void setCompany_logo(String company_logo) {
        this.company_logo = company_logo;
    }

    public String getJob_seeker() {
        return job_seeker;
    }

    public void setJob_seeker(String job_seeker) {
        this.job_seeker = job_seeker;
    }

    public String getPosted_ago() {
        return posted_ago;
    }

    public void setPosted_ago(String posted_ago) {
        this.posted_ago = posted_ago;
    }

    public String getFare_rate() {
        return fare_rate;
    }

    public void setFare_rate(String fare_rate) {
        this.fare_rate = fare_rate;
    }

    public String getChecked_in_time() {
        return checked_in_time;
    }

    public void setChecked_in_time(String checked_in_time) {
        this.checked_in_time = checked_in_time;
    }

    public String getChecked_in_time_ago() {
        return checked_in_time_ago;
    }

    public void setChecked_in_time_ago(String checked_in_time_ago) {
        this.checked_in_time_ago = checked_in_time_ago;
    }

    public String getChecked_out_time() {
        return checked_out_time;
    }

    public void setChecked_out_time(String checked_out_time) {
        this.checked_out_time = checked_out_time;
    }

    public String getChecked_out_time_ago() {
        return checked_out_time_ago;
    }

    public void setChecked_out_time_ago(String checked_out_time_ago) {
        this.checked_out_time_ago = checked_out_time_ago;
    }

    public String getCourier_process() {
        return courier_process;
    }

    public void setCourier_process(String courier_process) {
        this.courier_process = courier_process;
    }

    public String getJob_time_ago() {
        return job_time_ago;
    }

    public void setJob_time_ago(String job_time_ago) {
        this.job_time_ago = job_time_ago;
    }

    public String getLast_seen() {
        return last_seen;
    }

    public void setLast_seen(String last_seen) {
        this.last_seen = last_seen;
    }

    public String getJob_start() {
        return job_start;
    }

    public void setJob_start(String job_start) {
        this.job_start = job_start;
    }

    public String getJob_end() {
        return job_end;
    }

    public void setJob_end(String job_end) {
        this.job_end = job_end;
    }

    public String getJob_status() {
        return job_status;
    }

    public void setJob_status(String job_status) {
        this.job_status = job_status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCourier_alloted() {
        return courier_alloted;
    }

    public void setCourier_alloted(String courier_alloted) {
        this.courier_alloted = courier_alloted;
    }

    public String getJob_count() {
        return job_count;
    }

    public void setJob_count(String job_count) {
        this.job_count = job_count;
    }

    public String getAccept_count() {
        return accept_count;
    }

    public void setAccept_count(String accept_count) {
        this.accept_count = accept_count;
    }

    public String getReject_count() {
        return reject_count;
    }

    public void setReject_count(String reject_count) {
        this.reject_count = reject_count;
    }

    public String getApply_count() {
        return apply_count;
    }

    public void setApply_count(String apply_count) {
        this.apply_count = apply_count;
    }
}