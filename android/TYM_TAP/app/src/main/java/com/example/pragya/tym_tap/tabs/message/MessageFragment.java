package com.example.pragya.tym_tap.tabs.message;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.Controller.CallBack;
import com.example.pragya.tym_tap.Controller.DialogUtils;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.example.pragya.tym_tap.tabs.feed.FeedAdaptor;
import com.example.pragya.tym_tap.tabs.feed.FeedModel;
import com.example.pragya.tym_tap.tabs.message.MessageAdaptor;
import com.example.pragya.tym_tap.tabs.message.MessageModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MessageFragment extends Fragment implements CallBack {
    private List<MessageModel> messageModelList = new ArrayList<>();

    private RecyclerView recyclerView;
    private MessageAdaptor mAdapter;

    public MessageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        // Inflate the layout for this fragment
        recyclerView = view.findViewById(R.id.rv_message_layout);
        mAdapter = new MessageAdaptor(getContext(), messageModelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }




    @Override
    public void onResume() {
        super.onResume();
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadFeesData() throws JSONException {

        final ProgressDialog dialog= DialogUtils.showProgressDialog(getActivity(),"Loading...");

        JSONObject jsonObject = new JSONObject(SharedPref.getString(getActivity(), AppConstant.SF.LOGIN_RESPONSE));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        String url=AppConstant.URLS.MESSAGE_ROOT_URL + jsonObject.getJSONObject("0").getString("member_id");

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(Request.Method.GET,url , null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("eclipse", "Result response=" + response.toString());
                if (dialog != null &&dialog.isShowing()) {
                    dialog.dismiss();
                }
                messageModelList.clear();
                for (int i = 0; i < response.length(); i++) {
                    JSONObject jsonObj;
                    try {
                        jsonObj = (JSONObject) response.get(i);
                        MessageModel re = new MessageModel();
                        //re.setId(jsonObj.getString("id"));
                        re.setTimings(jsonObj.getString("timings"));
                        re.setCompany(jsonObj.getString("company"));
                        re.setContent(jsonObj.getString("content"));
                        re.setImage_url(jsonObj.getString("image_url"));
                        re.setCompany_id(jsonObj.getString("company_id"));
                        /*re.setApplied_at(jsonObj.getString("applied_at"));
                        re.setApplied_ago(jsonObj.getString("applied_ago"));
                        re.setJob_seeker(jsonObj.getString("job_seeker"));
                        re.setThread_id(jsonObj.getString("thread_id"));
                        re.setJob_title(jsonObj.getString("job_title"));
                        re.setJob_seeker_name(jsonObj.getString("job_seeker_name"));
                        re.setJob_seeker_about(jsonObj.getString("job_seeker_about"));
                        re.setJob_seeker_phone(jsonObj.getString("job_seeker_phone"));
                        re.setJob_seeker_profile_pic(jsonObj.getString("job_seeker_profile_pic"));
                        re.setCompany_name(jsonObj.getString("company_name"));
                        re.setJob_start_in(jsonObj.getString("job_start_in"));*/

                        messageModelList.add(re);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                }
                mAdapter = new MessageAdaptor(getContext(), messageModelList);
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                //Toast.makeText(getContext(), "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getContext(),
                                "Oops. Timeout error! Try Again", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                dialog.dismiss();
                Log.i("eclipse", "Result1");
                return null;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onCallBack() {
        try {
            loadFeesData();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
