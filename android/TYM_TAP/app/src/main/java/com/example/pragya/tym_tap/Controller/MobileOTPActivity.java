package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MobileOTPActivity extends AppCompatActivity {


    Button btn_snd_otp;
    EditText edt_digit_one;

    String verify_otp;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_otp);

        initialize();
    }

    public void initialize() {

        btn_snd_otp = (Button) findViewById(R.id.btn_OTPsubmit);

        edt_digit_one = (EditText) findViewById(R.id.edt_Otp1);

        btn_snd_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                verify_otp = edt_digit_one.getText().toString();


                if (verify_otp.isEmpty()) {
                    Toast.makeText(MobileOTPActivity.this, "Enter OTP", Toast.LENGTH_SHORT).show();

                } else {
                    try {
                        btnSubmit(verify_otp);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public void btnSubmit(String verify_otp) throws Exception {

        Intent i = getIntent();
        String phone = i.getStringExtra("TextPhone");
        final String email = i.getStringExtra("TextBox");
        final String password = i.getStringExtra("TextPassword");

        JSONObject jsonObject = new JSONObject(SharedPref.getString(MobileOTPActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        url = "http://monkhub.com/tap-real/json/verify_otp.php?phone=" + phone + "&user_otp=" + verify_otp;

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equals("success")) {

                        //Toast.makeText(getApplicationContext(), "OTP is correct", Toast.LENGTH_LONG).show();

                        Intent in_paswrd = new Intent(getApplicationContext(), VerifiedMobileActivity.class);
                        in_paswrd.putExtra("TextBox",email);
                        in_paswrd.putExtra("TextPassword",password);
                        startActivity(in_paswrd);
                        finish();


                        /*Intent inte = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(inte);
                        finish();*/

                    } else {

                        Toast.makeText(getApplicationContext(), "OTP is not correct", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: " + e, Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Enter correct OTP", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to verify OTP. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
                edt_digit_one.setText("");

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("verify_otp", edt_digit_one.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }


   /* public boolean validateOTP(String string){
        if (string.equals("")) {
            edt_digit_one.setError("Enter an OTP");
            return false;
        } else if (string.length() == 4) {
            edt_digit_one.setError("Maximum 4 Characters");
            return false;
        } else if (string.length() < 5) {
           edt_digit_one.setError("Minimum 1 Characters");
            return false;
        }
        //edt_digit_one.setErrorEnabled(false);
        return true;
    }*/
}
