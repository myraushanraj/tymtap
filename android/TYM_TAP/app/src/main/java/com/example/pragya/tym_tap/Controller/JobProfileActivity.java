package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class JobProfileActivity extends AppCompatActivity {


    Button btn_applynow;

    @BindView(R.id.tv_jobtitle)
    TextView tv_job_title;



    @BindView(R.id.tv_job_start)
    TextView tv_job_start;

    @BindView(R.id.tv_fair_rate)
    TextView tv_fair_rate;

    @BindView(R.id.tv_location)
    TextView tv_location;

    @BindView(R.id.tv_comapnyname)
    TextView tv_company_name;

    @BindView(R.id.tv_department)
    TextView tv_department;

    @BindView(R.id.tv_complocation)
    TextView tv_complocation;

    @BindView(R.id.tv_comprating)
    TextView tv_comprating;

    @BindView(R.id.tv_payment_verified)
    TextView tv_payment_verified;

    @BindView(R.id.tv_jobdetails)
    TextView tv_jobdetails;

    @BindView(R.id.tv_skills)
    TextView tv_skills;

    @BindView(R.id.tv_innumbers)
    TextView tv_innumbers;

    @BindView(R.id.tv_jobnumbers)
    TextView tv_jobnumbers;

    @BindView(R.id.iv_companyimage)
    CircleImageView iv_companyimage;

    String id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_profile);
        //btn_applynow= findViewById(R.id.btn_applynow);
        /*btn_applynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent apply_intent = new Intent(getApplicationContext(),ApplyQuesActivity.class);
                apply_intent.putExtra("JobId",id);
                startActivity(apply_intent);
            }
        });*/

        ButterKnife.bind(this);
        String job_id = getIntent().getStringExtra(AppConstant.INTENT_TAG.JOB_ID);
        try {
            loadJobProfile(job_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadJobProfile(String job_id)throws JSONException {


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        String url = "http://monkhub.com/tap-real/json/job_detail.php?id="+job_id;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i("eclipse", "Result response=" + response.toString());
                        try {

                            tv_job_title.setText(response.getString("title"));
                            tv_job_start.setText(response.getString("job_start"));
                            tv_fair_rate.setText(response.getString("fare_rate"));
                            tv_location.setText(response.getString("location"));
                            tv_company_name.setText(response.getString("company_name"));
                            tv_innumbers.setText(response.getString("job_count"));
                            tv_complocation.setText(response.getString("location"));
                            tv_department.setText(response.getString("company_category"));
                            tv_comprating.setText(response.getString("company_rating"));
                            if (response.getString("payment_verified").equals("1")){
                                tv_payment_verified.setText("Payment Verified");
                            }else{
                                tv_payment_verified.setText("Payment not Verified");
                            }
                            //tv_payment_verified.setText(response.getString("payment_verified"));
                            tv_jobdetails.setText(response.getString("description"));
                            tv_skills.setText(response.getString("skill"));
                            tv_jobnumbers.setText(response.getString("job_duration"));
                            Picasso.get().load(response.getString("image_url")).into(iv_companyimage);

                            id = response.getString("id");



                            //tv_company_name.setText(response.getString("title"));
                            //tv_company_department.setText(response.getString("category").trim());
                            //tv_company_location.setText(response.getString("address"));
                            //tv_ratings.setText(response.getString("rating"));
                            //tv_jobs_number.setText(response.getString("total_count"));
                            //tv_complete_number.setText(response.getString("paid_count"));
                            //tv_descripton.setText(response.getString("about"));
                            //Picasso.get().load(response.getString("image_url")).into(iv_company_image);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(JobProfileActivity.this, "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload job profile. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Log.i("eclipse", "Result1");
                return null;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

}
