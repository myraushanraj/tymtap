package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NumberVerifyActivity extends AppCompatActivity {

    Button btn_sendcode;
    EditText edt_number;



    String phoneno;


    String url;

    //String url = "http://monkhub.com/tap-real/json/forgot-password-user.php?send_otp=akansha12@gmail.com";;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number_verify);

        initialize();
    }

    private void initialize() {

        edt_number = (EditText) findViewById(R.id.edtNumber);

        btn_sendcode = (Button) findViewById(R.id.btnSendCode);

        btn_sendcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phoneno = edt_number.getText().toString();
                if (phoneno.isEmpty()) {

                    // String send_otp=edt_email.getText().toString();

                    Toast.makeText(NumberVerifyActivity.this, "Enter your phone no", Toast.LENGTH_SHORT).show();


                } else if (validateMobile(phoneno)) {

                    btnSubmit(phoneno);
                }


            }
        });
    }

    private void btnSubmit(final String phoneno) {

        Intent i = getIntent();
        final String email = i.getStringExtra("TextBox");
        final String password = i.getStringExtra("TextPassword");




        url = "http://monkhub.com/tap-real/json/send-otp-phone.php?phone=" + phoneno;


        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    if (response.getString("status").equals("success")) {

                        SharedPref.putString(NumberVerifyActivity.this, AppConstant.SF.LOGIN_RESPONSE,response.toString());


                        //Toast.makeText(getApplicationContext(), "OTP send to your mobile number.", Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(getApplicationContext(), MobileOTPActivity.class);
                        in.putExtra("TextBox",email);
                        in.putExtra("TextPassword",password);
                        in.putExtra("TextPhone", phoneno);
                        startActivity(in);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: " + e, Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error: ", Toast.LENGTH_SHORT).show();

                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to send OTP. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put("phone", edt_number.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    private boolean validateMobile(String string) {

        if (string.length() != 10) {
            edt_number.setError("Enter A Valid Mobile Number");
            return false;
        }
        //edt_number.setErrorEnabled(false);
        return true;
    }


}
