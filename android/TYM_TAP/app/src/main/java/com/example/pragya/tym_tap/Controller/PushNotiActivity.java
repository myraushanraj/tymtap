package com.example.pragya.tym_tap.Controller;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.pragya.tym_tap.R;
import com.google.firebase.iid.FirebaseInstanceId;

public class PushNotiActivity extends AppCompatActivity {

    EditText edt_GetToken;
    //Button btn_GetToken;   l                                     ,                            l


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_noti);

        edt_GetToken = (EditText)findViewById(R.id.edtgetToken);

        edt_GetToken.setText(FirebaseInstanceId.getInstance().getToken());

        Log.d("Refresh Token: ","id: "+FirebaseInstanceId.getInstance().getToken());

    }
}