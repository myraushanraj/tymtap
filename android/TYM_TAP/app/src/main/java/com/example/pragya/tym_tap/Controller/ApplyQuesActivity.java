package com.example.pragya.tym_tap.Controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.example.pragya.tym_tap.dialog.JobApplyDialogFragment;
import com.example.pragya.tym_tap.tabs.feed.FeedModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ApplyQuesActivity extends AppCompatActivity {

    //Button job_apply_btn;


    static String TAG = ApplyQuesActivity.class.getSimpleName();
    FeedModel feedModel;

    ProgressDialog pDialog;

    TextView tv_1, tv_2, tv_3,tv_4,tv_5,tv_6,tv_7,tv_8,tv_9,tv_10;
    EditText edt_1, edt_2, edt_3,edt_4,edt_5,edt_6,edt_7,edt_8,edt_9,edt_10;

    // temporary string to show the parsed response
    String jsonResponse, jsRes, jsoRes,jsoRes4,jsoRes5,jsoRes6,jsoRes7,jsoRes8,jsoRes9,jsoRes10,
            edt1_jsonResponse, edt2_jsRes, edt3_jsoRes, edt4_jso,edt5_jso,edt6_jso,edt7_jso,edt8_jso,edt9_jso,edt10_jso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_ques);
        feedModel = (FeedModel) getIntent().getSerializableExtra("Feed_Data");
        tv_1 = findViewById(R.id.txt_1);
        tv_2 = findViewById(R.id.txt_2);
        tv_3 = findViewById(R.id.txt_3);
        tv_4 = findViewById(R.id.txt_4);
        tv_5 = findViewById(R.id.txt_5);
        tv_6 = findViewById(R.id.txt_6);
        tv_7 = findViewById(R.id.txt_7);
        tv_8 = findViewById(R.id.txt_8);
        tv_9 = findViewById(R.id.txt_9);
        tv_10 = findViewById(R.id.txt_10);



        /*job_apply_btn = findViewById(R.id.job_apply_btn);

        job_apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in_apply = new Intent(getApplicationContext(), TymTapActivity.class);
                startActivity(in_apply);
            }
        });*/


        edt_1 = findViewById(R.id.edt_1);
        edt_2 = findViewById(R.id.edt_2);
        edt_3 = findViewById(R.id.edt_3);
        edt_4 = findViewById(R.id.edt_4);
        edt_5 = findViewById(R.id.edt_5);
        edt_6 = findViewById(R.id.edt_6);
        edt_7 = findViewById(R.id.edt_7);
        edt_8 = findViewById(R.id.edt_8);
        edt_9 = findViewById(R.id.edt_9);
        edt_10 = findViewById(R.id.edt_10);


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        final Button button = findViewById(R.id.job_apply_btn);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                button.setVisibility(View.INVISIBLE);

                edt1_jsonResponse=edt_1.getText().toString();
                edt2_jsRes=edt_2.getText().toString();
                edt3_jsoRes=edt_3.getText().toString();
                edt4_jso=edt_4.getText().toString();
                edt5_jso=edt_5.getText().toString();
                edt6_jso=edt_6.getText().toString();
                edt7_jso=edt_7.getText().toString();
                edt8_jso=edt_8.getText().toString();
                edt9_jso=edt_9.getText().toString();
                edt10_jso=edt_10.getText().toString();

                String QA = edt1_jsonResponse + "$$$" + edt2_jsRes
                        +"$$$" + edt3_jsoRes+ "$$$" + edt4_jso +"$$$" +edt5_jso +"$$$" +edt6_jso +"$$$" +edt7_jso +"$$$" +edt8_jso +"$$$" +
                        edt9_jso +"$$$" +edt10_jso;
                try {
                    applyJobsAPI(QA);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                makeJsonObjectRequest();
            }
        });

    }

    private void applyJobsAPI(String QA) throws JSONException {
        JSONObject jsonObject = new JSONObject(SharedPref.getString(ApplyQuesActivity.this, AppConstant.SF.LOGIN_RESPONSE));
        String url = "http://monkhub.com/tap-real/json/job_seeker_answers_interview.php?member=" + jsonObject.getJSONObject("0").getString("member_id")
                + "&thread_id=" + feedModel.getId() + "&answer=" + QA;
        url=url.replace(" ", "%20");
        url=url.replace("\n", "%20");
        //Toast.makeText(getApplicationContext(), "URL: "+url, Toast.LENGTH_SHORT).show();
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JobApplyDialogFragment jobApplyDialogFragment = new JobApplyDialogFragment();
                Bundle bundle = new Bundle();
                try {
                    bundle.putString("Title", (String) response.get("status_text"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                jobApplyDialogFragment.setArguments(bundle);
                jobApplyDialogFragment.show(getFragmentManager(), "Job Apply Dialog");
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error: "+error, Toast.LENGTH_SHORT).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload the data. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("QA",edt_1.getText().toString().trim()+edt_2.getText().toString().trim()+edt_3.getText().toString().trim()+
                        edt_4.getText().toString().trim()+edt_5.getText().toString().trim()+edt_6.getText().toString().trim()+
                        edt_7.getText().toString().trim()+edt_8.getText().toString().trim()+edt_9.getText().toString().trim()+
                        edt_10.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }

    public void makeJsonObjectRequest() {
        //Intent i = getIntent();

        //final String jobId = i.getStringExtra("JobId");
        showpDialog();
        String urlJsonObj = "http://monkhub.com/tap-real/json/load_interview_question_list.php?thread=" + feedModel.getId();
        urlJsonObj=urlJsonObj.replace(" ", "%20");

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    String query1 = response.getString("query1");
                    String query2 = response.getString("query2");
                    String query3 = response.getString("query3");
                    String query4 = response.getString("query4");
                    String query5 = response.getString("query5");
                    String query6 = response.getString("query6");
                    String query7 = response.getString("query7");
                    String query8 = response.getString("query8");
                    String query9 = response.getString("query9");
                    String query10 = response.getString("query10");
                    //String query5 = response.getString("query5");




                    jsonResponse = "";
                    jsonResponse += query1 ;

                    jsRes = "";
                    jsRes += query2 ;

                    jsoRes = "";
                    jsoRes += query3 ;

                    jsoRes4 = "";
                    jsoRes4 += query4 ;

                    jsoRes5 = "";
                    jsoRes5 += query5 ;

                    jsoRes6 = "";
                    jsoRes6 += query6 ;

                    jsoRes7 = "";
                    jsoRes7 += query7 ;

                    jsoRes8 = "";
                    jsoRes8 += query8 ;

                    jsoRes9 = "";
                    jsoRes9 += query9 ;

                    jsoRes10 = "";
                    jsoRes10 += query10 ;


                    /*if ((tv_1.getText().length()>8)){

                    }*/
                    if (response.getString("query1").equals(" ")) {

                        edt_1.setVisibility(View.INVISIBLE);
                    } else {

                        tv_1.setText(jsonResponse);
                        edt_1.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query2").equals(" ")) {

                        edt_2.setVisibility(View.INVISIBLE);
                    } else {

                        tv_2.setText(jsRes);
                        edt_2.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query3").equals(" ")) {

                        edt_3.setVisibility(View.INVISIBLE);
                    } else {

                        tv_3.setText(jsoRes);
                        edt_3.setVisibility(View.VISIBLE);

                    }


                    if (response.getString("query4").equals(" ")) {

                        edt_4.setVisibility(View.INVISIBLE);
                    } else {

                        tv_4.setText(jsoRes4);
                        edt_4.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query5").equals(" ")) {

                        edt_5.setVisibility(View.INVISIBLE);
                    } else {

                        tv_5.setText(jsoRes5);
                        edt_5.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query6").equals(" ")) {

                        edt_6.setVisibility(View.INVISIBLE);
                    } else {

                        tv_6.setText(jsoRes6);
                        edt_6.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query7").equals(" ")) {

                        edt_7.setVisibility(View.INVISIBLE);
                    } else {

                        tv_7.setText(jsoRes7);
                        edt_7.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query8").equals(" ")) {

                        edt_8.setVisibility(View.INVISIBLE);
                    } else {

                        tv_8.setText(jsoRes8);
                        edt_8.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query9").equals(" ")) {

                        edt_9.setVisibility(View.INVISIBLE);
                    } else {

                        tv_9.setText(jsoRes9);
                        edt_9.setVisibility(View.VISIBLE);

                    }

                    if (response.getString("query10").equals(" ")) {

                        edt_10.setVisibility(View.INVISIBLE);
                    } else {

                        tv_10.setText(jsoRes10);
                        edt_10.setVisibility(View.VISIBLE);

                    }





                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload the data. Please try again.", Toast.LENGTH_LONG).show();
                    }
                }
                // hide the progress dialog
                hidepDialog();
            }
        });

        // Adding request to request queue
        ApppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}

