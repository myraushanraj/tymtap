package com.example.pragya.tym_tap.tabs.feed;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.pragya.tym_tap.Controller.ApplyQuesActivity;
import com.example.pragya.tym_tap.Controller.CompanyProfileActivity;
import com.example.pragya.tym_tap.Controller.JobProfileActivity;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.squareup.picasso.Picasso;


import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AJIT on 10-03-2018.
 */

public class FeedAdaptor extends RecyclerView.Adapter<FeedAdaptor.MyViewHolder> {

    private List<FeedModel> feedModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_job_title, tv_company_name, tv_posted_ago, tv_job_start, tv_fair_rate, tv_location;
        public RatingBar rb_job_rating;
        public CircleImageView iv_jobs;
        public Button btn_jon_applied;

        public MyViewHolder(View view) {
            super(view);
            btn_jon_applied = (Button) view.findViewById(R.id.btn_jon_applied);
            btn_jon_applied.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //btn_jon_applied.setVisibility(View.INVISIBLE);
                    //if (feedModelList.getApplied().equalsIgnoreCase("0"))
                    //btn_jon_applied.setText(AppConstant.JOB_BTN.APPLIED);
                    //btn_jon_applied.setClickable(false);
                    Intent intent = new Intent(context, ApplyQuesActivity.class);
                    intent.putExtra("Feed_Data", feedModelList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });
            iv_jobs = view.findViewById(R.id.iv_jobs);
            rb_job_rating = view.findViewById(R.id.rb_job_rating);
            tv_job_title = view.findViewById(R.id.tv_job_title);
            tv_job_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, JobProfileActivity.class);
                    intent.putExtra(AppConstant.INTENT_TAG.JOB_ID, feedModelList.get(getAdapterPosition()).getId());
                    context.startActivity(intent);

                }
            });
            tv_company_name = view.findViewById(R.id.tv_company_name);
            tv_company_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CompanyProfileActivity.class);
                    intent.putExtra(AppConstant.INTENT_TAG.COMPANY_ID, feedModelList.get(getAdapterPosition()).getCompany_id());
                    context.startActivity(intent);
                }
            });
            tv_posted_ago = view.findViewById(R.id.tv_posted_ago);
            tv_job_start = view.findViewById(R.id.tv_job_start);
            tv_fair_rate = view.findViewById(R.id.tv_fair_rate);
            tv_location = view.findViewById(R.id.tv_location);
        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_feed_item, parent, false));
    }

    private Context context;

    public FeedAdaptor(Context context, List<FeedModel> moviesList) {
        this.context = context;
        this.feedModelList = moviesList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FeedModel feedModel = feedModelList.get(position);
        Picasso.get().load(feedModel.getImage_url()).into(holder.iv_jobs);
        holder.rb_job_rating.setRating(Float.parseFloat(feedModel.getRating()));
        holder.tv_job_title.setText(feedModel.getTitle());
        holder.tv_company_name.setText(feedModel.getCompany_name());
        holder.tv_posted_ago.setText(feedModel.getPosted_ago());
        holder.tv_job_start.setText(feedModel.getJob_start());
        holder.tv_fair_rate.setText(feedModel.getFare_rate());
        holder.tv_location.setText(feedModel.getLocation());

        /*if (requestMode.getJob_status().equalsIgnoreCase("accepted")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.CHECK_IN);
            holder.btn_request_tab.setClickable(true);*/

        if (feedModel.getApplied().equalsIgnoreCase("1")) {
           holder.btn_jon_applied.setText(AppConstant.JOB_BTN.APPLIED);
            holder.btn_jon_applied.setBackgroundColor(Color.parseColor("#E84C3D"));
           holder.btn_jon_applied.setClickable(false);
        }
        }

        @Override
        public int getItemCount () {
            return feedModelList.size();
        }
}