package com.example.pragya.tym_tap.Controller;

/**
 * Created by Pragya on 3/27/2018.
 */

public class Movie {

    public String content;


    public Movie() {

    }

    public Movie(String content) {
        this.content = content;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
