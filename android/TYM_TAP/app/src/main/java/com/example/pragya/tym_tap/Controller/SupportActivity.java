package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SupportActivity extends AppCompatActivity {

    Button btn_post;

    EditText edt_message;

    String message, subject;

    String httpURL = "http://monkhub.com/tap-real/json/new_support.php?member=";

    private Spinner sp_subject;

    String url;
    //private static final String[]paths = {"item 1", "item 2", "item 3"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        edt_message = findViewById(R.id.edt_message);
        btn_post = findViewById(R.id.btn_post);

        sp_subject = findViewById(R.id.sp_dropdown);
        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(SupportActivity.this,
                android.R.layout.simple_list_item_1,getResources().getStringArray(R.array.dropdown_items));

        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_subject.setAdapter(myAdapter);
        //sp_dropdown.setOnItemSelectedListener(this);

        btn_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subject = sp_subject.getSelectedItem().toString();
                message = edt_message.getText().toString();

                try {
                    getPost(subject,message);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void getPost(final String subject,final  String message) throws JSONException {

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JSONObject jsonObject = new JSONObject(SharedPref.getString(SupportActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        url = httpURL+jsonObject.getJSONObject("0").getString("member_id")+"&subject="+subject+"&message="+message;

        url=url.replace(" ", "%20");
        url=url.replace("\n", "%20");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")){

                        Intent post_intent = new Intent(getApplicationContext(), SupportTextActivity.class);
                        post_intent.putExtra("subject",sp_subject.getSelectedItem().toString());
                        post_intent.putExtra("message",edt_message.getText().toString());
                        startActivity(post_intent);

                        Toast.makeText(getApplicationContext(),"Successfully Submited",Toast.LENGTH_LONG).show();

                    }else {
                        Toast.makeText(getApplicationContext(),"Data has not Submitted",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to send queries. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("subject", sp_subject.getSelectedItem().toString().trim());
                params.put("message", edt_message.getText().toString().trim());
                //params.put("bank_location", edt_BankLocation.getText().toString().trim());
                //params.put("ifsc", edt_IFSCCode.getText().toString().trim());

                return params;
            }
        };

        requestQueue.add(jsonObjectRequest);


    }
}
