package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    Button btn_submit;
    EditText edt_Name, edt_EmailId, edt_Password, edt_ConPassword;


    TextView tv_signIn;
    //RequestQueue requestQueue;

    String name, email, password, token;

    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        initialize();
    }

    private void initialize() {

        tv_signIn = findViewById(R.id.tv_signIn);
        tv_signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signIn_intent= new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(signIn_intent);
            }
        });

        edt_Name = (EditText) findViewById(R.id.edtName);
        edt_Password = (EditText) findViewById(R.id.edtPassword);
        edt_ConPassword = (EditText) findViewById(R.id.edtConfirm_Password);
        //edt_PhoneNo = (EditText) findViewById(R.id.edtPhone_Num);
        edt_EmailId = (EditText) findViewById(R.id.edtEmailId);
        btn_submit = (Button) findViewById(R.id.btnSubmit);
        //tv_push_token = (TextView)findViewById(R.id.tv_push_token);


        //tv_push_token.setText(FirebaseInstanceId.getInstance().getToken());

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = edt_Name.getText().toString();
                password = edt_Password.getText().toString();
                email = edt_EmailId.getText().toString();
                //String conpassword = edt_ConPassword.getText().toString();
                //phone = edt_PhoneNo.getText().toString();
                token = FirebaseInstanceId.getInstance().getToken();

                if(name.isEmpty()|| password.isEmpty() || email.isEmpty() ){
                    Toast.makeText(RegisterActivity.this, "fill all details", Toast.LENGTH_SHORT).show();

                }else
                {
                    btnSubmit(name,password,email);
                }
            }
        });
    }

    public void btnSubmit(final String name, final String password, final String email) {



        url = "http://monkhub.com/tap-real/json/register.php?email="+email+"&password="+password+"&name="+name+"&apk_token="+token;
        url=url.replace(" ", "%20");
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equals("success")){

                        //Toast.makeText(getApplicationContext(),"Data successfully registered now you can login to your account",Toast.LENGTH_LONG).show();
                        Intent inte = new Intent(getApplicationContext(), NumberVerifyActivity.class);
                        inte.putExtra("TextBox",email);
                        inte.putExtra("TextPassword", password);
                        startActivity(inte);
                        finish();

                    }else {

                        Toast.makeText(getApplicationContext(),"Data is not Inserted",Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error....", Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to register. Please try again.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("email", edt_EmailId.getText().toString().trim());
                params.put("password", edt_Password.getText().toString().trim());
                params.put("name", edt_Name.getText().toString().trim());
                //params.put("phone", edt_PhoneNo.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
