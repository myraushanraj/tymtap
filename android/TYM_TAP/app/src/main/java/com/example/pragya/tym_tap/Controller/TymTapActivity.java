package com.example.pragya.tym_tap.Controller;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenuItemView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.example.pragya.tym_tap.tabs.feed.FeedFragment;
import com.example.pragya.tym_tap.tabs.request.RequestFragment;
import com.example.pragya.tym_tap.tabs.message.MessageFragment;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TymTapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent category_in = new Intent(getApplicationContext(), CategoriesActivity.class);
                startActivity(category_in);
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);


        toggle.syncState();
        try {
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            View headerview = navigationView.getHeaderView(0);
            JSONObject jsonObject = new JSONObject(SharedPref.getString(this, AppConstant.SF.LOGIN_RESPONSE));
            TextView tv_user_name = (TextView)headerview. findViewById(R.id.user_name);
            tv_user_name.setText(jsonObject.getJSONObject("0").getString("name"));
            TextView tv_user_email = (TextView) headerview.findViewById(R.id.tv_user_email);
            tv_user_email.setText(jsonObject.getJSONObject("0").getString("email"));
            navigationView.setNavigationItemSelectedListener(this);


            Menu m = navigationView.getMenu();
            MenuItem menuItem = m.findItem(R.id.nav_support);

            SpannableString s = new SpannableString(menuItem.getTitle());
            s.setSpan(new ForegroundColorSpan(Color.parseColor("#ea2311")), 0, s.length(), 0);
            menuItem.setTitle(s);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(SharedPref.getString(TymTapActivity.this, AppConstant.SF.LOGIN_RESPONSE));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/




        //LinearLayout header = (LinearLayout) headerview.findViewById(R.id.header);

       /* try {
            tv_user_name.setText(jsonObject.getJSONObject("0").getString("member_id"));
            tv_user_email.setText(jsonObject.getJSONObject("0").getString("email"));

        } catch (JSONException e) {
            e.printStackTrace();
        }*/


        //navigationView.setNavigationItemSelectedListener(this);

            //View headerView = navigationView.getHeaderView(0);
            //TextView tv_email = (TextView)findViewById(R.id.tv_user_email);

            //TextView tv_name = (TextView)findViewById(R.id.user_name);

        /*JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(SharedPref.getString(TymTapActivity.this, AppConstant.SF.LOGIN_RESPONSE));

            tv_email.setText(jsonObject.getJSONObject("0").getString("email"));
            tv_name.setText(jsonObject.getJSONObject("0").getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

            viewPager = (ViewPager) findViewById(R.id.viewpager);
            setupViewPager(viewPager);

            tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(viewPager);
        try {
            Log.d("Refresh Token: ", "id: " + FirebaseInstanceId.getInstance().getToken());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FeedFragment(), "FEED");
        adapter.addFragment(new MessageFragment(), "MESSAGE");
        adapter.addFragment(new RequestFragment(), "REQUEST");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //drawer.setDrawerShadow(R.drawable);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);

        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.nav_support)
            {

                //TypefaceSpan face = new TypefaceSpan("bold");
                applyFontToMenuItem(item);
                SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
                int end = spanString.length();
                spanString.setSpan(new RelativeSizeSpan(1.5f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                //spanString.setSpan(face, 0, item.length(), 0);
                item.setTitle(spanString);
            }
        }
        return true;
    }

    private void applyFontToMenuItem(MenuItem item) {

        Typeface font = Typeface.createFromAsset(getAssets(), "ds_digi_b.TTF");
        SpannableString mNewTitle = new SpannableString(item.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        //mNewTitle.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, mNewTitle.length(), 0); Use this if you want to center the items
        item.setTitle(mNewTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_refresh){
            //sendBroadcast(new Intent().setAction("WHATEVER_ACTION"));
           // FragmentTransaction ft = getFragmentManager().beginTransaction();
            //ft.detach(this).attach(this).commit();
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notification) {
            Intent intent = new Intent(TymTapActivity.this, NotificationActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_profile) {
            Intent intent = new Intent(TymTapActivity.this, ProfileDetailsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.change_profile_pic) {

            Intent intent_profile = new Intent(TymTapActivity.this, ProfileActivity.class);
            startActivity(intent_profile);
            return true;


        }  else if (id == R.id.payment_settings) {

            Intent intent_payment = new Intent(TymTapActivity.this, BankDetailsActivity.class);
            startActivity(intent_payment);
            return true;

        } else if (id == R.id.wallet_transaction) {

            Intent wallet_intent = new Intent(TymTapActivity.this, WalletActivity.class);
            startActivity(wallet_intent);
            return true;


        } else if (id == R.id.nav_support) {

            Intent intent_support = new Intent(TymTapActivity.this, SupportActivity.class);
            startActivity(intent_support);
            return true;

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.faq) {

        } else if (id == R.id.nav_logout) {
            SharedPref.putString(this, AppConstant.SF.LOGIN_RESPONSE, "");
            Intent intent=new Intent(this,SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
