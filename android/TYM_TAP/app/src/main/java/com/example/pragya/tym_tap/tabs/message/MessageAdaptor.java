package com.example.pragya.tym_tap.tabs.message;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import android.widget.TextView;

import com.example.pragya.tym_tap.Controller.ChatActivity;
import com.example.pragya.tym_tap.Controller.TymTapActivity;
import com.example.pragya.tym_tap.R;

import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AJIT on 10-03-2018.
 */

public class MessageAdaptor extends RecyclerView.Adapter<MessageAdaptor.MyViewHolder> {


    private List<MessageModel> messageModelsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView company, timings, message;
        Button btn_chat_now;
        public CircleImageView messageItemImage;

        public MyViewHolder(View view) {
            super(view);

              company = (TextView) view.findViewById(R.id.tv_company);
              timings = (TextView) view.findViewById(R.id.tv_timings);
              message = (TextView) view.findViewById(R.id.tv_message);
              messageItemImage = (CircleImageView) view.findViewById(R.id.iv_message_adaptor);
              btn_chat_now = (Button)view.findViewById(R.id.btn_chat_now);
              btn_chat_now.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      //JSONObject jsonObject = new JSONObject(SharedPref.getString(, AppConstant.SF.LOGIN_RESPONSE));
                      Intent chat_now_intent = new Intent(context, ChatActivity.class);
                      //chat_now_intent.putExtra("url", "http://monkhub.com/chat-box/my-chat-small.php?member=4593&owner=C4821266");
                      chat_now_intent.putExtra("Company_id",messageModelsList.get(getAdapterPosition()).getCompany_id());
                      context.startActivity(chat_now_intent);
                  }
              });
        }
    }

    Context context;

    MessageAdaptor(Context context, List<MessageModel> messageModelsList) {
        this.context = context;
        this.messageModelsList = messageModelsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_message_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessageModel messageModel = messageModelsList.get(position);
        holder.company.setText(messageModel.getCompany());
        holder.timings.setText(messageModel.getTimings());
        holder.message.setText(messageModel.getContent());
        Picasso.get().load(messageModel.getImage_url()).into(holder.messageItemImage);
//        holder.genre.setText(movie.getGenre());
//        holder.year.setText(movie.getYear());
    }

    @Override
    public int getItemCount() {
        return messageModelsList.size();
    }
}