package com.example.pragya.tym_tap.Controller;

/**
 * Created by Pragya on 3/19/2018.
 */

public class Product {

    private String notification, timings, image_url;

    public String getImage_url() {
        return image_url;
    }

    public Product(String notification, String timings, String image_url) {
        this.notification = notification;
        this.timings = timings;
        this.image_url=image_url;

    }

    public String getNotification() {
        return notification;
    }

    public String getTimings() {
        return timings;
    }
}

