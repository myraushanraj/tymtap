package com.example.pragya.tym_tap.Controller;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class BankDetailsActivity extends AppCompatActivity {

    String TAG = BankDetailsActivity.class.getSimpleName();

    ProgressDialog pDialog;


    EditText edt_BankName, edt_BankAcct, edt_BankLocation, edt_IFSCCode;

    Button btn_Submit;

    String url;

    String bankname, bankacct, banklocation, ifsccode;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);

        edt_BankName = (EditText)findViewById(R.id.edtBankName);
        edt_BankAcct = (EditText)findViewById(R.id.edtBankAcct);
        edt_BankLocation = (EditText)findViewById(R.id.edtBankLocation);
        edt_IFSCCode = (EditText)findViewById(R.id.edtIFSCCode);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);

        btn_Submit = (Button)findViewById(R.id.btnBankSubmit);

        try {
            loadbankdetails();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bankname = edt_BankName.getText().toString();
                bankacct = edt_BankAcct.getText().toString();
                banklocation = edt_BankLocation.getText().toString();
                ifsccode= edt_IFSCCode.getText().toString();


                if(bankname.isEmpty()|| bankacct.isEmpty() || banklocation.isEmpty() || ifsccode.isEmpty()){
                    Toast.makeText(BankDetailsActivity.this, "fill all details", Toast.LENGTH_SHORT).show();

                }else
                {
                    try {
                        btnSubmit(bankname,bankacct,banklocation,ifsccode);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });


    }

    private void loadbankdetails() throws JSONException {

        JSONObject jsonObject = new JSONObject(SharedPref.getString(BankDetailsActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        String urlJsonObj = "http://monkhub.com/tap-real/json/load_bank_detail.php?member="+jsonObject.getJSONObject("0").getString("member_id");

        urlJsonObj=urlJsonObj.replace(" ", "%20");
        showpDialog();

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                urlJsonObj, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());

                try {
                    // Parsing json object response
                    // response will be a json object
                    //String name = response.getString("name");
                    //String email = response.getString("email");


                    edt_BankName.setText(response.getJSONObject("0").getString("bank_name"));
                    edt_BankAcct.setText(response.getJSONObject("0").getString("bank_account"));
                    edt_BankLocation.setText(response.getJSONObject("0").getString("bank_location"));
                    edt_IFSCCode.setText(response.getJSONObject("0").getString("IFSC_code"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Error: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
                hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to upload bank details. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
                // hide the progress dialog
                hidepDialog();
            }
        });

        // Adding request to request queue
        ApppController.getInstance().addToRequestQueue(jsonObjReq);

    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }




    /*private void initialize() {


        edt_BankName = (EditText)findViewById(R.id.edtBankName);
        edt_BankAcct = (EditText)findViewById(R.id.edtBankAcct);
        edt_BankLocation = (EditText)findViewById(R.id.edtBankLocation);
        edt_IFSCCode = (EditText)findViewById(R.id.edtIFSCCode);

        btn_Submit = (Button)findViewById(R.id.btnBankSubmit);

        btn_Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                bankname = edt_BankName.getText().toString();
                bankacct = edt_BankAcct.getText().toString();
                banklocation = edt_BankLocation.getText().toString();
                ifsccode= edt_IFSCCode.getText().toString();


                if(bankname.isEmpty()|| bankacct.isEmpty() || banklocation.isEmpty() || ifsccode.isEmpty()){
                    Toast.makeText(BankDetailsActivity.this, "fill all details", Toast.LENGTH_SHORT).show();

                }else
                {
                    btnSubmit(bankname,bankacct,banklocation,ifsccode);
                }

            }
        });
    }*/

    public void btnSubmit(final String bankname, final String bankacct, final String banklocation, final String ifsccode) throws JSONException {

        JSONObject jsonObject = new JSONObject(SharedPref.getString(BankDetailsActivity.this, AppConstant.SF.LOGIN_RESPONSE));

        url = "http://monkhub.com/tap-real/json/update-bank.php?member="+ jsonObject.getJSONObject("0").getString("member_id")
                +"&bank_name="+bankname+"&bank_account="+bankacct+"&bank_location="+banklocation+"&ifsc="+ifsccode;
        url=url.replace(" ", "%20");

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")){

                        Toast.makeText(getApplicationContext(),"Successfully Submited",Toast.LENGTH_LONG).show();
                        Intent bank_intent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(bank_intent);

                    }else
                    {
                        Toast.makeText(getApplicationContext(),"Bank Details not Submitted",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getApplicationContext(), "Error....", Toast.LENGTH_LONG).show();

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("bank_name", edt_BankName.getText().toString().trim());
                params.put("bank_account", edt_BankAcct.getText().toString().trim());
                params.put("bank_location", edt_BankLocation.getText().toString().trim());
                params.put("ifsc", edt_IFSCCode.getText().toString().trim());

                return params;
            }
        };

        requestQueue.add(jsonObjectRequest);

    }
}
