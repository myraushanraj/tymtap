package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SecurityActivity extends AppCompatActivity {


    EditText edt_LastPaswrd,edt_NewPaswrd,edt_ConfirmPswrd;

    Button btn_NewPswrd;
    JSONObject jsonObject = null;


    TextView tv_forgot_pass;
    String lastPassword, newPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);

        tv_forgot_pass = findViewById(R.id.tv_forgot_pass);
        edt_LastPaswrd = findViewById(R.id.edt_LastPaswrd);
        edt_NewPaswrd = findViewById(R.id.edt_NewPaswrd);
        edt_ConfirmPswrd = findViewById(R.id.edt_ConfirmPswrd);
        try {
            jsonObject = new JSONObject(SharedPref.getString(SecurityActivity.this, AppConstant.SF.LOGIN_RESPONSE));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        btn_NewPswrd = findViewById(R.id.btn_NewPswrd);

        btn_NewPswrd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPassword = edt_NewPaswrd.getText().toString();
                lastPassword = edt_LastPaswrd.getText().toString();

                try {
                    setNew_Password(lastPassword,newPassword);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        tv_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent setPassword_intent = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                startActivity(setPassword_intent);
            }
        });
    }

    private void setNew_Password(String lastPassword, String newPassword) throws JSONException {

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        String url = "http://monkhub.com/tap-real/json/password_update.php?old_password="+lastPassword+"&new_password="+newPassword+"&member="+jsonObject.getJSONObject("0").getString("member_id");

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")) {

                        Toast.makeText(getApplicationContext(),"Successfully Updated", Toast.LENGTH_SHORT).show();

                        Intent setUpdate_intent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(setUpdate_intent);
                        finish();

                    }else {

                        Toast.makeText(getApplicationContext(), "Password is not updated", Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(), "Error: " + e, Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error:" + error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                //params.put("email", edt_email.getText().toString().trim());
                params.put("old_password", edt_LastPaswrd.getText().toString().trim());
                params.put("new_password", edt_NewPaswrd.getText().toString().trim());

                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);


    }
}
