package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pragya.tym_tap.R;

public class ProfileDetailsActivity extends AppCompatActivity {

   TextView tv_basictab, tv_payment,tvSecurityTab;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile__details);



        tv_basictab = findViewById(R.id.tvBasicTab);
        tv_payment = findViewById(R.id.tvPaymentsTab);
        tvSecurityTab = findViewById(R.id.tvSecurityTab);

        tvSecurityTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent security_intent = new Intent(getApplicationContext(), SecurityActivity.class);
                startActivity(security_intent);
            }
        });

        tv_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in_dropdownpro = new Intent(getApplicationContext(), BankDetailsActivity.class);
                startActivity(in_dropdownpro);
            }
        });
        tv_basictab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in_profile = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(in_profile);
            }
        });
    }
}
