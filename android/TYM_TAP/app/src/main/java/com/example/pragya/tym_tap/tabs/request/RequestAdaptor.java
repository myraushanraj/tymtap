package com.example.pragya.tym_tap.tabs.request;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.Controller.CategoriesActivity;
import com.example.pragya.tym_tap.Controller.CheckInActivity;
import com.example.pragya.tym_tap.Controller.CheckOutActivity;
import com.example.pragya.tym_tap.Controller.ViewInvoiceActivity;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.squareup.picasso.Picasso;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by AJIT on 10-03-2018.
 */

public class RequestAdaptor extends RecyclerView.Adapter<RequestAdaptor.MyViewHolder> {

    private List<RequestModel> requestModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_job_title, tv_company_name, tv_posted_ago, tv_job_start, tv_fair_rate, tv_location;
        public RatingBar rb_job_rating;
        public CircleImageView iv_jobs;
        public Button btn_request_tab;
        String company;

        public MyViewHolder(View view) {
            super(view);
            btn_request_tab = view.findViewById(R.id.btn_request_tab);
            btn_request_tab.setOnClickListener(listener);
            iv_jobs = view.findViewById(R.id.iv_jobs);
            rb_job_rating = view.findViewById(R.id.rb_job_rating);
            tv_job_title = view.findViewById(R.id.tv_job_title);
            tv_company_name = view.findViewById(R.id.tv_company_name);
            tv_posted_ago = view.findViewById(R.id.tv_posted_ago);
            tv_job_start = view.findViewById(R.id.tv_job_start);
            tv_fair_rate = view.findViewById(R.id.tv_fair_rate);
            tv_location = view.findViewById(R.id.tv_location);
        }

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button = (Button) view;
                if (button.getText().equals(AppConstant.JOB_BTN.CHECK_IN)) {
                    //String company = requestModelList.get(getAdapterPosition()).getCompany();
                    Intent intent = new Intent(context, CheckInActivity.class);
                    intent.putExtra(AppConstant.INTENT_TAG.CHECKING_ID, requestModelList.get(getAdapterPosition()));
                    //intent.putExtra("company",requestModelList.get(getAdapterPosition()).getCompany());
                    context.startActivity(intent);

                } else if (button.getText().equals(AppConstant.JOB_BTN.CHECK_OUT)) {
                    Intent intent = new Intent(context, CheckOutActivity.class);
                    intent.putExtra(AppConstant.INTENT_TAG.CHECKING_ID, requestModelList.get(getAdapterPosition()));
                    context.startActivity(intent);
                } else if (button.getText().equals(AppConstant.JOB_BTN.VIEW_INVOICE)) {
                    Intent intent = new Intent(context, ViewInvoiceActivity.class);
                    intent.putExtra(AppConstant.INTENT_TAG.CHECKING_ID, requestModelList.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            }
        };
    }

    private Context context;

    RequestAdaptor(Context context, List<RequestModel> requestModelList) {
        this.context = context;
        this.requestModelList = requestModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_request_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RequestModel requestMode = requestModelList.get(position);
        holder.rb_job_rating.setVisibility(View.INVISIBLE);
        holder.tv_job_title.setText(requestMode.getJob_title());
        holder.tv_fair_rate.setText(requestMode.getFare_rate_formatted());
        holder.tv_job_start.setText(requestMode.getApplied_at());
        holder.tv_company_name.setText(requestMode.getCompany_name());
        holder.tv_location.setText(requestMode.getLocation());
        //Picasso.get().load(requestMode.getImage_url()).into(holder.iv_jobs);
        Picasso.get().load(requestMode.getImage_url()).into(holder.iv_jobs);

        holder.tv_posted_ago.setText(requestMode.getApplied_ago());
        if (requestMode.getJob_status().equalsIgnoreCase("accepted")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.CHECK_IN);
            holder.btn_request_tab.setClickable(true);
        } else if (requestMode.getJob_status().equalsIgnoreCase("checked_in")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.CHECK_OUT);
            holder.btn_request_tab.setClickable(true);
        } else if (requestMode.getJob_status().equalsIgnoreCase("payment_done")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.VIEW_INVOICE);
            holder.btn_request_tab.setClickable(true);
        } else if (requestMode.getJob_status().equalsIgnoreCase("checked_out")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.PAYMENT_UNDER_PROCESS);
            holder.btn_request_tab.setClickable(false);
        } else if (requestMode.getJob_status().equalsIgnoreCase("null")) {
            holder.btn_request_tab.setVisibility(View.INVISIBLE);
            holder.btn_request_tab.setClickable(true);
        }else if (requestMode.getJob_status().equalsIgnoreCase("rejected")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.REJECTED);
            holder.btn_request_tab.setClickable(false);
        }else if (requestMode.getJob_status().equalsIgnoreCase("disputed")) {
            holder.btn_request_tab.setText(AppConstant.JOB_BTN.DISPUTED);
            holder.btn_request_tab.setClickable(false);
        }

    }

    @Override
    public int getItemCount() {
        return requestModelList.size();
    }
}