package com.example.pragya.tym_tap.tabs.request;

import java.io.Serializable;

/**
 * Created by AJIT on 10-03-2018.
 */

public class RequestModel implements Serializable {

    String id;
    String job_status;
    String job_status1;
    String hangout_done;
    String applied_at;
    String applied_ago;
    String job_seeker;
    String thread_id;
    String job_title;
    String job_seeker_name;
    String job_seeker_about;
    String job_seeker_phone;
    String job_seeker_profile_pic;
    String fare_rate_formatted;
    String job_end_time;
    String company;
    String image_url;
    String location;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJob_end_time() {
        return job_end_time;
    }

    public void setJob_end_time(String job_end_time) {
        this.job_end_time = job_end_time;
    }

    public String getFare_rate_formatted() {
        return fare_rate_formatted;
    }

    public void setFare_rate_formatted(String fare_rate_formatted) {
        this.fare_rate_formatted = fare_rate_formatted;
    }

    public String getJob_start_in() {
        return job_start_in;
    }

    public void setJob_start_in(String job_start_in) {
        this.job_start_in = job_start_in;
    }

    String job_start_in;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    String company_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJob_status() {
        return job_status;
    }

    public void setJob_status(String job_status) {
        this.job_status = job_status;
    }

    public String getJob_status1() {
        return job_status1;
    }

    public void setJob_status1(String job_status1) {
        this.job_status1 = job_status1;
    }

    public String getHangout_done() {
        return hangout_done;
    }

    public void setHangout_done(String hangout_done) {
        this.hangout_done = hangout_done;
    }

    public String getApplied_at() {
        return applied_at;
    }

    public void setApplied_at(String applied_at) {
        this.applied_at = applied_at;
    }

    public String getApplied_ago() {
        return applied_ago;
    }

    public void setApplied_ago(String applied_ago) {
        this.applied_ago = applied_ago;
    }

    public String getJob_seeker() {
        return job_seeker;
    }

    public void setJob_seeker(String job_seeker) {
        this.job_seeker = job_seeker;
    }

    public String getThread_id() {
        return thread_id;
    }

    public void setThread_id(String thread_id) {
        this.thread_id = thread_id;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getJob_seeker_name() {
        return job_seeker_name;
    }

    public void setJob_seeker_name(String job_seeker_name) {
        this.job_seeker_name = job_seeker_name;
    }

    public String getJob_seeker_about() {
        return job_seeker_about;
    }

    public void setJob_seeker_about(String job_seeker_about) {
        this.job_seeker_about = job_seeker_about;
    }

    public String getJob_seeker_phone() {
        return job_seeker_phone;
    }

    public void setJob_seeker_phone(String job_seeker_phone) {
        this.job_seeker_phone = job_seeker_phone;
    }

    public String getJob_seeker_profile_pic() {
        return job_seeker_profile_pic;
    }

    public void setJob_seeker_profile_pic(String job_seeker_profile_pic) {
        this.job_seeker_profile_pic = job_seeker_profile_pic;
    }
}