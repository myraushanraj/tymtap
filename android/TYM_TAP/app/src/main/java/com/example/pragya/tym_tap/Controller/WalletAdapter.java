package com.example.pragya.tym_tap.Controller;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.pragya.tym_tap.R;

import java.util.List;

/**
 * Created by Pragya on 4/5/2018.
 */

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {

    private Context context;
    private List<Wallet> list;

    public WalletAdapter(Context context, List<Wallet> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.wallet_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Wallet movie = list.get(position);

        holder.tv_dates.setText(movie.getTimings());
        holder.tv_balance.setText(movie.getBalance());
        holder.tv_amount.setText(movie.getAmount());
        holder.btn_wallet.setText(movie.getType1());

        if (movie.getType1().equalsIgnoreCase("withdraw")){

            //holder.btn_wallet.setBackgroundColor(Color.parseColor("#8DC63F"));
            holder.btn_wallet.setBackgroundColor(Color.parseColor("#8DC63F"));


        }



        //holder.textRating.setText(String.valueOf(movie.getRating()));
        //holder.textYear.setText(String.valueOf(movie.getYear()));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_dates, tv_balance, tv_amount, btn_wallet;


        public ViewHolder(View itemView) {
            super(itemView);

            tv_dates = itemView.findViewById(R.id.tv_dates);
            tv_balance = itemView.findViewById(R.id.tv_balance);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            btn_wallet = itemView.findViewById(R.id.btn_wallet);
            /*if (btn_wallet.getText().equals("withdraw")){

                btn_wallet.setBackgroundResource(R.color.withdraw);

            }*/
            //textRating = itemView.findViewById(R.id.main_rating);
            //textYear = itemView.findViewById(R.id.main_year);
        }
    }
}

