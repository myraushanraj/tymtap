package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerifiedMobileActivity extends AppCompatActivity {

    Button btn_login_now;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verified_mobile);

        Intent i = getIntent();
        final String email = i.getStringExtra("TextBox");
        final String password = i.getStringExtra("TextPassword");

        btn_login_now = findViewById(R.id.btn_Loginnow);
        btn_login_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login_now(email,password);
            }
        });
    }

    public void login_now(final String email, final String password) {


        String url = "http://monkhub.com/tap-real/json/login.php?email="+email+"&password="+password;

        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                SharedPref.putString(VerifiedMobileActivity.this,"LOGIN_DATA",response.toString());

                try {
                    if (response.getString("status").equals("success")){
                        SharedPref.putString(VerifiedMobileActivity.this, AppConstant.SF.LOGIN_RESPONSE,response.toString());
                        Intent in_tent = new Intent(getApplicationContext(), TymTapActivity.class);
                        startActivity(in_tent);
                        finish();
                    }else {
                        Toast.makeText(getApplicationContext(),"Login failed",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "error: "+e,Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), "Error:"+error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return params;
            }
        };
        requestQueue.add(jsonObjectRequest);
    }
}