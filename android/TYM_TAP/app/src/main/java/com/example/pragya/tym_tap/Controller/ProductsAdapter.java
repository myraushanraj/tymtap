package com.example.pragya.tym_tap.Controller;

/**
 * Created by Pragya on 3/19/2018.
 */
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pragya.tym_tap.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder> {


    private Context mCtx;
    private List<Product> productList;

    public ProductsAdapter(Context mCtx, List<Product> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.product_list, null);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        Product product = productList.get(position);

        //loading the image
        /*Glide.with(mCtx)
                .load(product.getImage())
                .into(holder.imageView);*/
        Picasso.get().load(product.getImage_url()).into(holder.imageView);
        holder.tv_notification.setText(product.getNotification());
        holder.tv_timings.setText(product.getTimings());
        //holder.textViewShortDesc.setText(product.getShortdesc());
        //holder.textViewRating.setText(String.valueOf(product.getRating()));
        //holder.textViewPrice.setText(String.valueOf(product.getPrice()));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView tv_notification, tv_timings;
        CircleImageView imageView;

        public ProductViewHolder(View itemView) {
            super(itemView);

            tv_notification = itemView.findViewById(R.id.tvNotification1);
            tv_timings = itemView.findViewById(R.id.tvTime);
            imageView=(CircleImageView)itemView.findViewById(R.id.iv_NotiImage);
        }
    }
}


