package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.tabs.request.RequestModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckInActivity extends AppCompatActivity {

    Button btnCheckinNow;
    @BindView(R.id.tv_job_name)
    TextView tv_job_name;

    @BindView(R.id.tvCompanyName)
    TextView tvCompanyName;

    @BindView(R.id.tvLabel)
    TextView tvLabel;

    @BindView(R.id.tvLabe2)
    TextView tvLabe2;



    String status_text, company;
    RequestModel requestModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);

        btnCheckinNow = findViewById(R.id.btnCheckinNow);
        ButterKnife.bind(this);
        requestModel = (RequestModel) getIntent().getSerializableExtra(AppConstant.INTENT_TAG.CHECKING_ID);
        tv_job_name.setText(requestModel.getJob_title());
        tvCompanyName.setText(requestModel.getCompany_name());
        //tvLabel.setText(requestModel.getJob_start_in());
        tvLabe2.setText(requestModel.getJob_start_in());

        company = requestModel.getCompany();




    }

    @OnClick(R.id.btnCheckinNow)
    void btnCheckinNow() {
        btn_CheckIn(requestModel.getId());
    }

    @OnClick(R.id.btnSuper)
    void btnSuper() {
        String companyId = getIntent().getStringExtra("Company_id");
        Intent supervisor_intent = new Intent(getApplicationContext(), ChatActivity.class);
        //supervisor_intent.putExtra("company",companyId);

        startActivity(supervisor_intent);
    }

    private void initialize() {


    }

    private void btn_CheckIn(String id) {

        //Intent i = getIntent();
        //final String company = i.getStringExtra("company");
        String url = "http://monkhub.com/tap-real/json/check-in.php?id=" + id;
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    if (response.getString("status").equals("success")) {
                        btnCheckinNow.setVisibility(View.INVISIBLE);

                        Toast.makeText(getApplicationContext(), "Successfully Check In", Toast.LENGTH_LONG).show();

                        Intent check_in = new Intent(getApplicationContext(), AnouncementsActivity.class);
                        check_in.putExtra(AppConstant.INTENT_TAG.CHECKING_ID,requestModel);
                        check_in.putExtra("company",company);
                        startActivity(check_in);
                        status_text = response.getString("status_text");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    Toast.makeText(getApplicationContext(), "error:" + e, Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(getApplicationContext(), "Volley Error:" + error, Toast.LENGTH_LONG).show();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Failed to Check-In. Please try again later.", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

        requestQueue.add(jsonObjectRequest);


    }
}