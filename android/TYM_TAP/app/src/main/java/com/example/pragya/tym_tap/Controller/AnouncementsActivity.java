package com.example.pragya.tym_tap.Controller;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.pragya.tym_tap.R;
import com.example.pragya.tym_tap.Utils.AppConstant;
import com.example.pragya.tym_tap.Utils.SharedPref;
import com.example.pragya.tym_tap.tabs.request.RequestModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AnouncementsActivity extends AppCompatActivity {

    TextView tv_JobName,tv_CompanyName;


    //private RecyclerView mList;

    //private LinearLayoutManager linearLayoutManager;
    //private DividerItemDecoration dividerItemDecoration;
    //private List<Movie> movieList;
    //private RecyclerView.Adapter adapter;
    RequestModel requestModel;

    Button  btn_checkout_now, btn_contact_supervisor,btn_load_anouncements;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anouncements);
        tv_JobName = findViewById(R.id.tv_JobName);
        tv_CompanyName = findViewById(R.id.tv_CompanyName);


        requestModel = (RequestModel) getIntent().getSerializableExtra(AppConstant.INTENT_TAG.CHECKING_ID);
        //final String request_id = requestModel.getId();


        btn_load_anouncements = findViewById(R.id.btn_load_anouncements);
        btn_load_anouncements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent load_anounacement_intent  = new Intent(getApplicationContext(), LoadAnouncementsActivity.class);
                load_anounacement_intent.putExtra(AppConstant.INTENT_TAG.CHECKING_ID,requestModel);
                startActivity(load_anounacement_intent);
            }
        });

        //mList = findViewById(R.id.rv_anouncement_layout);



        tv_JobName.setText(requestModel.getJob_title());
        tv_CompanyName.setText(requestModel.getCompany_name());

        final String company = requestModel.getCompany();


        //Intent i = getIntent();
        //final String company = i.getStringExtra("company");


        //movieList = new ArrayList<>();
        //adapter = new MovieAdapter(getApplicationContext(),movieList);

        //String companyId = setText(requestModel.get)

        //linearLayoutManager = new LinearLayoutManager(this);
        //linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //dividerItemDecoration = new DividerItemDecoration(mList.getContext(), linearLayoutManager.getOrientation());



        //mList.setHasFixedSize(true);
        //mList.setLayoutManager(linearLayoutManager);
        //mList.addItemDecoration(dividerItemDecoration);
        //mList.setAdapter(adapter);

        //getData();




        btn_contact_supervisor = findViewById(R.id.btn_CntctSuper);

        /*btn_contact_supervisor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent supervisor_intent = new Intent(getApplicationContext(),ChatActivity.class);

                    JSONObject jsonObject = new JSONObject(SharedPref.getString(AnouncementsActivity.this, AppConstant.SF.LOGIN_RESPONSE));

                    supervisor_intent.putExtra("url", "http://monkhub.com/chat-box/my-chat-small.php?owner="+jsonObject.getJSONObject("0").getString("member_id")+"&member="+company);
                    startActivity(supervisor_intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });*/
        btn_checkout_now=(Button)findViewById(R.id.btn_checkout_now);
        btn_checkout_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent checkout_in = new Intent(getApplicationContext(), CheckOutActivity.class);
                checkout_in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                checkout_in.putExtra(AppConstant.INTENT_TAG.CHECKING_ID,requestModel);
                startActivity(checkout_in);
                finish();

            }
        });
    }

    /*public void getData() {



        String url = "http://monkhub.com/tap-real/json/load_announcement.php?id="+requestModel.getId();

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(i);

                        Movie movie = new Movie();
                        movie.setContent(jsonObject.getString("content"));
                        //movie.setTitle(jsonObject.getString("title"));


                        movieList.add(movie);
                    } catch (JSONException e) {
                        e.printStackTrace();

                        
                        //progressDialog.dismiss();


                    }
                }
                adapter.notifyDataSetChanged();
                //progressDialog.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Volley", error.toString());
                //progressDialog.dismiss();
                if (error.networkResponse == null){
                    if (error.getClass().equals(TimeoutError.class)){
                        Toast.makeText(getApplicationContext(),
                                "Oops. Timeout error! Try Again", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }*/


}
