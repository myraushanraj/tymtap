package com.example.pragya.tym_tap.Utils;

/**
 * Created by AJIT on 10-03-2018.
 */

public class AppConstant {
    public interface URLS {
        String IMG_ROOT_URL = "http://monkhub.com/tap-real/img-company/";
        String FEED_ROOT_URL = "http://monkhub.com/tap-real/json/load_all_feeds.php?id=";
        String REQUEST_ROOT_URL = "http://monkhub.com/tap-real/json/load_my_application.php?id=";
        String MESSAGE_ROOT_URL = "http://monkhub.com/tap-real/json/my_all_msg_list.php?member=";
    }

    public interface SF {
        String LOGIN_RESPONSE = "LOGIN RESPONSE";
    }

    public interface JOB_BTN {
        String CHECK_IN = "Check In";
        String CHECK_OUT = "Check Out";
        String VIEW_INVOICE = "View Invoice";
        String PAYMENT_UNDER_PROCESS = "Payment In Process";
        String REJECTED = "Rejected";
        String DISPUTED = "Disputed";
        String APPLIED = "Applied";
        //String APPLY_NOW = "Apply Now";
    }

    public interface INTENT_TAG{
        String CHECKING_ID="Checking Id";
        String COMPANY_ID="Company Id";
        String JOB_ID="Job Id";
    }
}
